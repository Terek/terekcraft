﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;

namespace TerekCraftEngine_Test.spatial
{

    [TestFixture]
    public class QuadTreeCorrectnessTest : SpatialDatabaseCompareTest
    {
        [SetUp]
        public void Init()
        {
            SpatialDataBase = new QuadTree<TestLocation>(20, -100, -100, 100, 100);
        }
    }


    [TestFixture]
    public abstract class SpatialDatabaseCompareTest : BaseTest
    {
        protected ISpatialDataBase<TestLocation> SpatialDataBase;
        protected ISpatialDataBase<TestLocation> ReferenceDataBase = new NaiveSpatialDatabase<TestLocation>();

        readonly Random random = new Random();

        readonly Stopwatch naiveImplementationBuildStopWatch = new Stopwatch();
        readonly Stopwatch newImplementationBuildStopWatch = new Stopwatch();

        readonly Stopwatch naiveImplementationRetreiveInRangeStopWatch = new Stopwatch();
        readonly Stopwatch newImplementationRetreiveInRangeStopWatch = new Stopwatch();

        readonly Stopwatch naiveImplementationFindClosestStopWatch = new Stopwatch();
        readonly Stopwatch newImplementationFindClosestStopWatch = new Stopwatch();

        [Test]
        public void Compare_To_Reference_Spatial_Database()
        {
            List<TestLocation> generatedLocations = new List<TestLocation>();
            for (int i = 0; i < 10; i++)
            {
                var testLocation = new TestLocation() {Location = new Point(GetRandomCoordinate(), GetRandomCoordinate()), Size = 0.01};
                generatedLocations.Add(testLocation);
            }

            naiveImplementationBuildStopWatch.Start();
            foreach (var location in generatedLocations)
            {
                ReferenceDataBase.Add(location);
            }
            naiveImplementationBuildStopWatch.Stop();

            newImplementationBuildStopWatch.Start();
            foreach (var location in generatedLocations)
            {
                SpatialDataBase.Add(location);
            }
            newImplementationBuildStopWatch.Stop();

            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    var location = new Point(i*2f - 100, j*2f - 100);
                    
                    naiveImplementationRetreiveInRangeStopWatch.Start();
                    HashSet<TestLocation> referenceLocations = new HashSet<TestLocation>(ReferenceDataBase.RetreiveInRange(location, 5f, null));
                    naiveImplementationRetreiveInRangeStopWatch.Stop();
                    
                    newImplementationRetreiveInRangeStopWatch.Start();
                    HashSet<TestLocation> testLocations = new HashSet<TestLocation>(SpatialDataBase.RetreiveInRange(location, 5f, null));
                    newImplementationRetreiveInRangeStopWatch.Stop();

                    Assert.AreEqual(referenceLocations.Count, testLocations.Count);
                    foreach (var referenceLocation in referenceLocations)
                    {
                        Assert.IsTrue( testLocations.Contains( referenceLocation ));
                    }

                    naiveImplementationFindClosestStopWatch.Start();
                    TestLocation referenceClosest = ReferenceDataBase.FindClosest(location, null);
                    naiveImplementationFindClosestStopWatch.Stop();

                    newImplementationFindClosestStopWatch.Start();
                    TestLocation testClosest = SpatialDataBase.FindClosest(location, null);
                    newImplementationFindClosestStopWatch.Stop();
                    
                    Assert.AreEqual(referenceClosest, testClosest);
                }
            }


            System.Console.WriteLine("Naive build time: " + naiveImplementationBuildStopWatch.ElapsedMilliseconds);
            System.Console.WriteLine("New implementation build time: " + newImplementationBuildStopWatch.ElapsedMilliseconds);


            System.Console.WriteLine("Naive retreive in range time: " + naiveImplementationRetreiveInRangeStopWatch.ElapsedMilliseconds);
            System.Console.WriteLine("New implementation retreive in range time: " + newImplementationRetreiveInRangeStopWatch.ElapsedMilliseconds);

            System.Console.WriteLine("Naive find closest time: " + naiveImplementationFindClosestStopWatch.ElapsedMilliseconds);
            System.Console.WriteLine("New implementation find closest time: " + newImplementationFindClosestStopWatch.ElapsedMilliseconds);

        }

        double GetRandomCoordinate()
        {
            return random.NextDouble() * 200 - 100;
        }
    } 
}
