﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;

namespace TerekCraftEngine_Test.spatial
{
    [TestFixture]
    public class QuadTreeContainsTest
    {
        readonly QuadTree<TestLocation> _quadTree = new QuadTree<TestLocation>(10, -1, -1, 1, 1);

        [Test]
        public void Test_contains_everything_is_in()
        {
            ShouldBeInside(0, 0, 0.5);
        }

        [Test]
        public void Test_not_contains_x_too_small()
        {
            ShouldBeOutside(-2, 0, 0.5);
        }


        [Test]
        public void Test_not_contains_x_too_big()
        {
            ShouldBeOutside(2, 0, 0.5);
        }

        [Test]
        public void Test_not_contains_y_too_small()
        {
            ShouldBeOutside(0, -2, 0.5);
        }

        [Test]
        public void Test_not_contains_y_too_big()
        {
            ShouldBeOutside(0, 2, 0.5);
        }

        [Test]
        public void Test_contains_location_inside_size_very_big()
        {
            ShouldBeInside(0,0,5);
        }

        [Test]
        public void Test_contains_x_outside_size_big()
        {
            ShouldBeInside(1.1, 0, 0.2);
            ShouldBeInside(1.1, 0, 1);
            ShouldBeInside(1.1, 0, 10);
            ShouldBeInside(-1.1, 0, 0.2);
            ShouldBeInside(-1.1, 0, 1);
            ShouldBeInside(-1.1, 0, 10);
        }

        [Test]
        public void Test_contains_y_outside_size_big()
        {
            ShouldBeInside(0, 1.1, 0.2);
            ShouldBeInside(0, 1.1, 1);
            ShouldBeInside(0, 1.1, 10);
            ShouldBeInside(0, -1.1, 0.2);
            ShouldBeInside(0, -1.1, 1);
            ShouldBeInside(0, -1.1, 10);
        }


        [Test]
        public void Test_contains_x_and_y_outside_size_big()
        {
            ShouldBeInside(1.1, 1.1, 0.2);
            ShouldBeInside(1.1, 1.1, 1);
            ShouldBeInside(1.1, 1.1, 10);
            ShouldBeInside(1.1, -1.1, 0.2);
            ShouldBeInside(1.1, -1.1, 1);
            ShouldBeInside(1.1, -1.1, 10);

            ShouldBeInside(-1.1, 1.1, 0.2);
            ShouldBeInside(-1.1, 1.1, 1);
            ShouldBeInside(-1.1, 1.1, 10);
            ShouldBeInside(-1.1, -1.1, 0.2);
            ShouldBeInside(-1.1, -1.1, 1);
            ShouldBeInside(-1.1, -1.1, 10);
        }

        void ShouldBeInside(double x, double y, double size)
        {
            Assert.IsTrue(_quadTree.Contains(new TestLocation { Location = new Point(x, y), Size = size }));            
        }

        void ShouldBeOutside(double x, double y, double size)
        {
            Assert.IsFalse(_quadTree.Contains(new TestLocation { Location = new Point(x, y), Size = size }));
        }


    }
}
