﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;

namespace TerekCraftEngine_Test.spatial
{

    public class TestLocation : ISpatialData
    {
        public Point Location { get; set; }
        public double Size { get; set; }

        public string Tag { get; set; }

        public TestLocation()
        {
            Size = 1;
            Location = new Point(0,0);
            Tag = "";
        }
    }

    [TestFixture]
    public class NaiveSpatialDatabaseTest : SpatialDatabaseTest
    {
        [SetUp]
        public void Init()
        {
            SpatialDataBase = new NaiveSpatialDatabase<TestLocation>();
        }
    }

    [TestFixture]
    public class QuadTreeTest : SpatialDatabaseTest
    {
        [SetUp]
        public void Init()
        {
            SpatialDataBase = new QuadTree<TestLocation>(10, -100, -100, 100, 100);
        }
    }


    [TestFixture]
    public abstract class SpatialDatabaseTest : BaseTest
    {
        protected ISpatialDataBase<TestLocation> SpatialDataBase;

        [Test]
        public void Test_Empty_RetreiveInRange()
        {
            Assert.IsFalse(SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).Any());
        }

        [Test]
        public void Test_RetreiveInRange_not_there()
        {
            SpatialDataBase.Add(new TestLocation { Location = new Point(10, 0) });

            Assert.IsFalse(SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).Any());
        }


        [Test]
        public void Test_RetreiveInRange_one_there()
        {
            var testObj = new TestLocation { Location = new Point(0, 0) };
            SpatialDataBase.Add(testObj);

            ICollection<TestLocation> retreived = SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null);
            Assert.IsTrue(retreived.Any());

            Assert.AreEqual(testObj, retreived.First());
        }


        [Test]
        public void Test_RetreiveInRange_one_there_with_distance()
        {
            var testObj = new TestLocation { Location = new Point(1f, 0) };
            SpatialDataBase.Add(testObj);

            Assert.IsTrue(SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).Any());

            Assert.AreEqual(testObj, SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).First());
        }

        [Test]
        public void Test_RetreiveInRange_one_there_with_more_distance_bigger_size()
        {
            var testObj = new TestLocation { Location = new Point(10f, 0), Size = 12f };
            SpatialDataBase.Add(testObj);

            Assert.IsTrue(SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).Any());

            Assert.AreEqual(testObj, SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).First());
        }

        [Test]
        public void Test_RetreiveInRange_one_there_with_more_distance_bigger_size_greater_range()
        {
            var testObj = new TestLocation { Location = new Point(10f, 0), Size = 9f };
            SpatialDataBase.Add(testObj);

            Assert.IsTrue(SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).Any());

            Assert.AreEqual(testObj, SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null).First());
        }


        [Test]
        public void Test_RetreiveInRange_more_there()
        {
            var testObj1 = new TestLocation { Location = new Point(0, 0) };
            var testObj2 = new TestLocation { Location = new Point(0, 0) };
            SpatialDataBase.Add(testObj1);
            SpatialDataBase.Add(testObj2);

            ICollection<TestLocation> retreivedLocations = SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null);
            Assert.IsTrue(retreivedLocations.Any());
            Assert.IsTrue(retreivedLocations.Contains(testObj1));
            Assert.IsTrue(retreivedLocations.Contains(testObj2));
        }

        [Test]
        public void Test_RetreiveInRange_one_there_and_one_not_there()
        {
            var testObj1 = new TestLocation { Location = new Point(0, 0) };
            var testObj2 = new TestLocation { Location = new Point(10, 0) };
            SpatialDataBase.Add(testObj1);
            SpatialDataBase.Add(testObj2);

            ICollection<TestLocation> retreivedLocations = SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, null);
            Assert.IsTrue(retreivedLocations.Any());
            Assert.IsTrue(retreivedLocations.Contains(testObj1));
            Assert.IsFalse(retreivedLocations.Contains(testObj2));
        }

        [Test]
        public void Test_RetreiveInRange_one_does_not_match()
        {
            var tag = GetUniqueString();

            var testObj1 = new TestLocation { Location = new Point(0, 0), Tag = tag };
            var testObj2 = new TestLocation { Location = new Point(0, 0) };
            SpatialDataBase.Add(testObj1);
            SpatialDataBase.Add(testObj2);

            ICollection<TestLocation> retreivedLocations = SpatialDataBase.RetreiveInRange(new Point(0, 0), 1f, item=> item.Tag == tag );
            Assert.IsTrue(retreivedLocations.Any());
            Assert.IsTrue(retreivedLocations.Contains(testObj1));
            Assert.IsFalse(retreivedLocations.Contains(testObj2));
        }

        [Test]
        public void Test_GetClosest()
        {
            var testObj1 = new TestLocation { Location = new Point(0, 0) };
            var testObj2 = new TestLocation { Location = new Point(10, 0) };
            SpatialDataBase.Add(testObj1);
            SpatialDataBase.Add(testObj2);

            var found = SpatialDataBase.FindClosest(new Point(0, 0), null);

            Assert.IsNotNull(found);
            Assert.AreEqual(testObj1, found);
        }


        [Test]
        public void Test_GetClosest_No_Matching()
        {
            var testObj1 = new TestLocation { Location = new Point(0, 0) };
            SpatialDataBase.Add(testObj1);

            var found = SpatialDataBase.FindClosest(new Point(0, 0), item => item.Tag == GetUniqueString());

            Assert.IsNull(found);
        }

        [Test]
        public void Test_GetClosest_empty()
        {
            var found = SpatialDataBase.FindClosest(new Point(0, 0), null);

            Assert.IsNull(found);
        }

        [Test]
        public void Test_GetClosest_empty_with_predicate()
        {
            var found = SpatialDataBase.FindClosest(new Point(0, 0), item=> item.Tag == GetUniqueString());

            Assert.IsNull(found);
        }

        public void Test_ListAll()
        {
            var testObj1 = new TestLocation { Location = new Point(0, 0) };
            var testObj2 = new TestLocation { Location = new Point(0, 0) };
            SpatialDataBase.Add(testObj1);
            SpatialDataBase.Add(testObj2);

            ICollection<TestLocation> retreivedLocations = SpatialDataBase.ListAll();
            Assert.IsTrue(retreivedLocations.Contains(testObj1));
            Assert.IsTrue(retreivedLocations.Contains(testObj2));
            Assert.AreEqual(2, retreivedLocations.Count);
        }

    } 
}
