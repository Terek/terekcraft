﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class ObstacleAvoidanceTest : BaseTest
    {
        readonly ActorType _actorType = new ActorType("actor", 10, 10, new List<BaseAction>() { new MoveAction(5) });

        string _actorId;

        Point target = new Point(100, 0);

        double _distanceToObstacle;
        double _directionToObstacle;

        [SetUp]
        public void SetUp()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            string obstacleId = AddActorWithType(_actorType, 24, -5, 0);

            Actor actor = World.GetActor(_actorId);
            Actor obstacle = World.GetActor(obstacleId);

            _distanceToObstacle = Math.Sqrt(actor.Location.GetDistanceSquaredTo(obstacle.Location));
            _directionToObstacle = actor.Location.GetDirectionTo(obstacle.Location);

        }

        [Test]
        public void Test_FailToMove()
        {
            World.Move(_actorId, 0, 5);
           
            World.Tick();

            Assert.AreEqual(0, World.GetTickEvents().Count);
        }

        [Test]
        public void Test_AITools_AvoidObstacles()
        {
            double distance = 5f;
            double direction = 0f;

            World.Move(_actorId, direction, distance);

            World.Tick();

            Assert.AreEqual(0, World.GetTickEvents().Count);
        }





    }
}
