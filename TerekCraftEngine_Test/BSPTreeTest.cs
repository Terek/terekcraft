﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class BspTreeTest : BaseTest
    {

        [Test]
        public void Test_Empty()
        {
            var bspTree = new BSPTree<Polygon>(new List<Polygon>());
            Assert.That(bspTree.IsOutside(new Point(0, 0)), Is.True);
        }


        [Test]
        public void Test_ClosedPolygon_Inside()
        {
            var bspTree = CreateBSPTreeFromPoints(
                new Point(0, 0),
                new Point(10, 0),
                new Point(0, 10)
                );

            Assert.That(bspTree.IsOutside(new Point(1, 1)), Is.False);


            Assert.That(bspTree.HasCollision(new Point(1, 1), 0.5), Is.False);

            Assert.That(bspTree.HasCollision(new Point(5, 1), 2), Is.True);
            Assert.That(bspTree.HasCollision(new Point(1, 5), 2), Is.True);
            Assert.That(bspTree.HasCollision(new Point(4, 4), 2), Is.True);
        }


        [Test]
        public void Test_ClosedPolygon_Outside()
        {
            var bspTree = CreateBSPTreeFromPoints(
                new Point(0, 0),
                new Point(10, 0),
                new Point(0, 10)
                );

            Assert.That(bspTree.IsOutside(new Point(20, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(20, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(0, 20)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(0, -20)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(-1, 5)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(10, 10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(5, -1)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(6, 6)), Is.True);


            Assert.That(bspTree.HasCollision(new Point(10, 10), 2), Is.True);

        }



        [Test]
        public void Test_Convex_Closed_Polygon()
        {
            var bspTree = CreateBSPTreeFromPoints(
                new Point(0, 0),
                new Point(10, 10),
                new Point(30, 10),
                new Point(40, 0),
                new Point(40, 30),
                new Point(30, 20),
                new Point(10, 20),
                new Point(0, 30))
                ;

            Assert.That(bspTree.IsOutside(new Point(50, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(50, 10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(50, 20)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(50, 30)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(-10, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 20)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 30)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(0, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(0, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(10, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(10, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(20, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(20, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(30, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(30, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(40, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(0, 0)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 20)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 25)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 30)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(5, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 20)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 25)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(10, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(10, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(10, 20)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(20, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(20, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(20, 20)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(30, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(30, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(30, 20)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(35, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(35, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(35, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(35, 20)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(35, 25)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(40, 0)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(40, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(40, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(40, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(40, 20)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(40, 25)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(40, 30)), Is.False);

        }

        [Test]
        public void Test_Convex_Closed_Polygon_colinear_edges()
        {
            var bspTree = CreateBSPTreeFromPoints(
                new Point(0, 0),
                new Point(10, 10),
                new Point(20, 10),
                new Point(30, 0),
                new Point(30, 30),
                new Point(20, 20),
                new Point(10, 20),
                new Point(0, 30))
                ;

            Assert.That(bspTree.IsOutside(new Point(25, 24)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(25, 26)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(40, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 20)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 30)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(-10, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 20)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 30)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(-10, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(0, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(0, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(10, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(10, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(10, 30)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(10, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(20, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(20, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(20, 30)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(20, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(30, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(30, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(40, -10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 40)), Is.True);

            Assert.That(bspTree.IsOutside(new Point(0, 0)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 20)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 25)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(0, 30)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(5, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 20)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(5, 25)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(10, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(10, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(10, 20)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(20, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(20, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(20, 20)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(25, 5)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(25, 10)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(25, 15)), Is.False);
            Assert.That(bspTree.IsOutside(new Point(25, 20)), Is.False);

            Assert.That(bspTree.IsOutside(new Point(40, 0)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 5)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 10)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 15)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 20)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 25)), Is.True);
            Assert.That(bspTree.IsOutside(new Point(40, 30)), Is.True);


            Assert.That(bspTree.HasCollision(new Point(8, 15), 3), Is.False);
            Assert.That(bspTree.HasCollision(new Point(12, 15), 3), Is.False);

            Assert.That(bspTree.HasCollision(new Point(18, 15), 3), Is.False);
            Assert.That(bspTree.HasCollision(new Point(12, 15), 3), Is.False);


        }


        ITerrain<Polygon> CreateBSPTreeFromPoints(params Point[] points)
        {
            var polygon = new Polygon(points);

            var polyList = new List<Polygon> { polygon };

            List<Polygon> convexSubPolygons = new PolygonSplitter(polygon).GetConvexSubPolygons();
            return new BSPTree<Polygon>(convexSubPolygons);
        }

    }
}
