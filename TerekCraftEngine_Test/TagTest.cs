﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class TagTest : BaseTest
    {

        [Test]
        public void TestTypeTags()
        {
            string tag1 = GetUniqueString();
            string tag2 = GetUniqueString();
            string tag3 = GetUniqueString();

            string name1 = GetUniqueString();
            string name2 = GetUniqueString();
            string name3 = GetUniqueString();

            var type1 = new ActorType(name1, 10, 10);
            var type2 = new ActorType(name2, 10, 10, tags : new HashSet<string> { tag1 });
            var type3 = new ActorType(name3, 10, 10, tags : new HashSet<string> { tag1, tag2, tag3 } );

            Assert.IsFalse( type1.HasTag(tag1) );
            Assert.IsFalse(type1.HasTag(tag2));
            Assert.IsFalse(type1.HasTag(tag3));

            Assert.IsTrue(type2.HasTag(tag1));
            Assert.IsFalse(type2.HasTag(tag2));
            Assert.IsFalse(type2.HasTag(tag3));

            Assert.IsTrue(type3.HasTag(tag1));
            Assert.IsTrue(type3.HasTag(tag2));
            Assert.IsTrue(type3.HasTag(tag3));

            Assert.AreEqual(0, type1.Tags.Count());
            Assert.AreEqual(1, type2.Tags.Count());
            Assert.AreEqual(3, type3.Tags.Count());

            Assert.IsFalse(type1.Tags.Contains(tag1));
            Assert.IsFalse(type1.Tags.Contains(tag2));
            Assert.IsFalse(type1.Tags.Contains(tag3));

            Assert.IsTrue(type2.Tags.Contains(tag1));
            Assert.IsFalse(type2.Tags.Contains(tag2));
            Assert.IsFalse(type2.Tags.Contains(tag3));

            Assert.IsTrue(type3.Tags.Contains(tag1));
            Assert.IsTrue(type3.Tags.Contains(tag2));
            Assert.IsTrue(type3.Tags.Contains(tag3));

            Assert.AreEqual(name1, type1.Name);
            Assert.AreEqual(name2, type2.Name);
            Assert.AreEqual(name3, type3.Name);
        }


        [Test]
        public void TestActionTags()
        {
            string tag1 = GetUniqueString();
            string tag2 = GetUniqueString();
            string tag3 = GetUniqueString();

            var action1 = new MoveAction(10) ;
            var action2 = new MoveAction(10, new HashSet<string> { tag1 }); 
            var action3 = new MoveAction(10, new HashSet<string> { tag1, tag2, tag3 }); 

            Assert.IsFalse(action1.HasTag(tag1));
            Assert.IsFalse(action1.HasTag(tag2));
            Assert.IsFalse(action1.HasTag(tag3));

            Assert.IsTrue(action2.HasTag(tag1));
            Assert.IsFalse(action2.HasTag(tag2));
            Assert.IsFalse(action2.HasTag(tag3));

            Assert.IsTrue(action3.HasTag(tag1));
            Assert.IsTrue(action3.HasTag(tag2));
            Assert.IsTrue(action3.HasTag(tag3));

            Assert.AreEqual(0, action1.Tags.Count());
            Assert.AreEqual(1, action2.Tags.Count());
            Assert.AreEqual(3, action3.Tags.Count());

            Assert.IsFalse(action1.Tags.Contains(tag1));
            Assert.IsFalse(action1.Tags.Contains(tag2));
            Assert.IsFalse(action1.Tags.Contains(tag3));

            Assert.IsTrue(action2.Tags.Contains(tag1));
            Assert.IsFalse(action2.Tags.Contains(tag2));
            Assert.IsFalse(action2.Tags.Contains(tag3));

            Assert.IsTrue(action3.Tags.Contains(tag1));
            Assert.IsTrue(action3.Tags.Contains(tag2));
            Assert.IsTrue(action3.Tags.Contains(tag3));
        }


        [Test]
        public void TestEachActionTags()
        {
            string tag = GetUniqueString();

            Assert.IsTrue(new MoveAction(0, tag).HasTag(tag) );
            Assert.IsTrue(new MoveForwardAction(0, tag).HasTag(tag));

            Assert.IsTrue(new AimAction(0, tag).HasTag(tag));
            Assert.IsTrue(new TurnAction(0, tag).HasTag(tag));

            Assert.IsTrue(new ShootAction(0, 0, tag).HasTag(tag));
            Assert.IsTrue(new ShootForwardAction(0, 0, tag).HasTag(tag));
            Assert.IsTrue(new ShootAimedAction(0, 0, tag).HasTag(tag));
        }

    }
}
