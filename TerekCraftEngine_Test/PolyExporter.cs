﻿using System;
using System.Collections.Generic;
using System.Text;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{
    public class PolyExporter
    {
        private const string VERTEX = "v {0} {1} 0";
        List<Point> points = new List<Point>();
        List<Polygon> polys = new List<Polygon>();

        public PolyExporter()
        {
        }

        public string Export()
        {
            StringBuilder builder = new StringBuilder();

            foreach (var point in points)
            {
                builder.Append(Convert(point));
                builder.Append(Environment.NewLine);
            }

            foreach(var poly in polys)
            {
                builder.Append(Convert(poly));

                builder.Append(Environment.NewLine);
            }

            return builder.ToString();
        }

        private string Convert(Polygon poly)
        {
            var result = ("f ");

            foreach (var point in poly.Points)
            {
                result += (points.IndexOf(point) + 1);
                result += " ";

            }

            return result.Trim();
        }

        public void Add(Point point)
        {
            points.Add(point);
        }

        public string Convert(Point point)
        {
            return String.Format(VERTEX, point.X, point.Y);
        }

        public void Add(Polygon poly)
        {
            foreach(var point in poly.Points)
            {
                points.Add(point);
            }

            polys.Add(poly);
        }
    }
}