﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{

    [TestFixture]
    public class PolyExporterTest : BaseTest
    {
        PolyExporter exporter;
        
        [SetUp]
        public void SetUp()
        {
            exporter = new PolyExporter();
        }


        [Test]
        public void Export_Empty()
        {

            string data = exporter.Export();

            Assert.IsNotNull(data);
            Assert.AreEqual("", data);
        }



        [Test]
        public void Convert_Vertex()
        {
            Assert.AreEqual("v 0 0 0", exporter.Convert(new Point(0, 0)));
            Assert.AreEqual("v 1 1 0", exporter.Convert(new Point(1, 1)));
            Assert.AreEqual("v 5 5 0", exporter.Convert(new Point(5, 5)));
        }

        [Test]
        public void Export_Multiple_Vertex()
        {
            List<Point> input = new List<Point>();

            for (int i = 0; i < TEST_SIZE; i++)
            {
                input.Add(new Point(GetUniqueInt(), GetUniqueInt()));
            }

            foreach (var point in input) exporter.Add(point);

            string data = exporter.Export();

            Assert.IsNotNull(data);

            var lines = SplitIntoLines(data);

            Assert.AreEqual(input.Count, lines.Length);

            for(int i = 0; i < lines.Length; i++)
            {
                Assert.AreEqual(exporter.Convert(input[i]), lines[i], "difference in line:" + i);
            }
        }


        [Test]
        public void Export_Poly()
        {
            List<Point> input = new List<Point>();

            for (int i = 0; i < TEST_SIZE; i++)
            {
                input.Add(new Point(GetUniqueInt(), GetUniqueInt()));
            }

            var poly = new Polygon(input);

            exporter.Add(poly);

            string data = exporter.Export();

            Assert.IsNotNull(data);

            var lines = SplitIntoLines(data);

            Assert.AreEqual(input.Count + 1, lines.Length);

            for (int i = 0; i < input.Count; i++)
            {
                Assert.AreEqual(exporter.Convert(input[i]), lines[i], "difference in line:" + i);
            }

            Assert.AreEqual("f 1 2 3", lines[input.Count]);
        }

        static string[] SplitIntoLines(string data)
        {
            return data.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
