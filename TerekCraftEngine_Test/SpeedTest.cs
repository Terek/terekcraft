﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class SpeedTest : BaseTest
    {
        readonly ActorType _type = new ActorType("tank", 10, 0);

        [Test]
        public void TestZeroSpeed()
        {
            AddActorWithType(_type, 0, 0, 0);

            World.Tick();

            Assert.AreEqual(0, World.GetTickEvents().Count);
        }


        [Test]
        public void TestSpeeds()
        {
            for (double speed = 1f; speed < 5f; speed ++)
            {
                for (double direction = 0f; direction <= 4*Math.PI; direction += Math.PI/8)
                {
                    ValidateSpeedMovement(new Point(0, 0), direction, speed);
                    ValidateSpeedMovement(new Point(1, 1), direction, speed);
                }
            }
        }

        void ValidateSpeedMovement(Point originalLocation, double direction, double speed)
        {
            string actorId = World.AddActor(new Actor(_type)
                {
                    Location = originalLocation,
                    Direction = direction,
                    Aim = 0,
                    Speed = speed
                }).ActorId;

            World.Tick();

            Point expectedLocation = originalLocation.Move(direction, speed);

            ValidateEvents(World.GetTickEvents(), typeof (SpawnEvent), typeof (MoveEvent));

            var moveEvent = GetSingleEventOfTypeForActor<MoveEvent>(World.GetTickEvents(), actorId);

            Assert.IsNotNull(moveEvent);
            Assert.AreEqual(actorId, moveEvent.ActorId);
            Assert.AreEqual(originalLocation, moveEvent.OldLocation);
            Assert.AreEqual(expectedLocation, moveEvent.NewLocation);
        }
    }
}
