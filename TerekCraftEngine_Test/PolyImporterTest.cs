﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class PolyImporterTest : BaseTest
    {
        PolyImporter importer;

        [SetUp]
        public void SetUp()
        {
            importer = new PolyImporter();
        }

        [Test]
        public void ConvertPoint_null()
        {
            Assert.Throws<ArgumentException>( () => importer.ConvertPoint(null));
        }


        [Test]
        public void ConvertPoint_empty()
        {
            Assert.Throws<ArgumentException>(() => importer.ConvertPoint(""));
        }

        [Test]
        public void ConvertPoint_random()
        {
            Assert.Throws<ArgumentException>(() => importer.ConvertPoint(GetUniqueString()));
        }

        [Test]
        public void ConvertPoint_valid()
        {
            double x = GetUniqueInt();
            double y = GetUniqueInt();

            string input = "v " + x + " " + y + " 0"; 

            var point = importer.ConvertPoint(input);

            Assert.IsNotNull(point);
            Assert.AreEqual(x, point.X);
            Assert.AreEqual(y, point.Y);
        }


        [Test]
        public void ConvertPoint_invalid_first()
        {
            string input = "v csoki 0 0";

            Assert.Throws<FormatException>(() => importer.ConvertPoint(input));
        }

        [Test]
        public void ConvertPoint_invalid_second()
        {
            string input = "v 0 csoki 0";

            Assert.Throws<FormatException>(() => importer.ConvertPoint(input));
        }

        [Test]
        public void Convert()
        {
            List<Point> points = new List<Point>();

            for(int i = 0; i < 3; i++)
            {
                points.Add(new Point(GetUniqueInt(), GetUniqueInt()));
            }

            string input = "f 1 2 3";

            var poly = importer.ConvertPoly(input, points);

            Assert.IsNotNull(poly);
            for (int i = 0; i < points.Count; i++)
            {
                Assert.AreEqual(points[i], poly.Points[i]);
            }
        }


        [Test]
        public void ConvertFile()
        {
            importer = new PolyImporter(0, 2);

            double test = GetUniqueInt() / 3.3;

            System.Console.WriteLine(test);

            var reader = new StreamReader(TestContext.CurrentContext.TestDirectory +  "/testAssets/map0.obj");

            var polys = importer.ConvertInput(reader);

            foreach(var poly in polys)
            {
                System.Console.WriteLine(poly);
            }

        }



        [Test]
        public void ConvertFile2()
        {
            importer = new PolyImporter(0, 2);

            double test = GetUniqueInt() / 3.3;

            System.Console.WriteLine(test);

            var reader = new StreamReader(TestContext.CurrentContext.TestDirectory + "/testAssets/map2.txt");

            var polys = importer.ConvertInput(reader);

            foreach (var poly in polys)
            {
                System.Console.WriteLine(poly);
            }

        }




    }
}
