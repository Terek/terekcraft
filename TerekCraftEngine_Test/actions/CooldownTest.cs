﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test.actions
{
    class CooldownTest : BaseTest
    {
        const int CooldownNoCooldown = 0;
        const int CooldownNormal = 5;
        const int CooldownLong = 10;
        
        [Test]
        public void Cooldown_cant_shoot_with_active_cooldown()
        {
            ActorType _actorType = new ActorType(
                name: "tank",
                maxHitPoints: 10,
                size: 1,
                actions: new BaseAction[] { new ShootForwardAction(damage: 1, range: 10) { Cooldown = CooldownNormal } }
            );

            var _actorId = AddActorWithType(_actorType, 0, 0, 0);
            World.Tick();

            ValidateCooldown(CooldownNormal, _actorId);
        }

        [Test]
        public void Cooldown_cant_shoot_with_active_cooldown_long_cooldown()
        {
            ActorType _actorType = new ActorType(
                name: "tank",
                maxHitPoints: 10,
                size: 1,
                actions: new BaseAction[] { new ShootForwardAction(damage: 1, range: 10) { Cooldown = CooldownLong } }
            );

            var _actorId = AddActorWithType(_actorType, 0, 0, 0);

            ValidateCooldown(CooldownLong, _actorId);
        }

        [Test]
        public void Cooldown_no_cooldown()
        {
            ActorType _actorType = new ActorType(
                name: "tank",
                maxHitPoints: 10,
                size: 1,
                actions: new BaseAction[] { new ShootForwardAction(damage: 1, range: 10) { Cooldown = CooldownNoCooldown } }
            );

            var _actorId = AddActorWithType(_actorType, 0, 0, 0);

            ValidateCooldown(CooldownNoCooldown, _actorId);
        }

        protected void ValidateCooldown(int cooldown, string actorId)
        {
            World.ShootForward(actorId);
            World.Tick();
            
            ValidateEvents(World.GetTickEvents(), typeof(ShootEvent));

            for (int i = 0; i < cooldown; i++)
            {
                World.ShootForward(actorId);
                World.Tick();
                ValidateEvents(World.GetTickEvents());
            }

            World.ShootForward(actorId);
            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(ShootEvent));
        }


    }
}
