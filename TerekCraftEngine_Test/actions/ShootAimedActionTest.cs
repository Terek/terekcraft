﻿using System.Collections.Generic;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;

namespace TerekCraftEngine_Test.actions
{
    [TestFixture]
    public class ShootAimedActionTest : ShootActionTestBase
    {
        protected override IEnumerable<BaseAction> GetActions(int damage, double range, int cooldown)
        {
            yield return new AimAction(10);
            yield return new ShootAimedAction(damage, range){Cooldown = cooldown};
        }

        protected override void DoShootAction(double direction)
        {
            World.Aim(_actorId, direction);
            World.Tick();
            World.ShootAimed(_actorId);
            World.Tick();
        }
    }

    [TestFixture]
    public class ShootAimedActionTestWithTurn : ShootActionTestBase
    {
        protected override IEnumerable<BaseAction> GetActions(int damage, double range, int cooldown)
        {
            yield return new AimAction(10);
            yield return new TurnAction(10);
            yield return new ShootAimedAction(damage, range){Cooldown = cooldown};
        }

        protected override void DoShootAction(double direction)
        {
            World.Turn(_actorId, direction);
            World.Tick();
            World.ShootAimed(_actorId);
            World.Tick();
        }
    }

}