﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test.actions
{
    [TestFixture]
    public class AimActionTest : BaseTest
    {
        const double MaxSpeed1 = 1;
        const double MaxSpeed2 = 1.5;
        readonly ActorType _actorType1 = new ActorType("tank", 10, 10, new List<BaseAction>() { new AimAction(MaxSpeed1) });
        readonly ActorType _actorType2 = new ActorType("tank", 10, 10, new List<BaseAction>() { new AimAction(MaxSpeed2) });

        string _actorId;

        [SetUp]
        public void InitMoveActionTest()
        {
        }


        [Test]
        public void TestAim()
        {
            _actorId = AddActorWithType(_actorType1);

            for (int i = 0; i < 10; i++)
            {
                ValidateAiming(MaxSpeed1, MaxSpeed1);                
            }

        }

        [Test]
        public void TestAim_negative()
        {
            _actorId = AddActorWithType(_actorType1);

            for (int i = 0; i < 10; i++)
            {
                ValidateAiming(-MaxSpeed1, -MaxSpeed1);
            }

        }

        [Test]
        public void TestAim_maxSpeed()
        {
            _actorId = AddActorWithType(_actorType1);

            for (int i = 0; i < 10; i++)
            {
                ValidateAiming(2 * MaxSpeed1, MaxSpeed1);
            }
        }

        [Test]
        public void TestAim_maxSpeed_negative()
        {
            _actorId = AddActorWithType(_actorType1);

            for (int i = 0; i < 10; i++)
            {
                ValidateAiming(-2 * MaxSpeed1, -MaxSpeed1);
            }
        }


        [Test]
        public void TestAim_different_maxSpeed()
        {
            _actorId = AddActorWithType(_actorType2);

            for (int i = 0; i < 10; i++)
            {
                ValidateAiming(2* MaxSpeed2, MaxSpeed2);
            }
        }



        void ValidateAiming(double diff, double expectedDiff)
        {
            diff = EngineMath.NormalizeAngleDiff(diff);

            double oldAim = World.GetActor(_actorId).Aim;
            World.Aim(_actorId, oldAim + diff);
            World.Tick();

            double expectedAim = oldAim + expectedDiff;

            expectedAim = EngineMath.NormalizeAngle(expectedAim);

            Assert.AreEqual(expectedAim, World.GetActor(_actorId).Aim);
            Assert.AreEqual(1, World.GetTickEvents().Count);

            AimEvent aimEvent = (AimEvent) World.GetTickEvents()[0];
            Assert.AreEqual(_actorId, aimEvent.ActorId);
            Assert.AreEqual(expectedAim, aimEvent.NewDirection);
            Assert.AreEqual(oldAim, aimEvent.OldDirection);
        }
    }
}
