﻿using System;
using System.Linq;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;
using System.Collections.Generic;

namespace TerekCraftEngine_Test.actions
{
    [TestFixture]
    public class TurnActionTest : BaseTest
    {
        const double MaxSpeed = 1;
        const double MaxSpeed2 = 1.5;
        readonly ActorType _actorType = new ActorType("tank", 10, 10, new List<BaseAction>() { new TurnAction(MaxSpeed) } );
        readonly ActorType _actorType2 = new ActorType("tank", 10, 10, new List<BaseAction>() { new TurnAction(MaxSpeed2) } );

        string _actorId;

        [Test]
        public void TestTurn()
        {
            _actorId = AddActorWithType(_actorType);

            for (int i = 0; i < 10; i++)
            {
                ValidateTurning(MaxSpeed, MaxSpeed);
            }

        }

        [Test]
        public void TestTurn_different_maxSpeed()
        {
            _actorId = AddActorWithType(_actorType2);

            for (int i = 0; i < 10; i++)
            {
                ValidateTurning(MaxSpeed2, MaxSpeed2);
            }

        }


        [Test]
        public void TestTurn_negative()
        {
            _actorId = AddActorWithType(_actorType);
            for (int i = 0; i < 10; i++)
            {
                ValidateTurning(-MaxSpeed, -MaxSpeed);
            }

        }

        [Test]
        public void TestTurn_maxSpeed()
        {
            _actorId = AddActorWithType(_actorType);
            for (int i = 0; i < 10; i++)
            {
                ValidateTurning(2 * MaxSpeed, MaxSpeed);
            }
        }

        [Test]
        public void TestTurn_maxSpeed_negative()
        {
            _actorId = AddActorWithType(_actorType);
            for (int i = 0; i < 10; i++)
            {
                ValidateTurning(-2 * MaxSpeed, -MaxSpeed);
            }
        }


        void ValidateTurning(double diff, double expectedDiff)
        {
            double oldDirection = World.GetActor(_actorId).Direction;
            World.Turn(_actorId, oldDirection + diff);
            World.Tick();

            double expectedDirection = oldDirection + expectedDiff;

            expectedDirection = EngineMath.NormalizeAngle(expectedDirection);

            Assert.AreEqual(expectedDirection, World.GetActor(_actorId).Direction);
            Assert.AreEqual(1, World.GetTickEvents().Count);

            TurnEvent turnEvent = (TurnEvent)World.GetTickEvents()[0];
            Assert.AreEqual(_actorId, turnEvent.ActorId);
            Assert.AreEqual(expectedDirection, turnEvent.NewDirection);
            Assert.AreEqual(oldDirection, turnEvent.OldDirection);
        }

    }
}
