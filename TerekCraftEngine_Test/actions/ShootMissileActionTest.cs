﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test.actions
{
    [TestFixture]
    public class ShootMissileActionTest : BaseTest
    {
        const double Speed = 5f;
        const double Radius = 10f;
        const int Damage = 5;
        const int TimeToLive = 100;

        ActorType _type = new ActorType("tank", 10, 2, new List<BaseAction>() { new TurnAction(10), _shootMissileAction });

        static ShootMissileAction _shootMissileAction = new ShootMissileAction(Speed, Radius, Damage, TimeToLive, GetUniqueString(), GetUniqueString(), GetUniqueString());


        [Test]
        public void TestFireMissile()
        {
            const int direction = 0;

            string actorId = AddActorWithType(_type, 0, 0, 0);

            World.ShootMissile(actorId, direction);

            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(SpawnEvent));
            var spawnEvent = GetSingleEventOfType<SpawnEvent>(World.GetTickEvents());
            Assert.AreEqual(Speed, spawnEvent.Actor.Speed);

            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(MoveEvent));
        }
    }
}
