﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test.actions
{
    [TestFixture]
    public class ShootForwardActionTest : ShootActionTestBase
    {
        protected override IEnumerable<BaseAction> GetActions(int damage, double range, int cooldown)
        {
            yield return new TurnAction(10);
            yield return new ShootForwardAction(damage, range){Cooldown = cooldown};
        }

        protected override void DoShootAction(double direction)
        {
            World.Turn(_actorId, direction);
            World.Tick();
            World.ShootForward(_actorId);
            World.Tick();
        }
    }

    [TestFixture]
    public class ShootActionTest : ShootActionTestBase
    {
        protected override IEnumerable<BaseAction> GetActions(int damage, double range, int cooldown)
        {
            yield return new ShootAction(damage, range){Cooldown = cooldown};
        }

        protected override void DoShootAction(double direction)
        {
            World.Shoot(_actorId, direction);
            World.Tick();
        }
    }


    public abstract class ShootActionTestBase : BaseTest
    {
        ActorType _actorType;
        ActorType _actorTypeOneHitDies;
        ActorType _actorTypeBigDamage;
        ActorType _actorTypeSmall;
        ActorType _actorTypeLongRange;
        ActorType _actorTypeLongCooldown;
        ActorType _actorTypeNoCooldown;

        protected string _actorId;
        const int HitPointsNormal = 10;
        const int HitPointsWeak = 1;
        const double SizeNormal = 3;
        const double SizeSmall = 1;
        const int DamageNormal = 1;
        const int DamageBig = 5;
        const int RangeNormal = 10;
        const int RangeLong = 200;
        const int CooldownNoCooldown = 0;
        const int CooldownNormal = 5;
        const int CooldownLong = 10;

        [SetUp]
        public void InitShootActionTestBase()
        {
            _actorType = new ActorType("tank", HitPointsNormal, SizeNormal, GetActions(DamageNormal, RangeNormal, CooldownNormal));
            _actorTypeOneHitDies = new ActorType("tank", HitPointsWeak, SizeNormal, GetActions(DamageNormal, RangeNormal, CooldownNormal));
            _actorTypeBigDamage = new ActorType("tank", HitPointsNormal, SizeNormal, GetActions(DamageBig, RangeNormal, CooldownNormal));
            _actorTypeSmall = new ActorType("tank", HitPointsNormal, SizeSmall, GetActions(DamageNormal, RangeNormal, CooldownNormal));
            _actorTypeLongRange = new ActorType("tank", HitPointsNormal, SizeNormal, GetActions(DamageNormal, RangeLong, CooldownNormal));
            _actorTypeLongCooldown = new ActorType("tank", HitPointsNormal, SizeNormal, GetActions(DamageNormal, RangeNormal, CooldownLong));
            _actorTypeNoCooldown = new ActorType("tank", HitPointsNormal, SizeNormal, GetActions(DamageNormal, RangeNormal, CooldownNoCooldown));
        }


        protected abstract IEnumerable<BaseAction> GetActions(int damage, double range, int cooldown);


        [Test]
        public void Attack_actor_miss()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, -10, 0, 0);

            DoShootAction(0);

            ValidateMiss(victimId);
        }

        [Test]
        public void Attack_actor_miss_wrong_direction()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, -10, 0, 0);

            DoShootAction(1);

            ValidateMiss(victimId);
        }

        protected abstract void DoShootAction(double direction);

        void ValidateMiss(string victimId)
        {
            ValidateDamageOnVictim(0, victimId);
            ValidateMissEvents();
        }

        void ValidateMissEvents()
        {
            List<Event> tickEvents = World.GetTickEvents();

            var shootEvent = GetSingleEventOfType<ShootEvent>(tickEvents);
            ValidateShootEventActor(shootEvent);
        }

        void ValidateDamageOnVictim(int expectedDamage, string victimId)
        {
            Actor actor = World.GetActor(victimId);
            Assert.AreEqual(actor.Type.MaxHitPoints - expectedDamage, actor.HitPoints);
        }

        void ValidateShootEventActor(ShootEvent shootEvent)
        {
            Assert.AreEqual(_actorId, shootEvent.ActorId);

            Actor actor = World.GetActor(_actorId);

            Assert.AreEqual(actor.Location, shootEvent.FromLocation);
        }

        [Test]
        public void Attack_actor_hit()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, 10, 0, 0);

            DoShootAction(0);

            ValidateHit(DamageNormal, victimId);
        }


        [Test]
        public void Attack_actor_hit_multiple_directions()
        {
            _actorId = AddActorWithType(_actorTypeNoCooldown, 0, 0, 0);

            for (int i = 1; i < 8; i++)
            {
                double direction = i*Math.PI/8;

                Point originalPoint = new Point(0, 0);
                Point displacedPoint = originalPoint.Move(direction, 10);

                var victimId = AddActorWithType(_actorTypeSmall, displacedPoint.X, displacedPoint.Y, 0);

                DoShootAction(direction);
                ValidateHit(DamageNormal, victimId);
            }

        }



        void ValidateHit(int damage, string victimId)
        {
            ValidateDamageOnVictim(damage, victimId);

            ValidateHitEvents(damage, victimId);
        }

        void ValidateHitEvents(int damage, string victimId)
        {
            List<Event> tickEvents = World.GetTickEvents();

            var shootEvent = GetSingleEventOfType<ShootEvent>(tickEvents);

            ValidateShootEventActor(shootEvent);

            var damageEvent = GetSingleEventOfType<DamageEvent>(tickEvents);

            Assert.AreEqual(_actorId, damageEvent.ActorId);
            Assert.AreEqual(victimId, damageEvent.Target);
            Assert.AreEqual(damage, damageEvent.Damage);
        }

        [Test]
        public void Attack_actor_hit_dies()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorTypeOneHitDies, 10, 0, 0);

            DoShootAction(0);

            World.Tick();

            Assert.IsNull(World.GetActor(victimId));

            Assert.IsTrue(HasTypeInCollection(World.GetTickEvents(), typeof(DespawnEvent)));

            var despawnEvent = GetSingleEventOfType<DespawnEvent>(World.GetTickEvents());

            Assert.AreEqual(victimId, despawnEvent.ActorId);
        }

        [Test]
        public void DamageTest_damagebig()
        {
            _actorId = AddActorWithType(_actorTypeBigDamage, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, 10, 0, 0);

            DoShootAction(0);

            ValidateHit(DamageBig, victimId);
        }

        [Test]
        public void DamageTest_size_normal_size()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, 12, 0, 0);

            DoShootAction(0);

            ValidateHit(DamageNormal, victimId);
        }

        [Test]
        public void DamageTest_size_normal_size_too_small()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorTypeSmall, 12, 0, 0);

            DoShootAction(0);

            ValidateMiss(victimId);
        }

        [Test]
        public void Range_test_too_far_miss()
        {
            _actorId = AddActorWithType(_actorType, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, 200, 0, 0);

            DoShootAction(0);

            ValidateMiss(victimId);
        }

        [Test]
        public void Range_test_longer_range_hit()
        {
            _actorId = AddActorWithType(_actorTypeLongRange, 0, 0, 0);
            var victimId = AddActorWithType(_actorType, 200, 0, 0);

            DoShootAction(0);

            ValidateDamageOnVictim(DamageNormal, victimId);
        }
    }

}
