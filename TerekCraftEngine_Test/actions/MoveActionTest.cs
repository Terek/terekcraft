﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test.actions
{
    [TestFixture]
    public class MoveActionTest : MoveActionTestBase
    {
        protected override void DoAction(double direction, double distance)
        {
            World.Move(ActorId, direction, distance);
            World.Tick();
        }

        protected override IEnumerable<BaseAction> GetActions(double maxSpeed)
        {
            yield return new MoveAction(maxSpeed);
        }
    }

    [TestFixture]
    public class MoveForwardActionTest : MoveActionTestBase
    {
        protected override void DoAction(double direction, double distance)
        {
            World.Turn(ActorId, direction);
            World.Tick();
            World.MoveForward(ActorId, distance);
            World.Tick();
        }

        protected override IEnumerable<BaseAction> GetActions(double maxSpeed)
        {
            yield return new TurnAction(10);
            yield return new MoveForwardAction(maxSpeed);
        }
    }


    public abstract class MoveActionTestBase : BaseTest
    {
        protected const double MaxSpeed = 1;
        protected const double MaxSpeed2 = 2;
        protected ActorType ActorType1;
        protected ActorType ActorType2;

        protected abstract IEnumerable<BaseAction> GetActions(double maxSpeed);
        protected abstract void DoAction(double direction, double distance);


        protected string ActorId;

        [SetUp]
        public void InitMoveActionTestBase()
        {
            ActorType1 = new ActorType("tank", 10, 10, GetActions(MaxSpeed));
            ActorType2 = new ActorType("tank", 10, 10, GetActions(MaxSpeed2));
        }


        [Test]
        public void TestMovement()
        {
            ActorId = AddActorWithType(ActorType1);

            for (int i = 0; i < 16; i++)
            {
                ValidateMovement(i * Math.PI / 8, MaxSpeed, MaxSpeed);
            }
        }

        [Test]
        public void TestMovement_different_maxSpeed()
        {
            ActorId = AddActorWithType(ActorType2);

            for (int i = 0; i < 16; i++)
            {
                ValidateMovement(i * Math.PI / 8, MaxSpeed2, MaxSpeed2);
            }
        }

        [Test]
        public void TestMovement_MaxSpeed()
        {
            ActorId = AddActorWithType(ActorType1);

            for (int i = 0; i < 16; i++)
            {
                ValidateMovement(i * Math.PI / 8, 2 * MaxSpeed, MaxSpeed);
            }
        }

        [Test]
        public void TestMovement_reverse()
        {
            ActorId = AddActorWithType(ActorType1);

            for (int i = 0; i < 16; i++)
            {
                ValidateMovement(i * Math.PI / 8, -MaxSpeed, -MaxSpeed);
            }
        }

        [Test]
        public void TestMovement_MaxSpeed_reverse()
        {
            ActorId = AddActorWithType(ActorType1);

            for (int i = 0; i < 16; i++)
            {
                ValidateMovement(i * Math.PI / 8, -2 * MaxSpeed, -MaxSpeed);
            }
        }

        void ValidateMovement(double direction, double distance, double actualDistance )
        {
            Point originalLocation = World.GetActor(ActorId).Location;

            DoAction(direction, distance);

            Point expectedLocation = originalLocation.Move(direction, actualDistance);
            Assert.AreEqual(expectedLocation, World.GetActor(ActorId).Location);

            Assert.AreEqual(1, World.GetTickEvents().Count);

            var moveEvent = (MoveEvent) World.GetTickEvents()[0];

            Assert.AreEqual(ActorId, moveEvent.ActorId);
            Assert.AreEqual(originalLocation, moveEvent.OldLocation);
            Assert.AreEqual(expectedLocation, moveEvent.NewLocation);
        }
    }
}
