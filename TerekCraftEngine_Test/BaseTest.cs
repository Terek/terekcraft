﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test
{
    public class BaseTest
    {
        public const int TEST_SIZE = 3;


        protected World World;

        static List<Type> _eventTypes;

        [SetUp]
        public void InitTests()
        {
            World = InitWorld();

            _eventTypes = new List<Type>
                {
                    typeof (SpawnEvent),
                    typeof (TurnEvent),
                    typeof (MoveEvent),
                    typeof (ShootEvent),
                    typeof (DespawnEvent)
                };
        }


        static int _uniqueInt = 1000;

        protected static string GetUniqueString()
        {
            return Guid.NewGuid().ToString();
        }

        protected int GetUniqueInt()
        {
            return ++_uniqueInt;
        }

        protected static World InitWorld()
        {
            return new World();
        }


        protected static bool HasTypeInCollection(IEnumerable<Event> tickEvents, Type type)
        {
            return tickEvents.Any(tickEvent => tickEvent.GetType() == type);
        }


        protected static void ValidateEvents(List<Event> tickEvents, params Type[] expectedTypes)
        {
            Assert.IsNotNull(tickEvents);

            var notNeededEventTypes = _eventTypes.Where(item => !expectedTypes.Contains(item));

            foreach (var type in expectedTypes)
            {
                Assert.IsTrue(HasTypeInCollection(tickEvents, type));
            }

            foreach (var type in notNeededEventTypes)
            {
                Assert.IsFalse(HasTypeInCollection(tickEvents, type));
            }
        }

        protected static T GetSingleEventOfType<T>(IEnumerable<Event> tickEvents) where T : Event
        {
            var tickEvent = tickEvents.Single(item => item.GetType() == typeof(T)) as T;
            return tickEvent;
        }

        protected static T GetSingleEventOfTypeForActor<T>(IEnumerable<Event> tickEvents, string actorId) where T : Event
        {
            var tickEvent = tickEvents.Single(item =>
                {
                    if (item is Event)
                    {
                        var actorEvent = item as Event;
                        return item.GetType() == typeof(T) && actorEvent.ActorId == actorId;
                    }
                    return false;
                } ) as T;
            return tickEvent;
        }

        protected string AddActorWithType(ActorType actorType)
        {
            var actor = new Actor(actorType)
            {
                Location = new Point(0, 0),
                Direction = 0,
                Aim = 0
            };


            string actorId = World.AddActor(actor).ActorId;

            World.Tick();

            return actorId;
        }

        protected string AddActorWithType(ActorType type, double x, double y, double direction)
        {
            string actorId = World.AddActor(new Actor(type) { Location = new Point(x, y), Direction = direction, Aim = 0 }).ActorId;

            World.Tick();

            return actorId;
        }

    }

    public class TestInitializeAttribute : Attribute
    {
    }
}
