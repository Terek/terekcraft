﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class PolygonTest : BaseTest
    {
        [Test]
        public void Test_Polygon_3_edges_points()
        {
            Point point1 = new Point(0, 0);
            Point point2 = new Point(1, 0);
            Point point3 = new Point(0, 1);

            Edge edge1 = new Edge(point1, point2);
            Edge edge2 = new Edge(point2, point3);
            Edge edge3 = new Edge(point3, point1);

            Polygon polygon = new Polygon(point1, point2, point3);

            Assert.AreEqual(3, polygon.Edges.Count());
            Assert.AreEqual(3, polygon.Points.Count());

            Assert.IsTrue(polygon.Edges.Contains(edge1));
            Assert.IsTrue(polygon.Edges.Contains(edge2));
            Assert.IsTrue(polygon.Edges.Contains(edge3));

            Assert.IsTrue(polygon.Points.Contains(point1));
            Assert.IsTrue(polygon.Points.Contains(point2));
            Assert.IsTrue(polygon.Points.Contains(point3));

            Assert.AreEqual(edge1, polygon.Edges[0]);
            Assert.AreEqual(edge2, polygon.Edges[1]);
            Assert.AreEqual(edge3, polygon.Edges[2]);
        }

        [Test]
        public void Test_Polygon_4_edges()
        {
            Point point1 = new Point(0, 0);
            Point point2 = new Point(1, 0);
            Point point3 = new Point(1, 1);
            Point point4 = new Point(0, 1);

            Edge edge1 = new Edge(point1, point2);
            Edge edge2 = new Edge(point2, point3);
            Edge edge3 = new Edge(point3, point4);
            Edge edge4 = new Edge(point4, point1);

            Polygon polygon = new Polygon(point1, point2, point3, point4);

            Assert.AreEqual(4, polygon.Edges.Count());
            Assert.AreEqual(4, polygon.Points.Count());

            Assert.IsTrue(polygon.Edges.Contains(edge1));
            Assert.IsTrue(polygon.Edges.Contains(edge2));
            Assert.IsTrue(polygon.Edges.Contains(edge3));
            Assert.IsTrue(polygon.Edges.Contains(edge4));

            Assert.IsTrue(polygon.Points.Contains(point1));
            Assert.IsTrue(polygon.Points.Contains(point2));
            Assert.IsTrue(polygon.Points.Contains(point3));
            Assert.IsTrue(polygon.Points.Contains(point4));

            Assert.AreEqual(edge1, polygon.Edges[0]);
            Assert.AreEqual(edge2, polygon.Edges[1]);
            Assert.AreEqual(edge3, polygon.Edges[2]);
            Assert.AreEqual(edge4, polygon.Edges[3]);
        }

        [Test]
        public void Test_Polygon_Factory()
        {
            Point point1 = new Point(0, 0);
            Point point2 = new Point(1, 0);
            Point point3 = new Point(0, 1);

            Polygon polygon = new Polygon(point1, point2, point3);

            Assert.AreEqual(3, polygon.Edges.Count());

            Assert.IsTrue(polygon.Edges.Contains(new Edge(point1, point2)));
            Assert.IsTrue(polygon.Edges.Contains(new Edge(point2, point3)));
            Assert.IsTrue(polygon.Edges.Contains(new Edge(point3, point1)));
        }


        [Test]
        public void Test_Polygon_IsConvex_3_point()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(1, 0), new Point(0, 1));
            Assert.IsTrue(polygon.IsConvex());
        }

        [Test]
        public void Test_Polygon_IsConvex_4_point()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1));
            Assert.IsTrue(polygon.IsConvex());
        }

        [Test]
        public void Test_Polygon_IsConvex_4_point_in_line()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(5, 0), new Point(10, 0), new Point(0, 1));
            Assert.IsTrue(polygon.IsConvex());
        }

        [Test]
        public void Test_Polygon_NotConvex_4_point()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(10, 0), new Point(1, 1), new Point(0, 10));
            Assert.IsFalse(polygon.IsConvex());
        }


        [Test]
        public void Test_Polygon_NotConvex_4_point_edges_crossing()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(10, 0), new Point(0, 10), new Point(10, 10));
            Assert.IsFalse(polygon.IsConvex());
        }


        [Test]
        public void Test_HasCommonEdge_NoCommonEdge()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(0, 10) );
            Polygon polygon2 = new Polygon(new Point(20, 0), new Point(30, 0), new Point(20, 10) );

            Assert.IsFalse(polygon1.HasCommonEdge(polygon2));
            Assert.IsFalse(polygon2.HasCommonEdge(polygon1));
        }


        [Test]
        public void Test_HasCommonEdge_Intersecting()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10));
            Polygon polygon2 = new Polygon(new Point(0, 20), new Point(5, 5), new Point(10, 20));

            Assert.IsFalse(polygon1.HasCommonEdge(polygon2));
            Assert.IsFalse(polygon2.HasCommonEdge(polygon1));
        }

        [Test]
        public void Test_HasCommonEdge_CommonEdge()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10));
            Polygon polygon2 = new Polygon(new Point(0, 10), new Point(20, 0), new Point(20, 10), new Point(10, 10));

            Assert.IsTrue(polygon1.HasCommonEdge(polygon2));
            Assert.IsTrue(polygon2.HasCommonEdge(polygon1));
        }

        [Test]
        public void Test_HasCommonEdge_ColinearEdge()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10));
            Polygon polygon2 = new Polygon(new Point(-10, 10), new Point(20, 10), new Point(10, 20), new Point(0, 20));

            Assert.IsFalse(polygon1.HasCommonEdge(polygon2));
            Assert.IsFalse(polygon2.HasCommonEdge(polygon1));
        }

        [Test]
        public void Test_HasCommonEdge_OneCommonPoint_NotTouching()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10));
            Polygon polygon2 = new Polygon(new Point(0, 0), new Point(0, -10), new Point(-10, -10), new Point(0, -10));

            Assert.IsFalse(polygon1.HasCommonEdge(polygon2));
            Assert.IsFalse(polygon2.HasCommonEdge(polygon1));
        }


        [Test]
        public void Test_Equals()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1));
            Polygon polygon2 = new Polygon(new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1));

            Assert.IsTrue(polygon1.Equals(polygon2));
            Assert.IsTrue(polygon2.Equals(polygon1));
        }


        [Test]
        public void Test_Equals_NotEquals()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1));
            Polygon polygon2 = new Polygon(new Point(10, 0), new Point(11, 0), new Point(11, 1), new Point(10, 1));

            Assert.IsFalse(polygon1.Equals(polygon2));
            Assert.IsFalse(polygon2.Equals(polygon1));
        }


        [Test]
        public void Test_Equals_OneCommonPoint_NotEquals()
        {
            Polygon polygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10));
            Polygon polygon2 = new Polygon(new Point(0, 0), new Point(0, -10), new Point(-10, -10), new Point(0, -10));

            Assert.IsFalse(polygon1.Equals(polygon2));
            Assert.IsFalse(polygon2.Equals(polygon1));
        }

        [Test]
        public void Test_PolygonCentroid()
        {
            Polygon triangle = new Polygon(new Point(0, 0), new Point(10, 0), new Point(0, 10));
            Polygon square = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10));

            Assert.AreEqual(new Point(5, 5) , square.GetCentroid());
            Assert.AreEqual(new Point(10.0 / 3, 10.0 / 3), triangle.GetCentroid());
        }

    }
}