﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;
using TerekCraftEngine.modifiers;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class ModifierTest : BaseTest
    {
        ActorType _type = new ActorType("tank", 10, 1);

        [Test]
        public void TestTimeToLive()
        {
            var actor = new Actor(_type) { Location = new Point(0,0), Direction = 0, Aim = 0 };
            var timeToLive = new TimeToLive(2, GetUniqueString(), GetUniqueString(), GetUniqueString());

            actor.AddModifier(timeToLive);

            string actorToMove = World.AddActor(actor).ActorId;

            World.Tick();
            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(DespawnEvent));
            var despawnEvent = GetSingleEventOfType<DespawnEvent>(World.GetTickEvents());
            Assert.AreEqual(actorToMove, despawnEvent.ActorId);
            Assert.AreEqual(actor.Location, despawnEvent.Location);
            Assert.IsTrue(timeToLive.HasAllTag(despawnEvent.Tags));

            Assert.IsNull(World.GetActor(actorToMove));
        }


        [Test]
        public void TestTimeToDetonate()
        {
            var actor = new Actor(_type) { Location = new Point(0, 0), Direction = 0, Aim = 0 };
            var timeToExplode = new TimeToExplode(2, 5, 5, GetUniqueString(), GetUniqueString(), GetUniqueString());

            actor.AddModifier(timeToExplode);

            string victimActor = AddActorWithType(_type, 2, 0, 0);

            string actorToExplode = World.AddActor(actor).ActorId;

            World.Tick();
            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(DespawnEvent), typeof(DamageEvent));
            var despawnEvent = GetSingleEventOfType<DespawnEvent>(World.GetTickEvents());
            Assert.AreEqual(actorToExplode, despawnEvent.ActorId);
            Assert.AreEqual(actor.Location, despawnEvent.Location);
            Assert.IsTrue(timeToExplode.HasAllTag(despawnEvent.Tags));
            Assert.IsTrue(despawnEvent.Tags.Count() > 0);

            var damageEvent = GetSingleEventOfType<DamageEvent>(World.GetTickEvents());
            Assert.IsTrue(timeToExplode.HasAllTag(damageEvent.Tags));
            Assert.AreEqual(actorToExplode, damageEvent.ActorId);
            Assert.AreEqual(timeToExplode.Damage, damageEvent.Damage);
            Assert.AreEqual(victimActor, damageEvent.Target);

            Assert.IsNull(World.GetActor(actorToExplode));
        }

    }
}
