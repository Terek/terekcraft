﻿using System;
using System.Threading;
using NUnit.Framework;
using TerekCraftEngine;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class POCTest
    {
        [Test]
        public void PocTest()
        {
            int threadCount = 10;
            ThreadPool.SetMaxThreads(threadCount, 4);
            ThreadPool.SetMinThreads(threadCount, 4);

            new POC(10).RunPoc();
        }
    }
}
