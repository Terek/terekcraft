﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class EventTest : BaseTest
    {
        Actor _actor1;
        Actor _actor2;
        string _actorId1;
        string _actorId2;
        Point _originalLocation;

        ActorType _actorType = new ActorType("tank", 10, 0.5f, new List<BaseAction>() { new MoveForwardAction(1f), new TurnAction(0.5), new ShootForwardAction(1, 20) { Cooldown = 5 } });

        const double Actor1OriginalDirection = 1f;
        const double Actor2OriginalDirection = 1f;

        public void Init()
        {
            _originalLocation = new Point(0, 0);
            _actor1 = new Actor(_actorType) { Direction = Actor1OriginalDirection, Location = _originalLocation, HitPoints = 1 };
            _actor2 = new Actor(_actorType) { Direction = Actor2OriginalDirection, Location = _originalLocation, HitPoints = 2 };

            _actorId1 = World.AddActor(_actor1).ActorId;
            _actorId2 = World.AddActor(_actor2).ActorId;
        }

        [Test]
        public void Event_emptyWorld_noEvent()
        {
            World.Tick();

            IEnumerable<Event> tickEvents = World.GetTickEvents();

            Assert.IsNotNull(tickEvents);
            Assert.IsTrue(!tickEvents.Any());
        }


        [Test]
        public void Event_nothingHappened_noEvent()
        {
            Init();

            World.Tick();
            World.Tick();

            List<Event> tickEvents = World.GetTickEvents();

            ValidateEvents(tickEvents);
        }


        [Test]
        public void Event_afterAddActor()
        {
            Init();
            World.Tick();

            List<Event> tickEvents = World.GetTickEvents();

            ValidateEvents(tickEvents, typeof(SpawnEvent));

            var spawnEvent = tickEvents.First() as SpawnEvent;

            Assert.IsNotNull(spawnEvent);

            Assert.IsTrue(World.GetActor(_actorId1).CompletelyEquals( spawnEvent.Actor) );
        }


        [Test]
        public void Event_afterShoot_hit()
        {
        }


        [Test]
        public void Event_afterShoot_kill()
        {
            Init();
            World.MoveForward(_actorId1, 10f);
            World.Tick();

            var victimLocation = World.GetActor(_actorId1).Location;

            World.ShootForward(_actorId2);
            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(ShootEvent));

            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(DespawnEvent));

            var despawnEvent = GetSingleEventOfType<DespawnEvent>(World.GetTickEvents());

            Assert.AreEqual(_actorId1, despawnEvent.ActorId);

            Assert.AreEqual(victimLocation, despawnEvent.Location);
        }

    }
}
