﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{

    [TestFixture]
    public class NavMeshTest
    {
        [Test]
        public void TestEmpty()
        {
            ICollection<Polygon> polygons = new List<Polygon>();

            var navMesh = new NavigationMesh<Polygon>(polygons);

            Assert.IsNotNull(navMesh.NodeMap);
        }

        [Test]
        public void Test_One_Node()
        {
            var poly = PolyFactory.CreatePoly(0, 0);

            ICollection<Polygon> polygons = new List<Polygon> { poly };

            var navMesh = new NavigationMesh<Polygon>(polygons);

            ValidateNavMeshNodes(navMesh, polygons);

        }

        [Test]
        public void Test_TwoNodes_NotNeighbors()
        {
            var poly1 = PolyFactory.CreatePoly(0, 0);
            var poly2 = PolyFactory.CreatePoly(5, 0);

            ICollection<Polygon> polygons = new List<Polygon> { poly1, poly2 };
            var navMesh = new NavigationMesh<Polygon>(polygons);

            ValidateNavMeshNodes(navMesh, poly1, poly2);
            AssertPolysAreNotNeighbors(navMesh, poly1, poly2);

        }


        [Test]
        public void Test_TwoNodes_Neighbors()
        {
            var poly1 = PolyFactory.CreatePoly(0, 0);
            var poly2 = PolyFactory.CreatePoly(1, 0);

            ICollection<Polygon> polygons = new List<Polygon> { poly1, poly2 };


            var navMesh = new NavigationMesh<Polygon>(polygons);

            ValidateNavMeshNodes(navMesh, poly1, poly2);
            AssertPolysAreNeighbors(navMesh, poly1, poly2);
        }


        [Test]
        public void Test_FourNodes_Line()
        {
            var poly1 = PolyFactory.CreatePoly(0, 0);
            var poly2 = PolyFactory.CreatePoly(1, 0);
            var poly3 = PolyFactory.CreatePoly(2, 0);
            var poly4 = PolyFactory.CreatePoly(3, 0);

            ICollection<Polygon> polygons = new List<Polygon> { poly1, poly2, poly3, poly4 };

            var navMesh = new NavigationMesh<Polygon>(polygons);

            ValidateNavMeshNodes(navMesh, poly1, poly2, poly3, poly4);
            AssertPolysAreNeighbors(navMesh, poly1, poly2);
            AssertPolysAreNeighbors(navMesh, poly2, poly3);
            AssertPolysAreNeighbors(navMesh, poly3, poly4);

            AssertPolysAreNotNeighbors(navMesh, poly1, poly3);
            AssertPolysAreNotNeighbors(navMesh, poly1, poly4);
            AssertPolysAreNotNeighbors(navMesh, poly2, poly4);

            List<Edge> expectedPath = new List<Edge> { poly1.CommonEdge(poly2), poly2.CommonEdge(poly3), poly3.CommonEdge(poly4) };
            AssertPath(poly1, poly4, navMesh, expectedPath);
        }

        static void AssertPath(Polygon source, Polygon target, NavigationMesh<Polygon> navMesh, List<Edge> expectedPath)
        {
            Assert.AreEqual(expectedPath, navMesh.GetPath(source, target));
            Assert.IsTrue(navMesh.HasPath(source, target));
        }

        [Test]
        public void Test_FourNodes_Forest()
        {
            var poly1 = PolyFactory.CreatePoly(0, 0);
            var poly2 = PolyFactory.CreatePoly(1, 0);
            var poly3 = PolyFactory.CreatePoly(0, 5);
            var poly4 = PolyFactory.CreatePoly(1, 5);

            ICollection<Polygon> polygons = new List<Polygon> { poly1, poly2, poly3, poly4 };

            var navMesh = new NavigationMesh<Polygon>(polygons);

            ValidateNavMeshNodes(navMesh, poly1, poly2, poly3, poly4);
            AssertPolysAreNeighbors(navMesh, poly1, poly2);
            AssertPolysAreNeighbors(navMesh, poly3, poly4);

            AssertPolysAreNotNeighbors(navMesh, poly1, poly3);
            AssertPolysAreNotNeighbors(navMesh, poly1, poly4);
            AssertPolysAreNotNeighbors(navMesh, poly2, poly3);
            AssertPolysAreNotNeighbors(navMesh, poly2, poly4);

            Assert.AreEqual(poly1.CommonEdge(poly2), navMesh.GetPath(poly1, poly2).Single());

            AssertNoPath(poly1, poly3, navMesh);
            AssertNoPath(poly1, poly4, navMesh);

            AssertNoPath(poly2, poly3, navMesh);
            AssertNoPath(poly2, poly4, navMesh);
        }



        [Test]
        public void Test_Circle()
        {
            var poly1 = PolyFactory.CreatePoly(0, 0);
            var poly2 = PolyFactory.CreatePoly(1, 0);
            var poly3 = PolyFactory.CreatePoly(2, 0);
            var poly4 = PolyFactory.CreatePoly(2, 1);
            var poly5 = PolyFactory.CreatePoly(2, 2);
            var poly6 = PolyFactory.CreatePoly(1, 2);
            var poly7 = PolyFactory.CreatePoly(0, 2);
            var poly8 = PolyFactory.CreatePoly(0, 1);

            ICollection<Polygon> polygons = new List<Polygon>
            {
                poly1,
                poly2,
                poly3,
                poly4,
                poly5,
                poly6,
                poly7,
                poly8
            };

            var navMesh = new NavigationMesh<Polygon>(polygons);

            List<Edge> expectedPath = new List<Edge> {
                poly1.CommonEdge(poly2),
                poly2.CommonEdge(poly3),
                poly3.CommonEdge(poly4)
            };

            AssertPath(poly1, poly4, navMesh, expectedPath);

            expectedPath = new List<Edge> {
                poly1.CommonEdge(poly8),
                poly8.CommonEdge(poly7),
                poly7.CommonEdge(poly6)
            };
            AssertPath(poly1, poly6, navMesh, expectedPath);
        }




        static void AssertNoPath(Polygon poly1, Polygon poly2, NavigationMesh<Polygon> navMesh)
        {
            Assert.IsFalse(navMesh.HasPath(poly1, poly2));
            Assert.IsFalse(navMesh.GetPath(poly1, poly2).Any());
        }

        static void AssertPolysAreNotNeighbors(NavigationMesh<Polygon> navMesh, Polygon poly1, Polygon poly2)
        {
            Assert.IsFalse(navMesh.NodeMap[poly1].Neighbors.Any(item => item.Navigable.Equals(poly2)));
            Assert.IsFalse(navMesh.NodeMap[poly2].Neighbors.Any(item => item.Navigable.Equals(poly1)));
            Assert.IsFalse(navMesh.NodeMap[poly1].AreNeighbors(navMesh.NodeMap[poly2]));
            Assert.IsFalse(navMesh.NodeMap[poly2].AreNeighbors(navMesh.NodeMap[poly1]));
            Assert.IsFalse(navMesh.NodeMap[poly1].AreNeighbors(poly2));
            Assert.IsFalse(navMesh.NodeMap[poly2].AreNeighbors(poly1));

            Assert.IsTrue(navMesh.GetPath(poly1, poly2).Count != 1);
        }


        static void AssertPolysAreNeighbors(NavigationMesh<Polygon> navMesh, Polygon poly1, Polygon poly2)
        {
            Assert.IsTrue(navMesh.NodeMap[poly1].Neighbors.Any(item => item.Navigable.Equals(poly2)));
            Assert.IsTrue(navMesh.NodeMap[poly2].Neighbors.Any(item => item.Navigable.Equals(poly1)));
            Assert.IsTrue(navMesh.NodeMap[poly1].AreNeighbors(navMesh.NodeMap[poly2]));
            Assert.IsTrue(navMesh.NodeMap[poly2].AreNeighbors(navMesh.NodeMap[poly1]));
            Assert.IsTrue(navMesh.NodeMap[poly1].AreNeighbors(poly2));
            Assert.IsTrue(navMesh.NodeMap[poly2].AreNeighbors(poly1));

            Assert.AreEqual(poly1.CommonEdge(poly2), navMesh.GetPath(poly1, poly2).Single() );
            Assert.IsTrue(navMesh.HasPath(poly1, poly2));
        }


        static void ValidateNavMeshNodes(NavigationMesh<Polygon> navMesh, params Polygon[] polygons)
        {
            ValidateNavMeshNodes(navMesh, polygons.ToList());
        }


        static void ValidateNavMeshNodes(NavigationMesh<Polygon> navMesh, ICollection<Polygon> polygons)
        {
            Assert.IsNotNull(navMesh.NodeMap);

            Assert.IsTrue(navMesh.NodeMap.Count == polygons.Count);

            foreach (var polygon in polygons)
            {
                Assert.IsTrue(navMesh.NodeMap.ContainsKey(polygon));
                Assert.IsTrue(navMesh.NodeMap[polygon].Navigable.Equals(polygon));
            }
        }
    }

}
