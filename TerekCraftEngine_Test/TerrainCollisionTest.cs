﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;
using TerekCraftEngine.terrain;
using System.Diagnostics;
using System.IO;
using TerekCraftEngine.actions;
using TerekCraftEngine.collisionhandlers;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class TerrainCollisionTest : BaseTest
    {
        readonly ActorType _actorType = new ActorType("tank", 1, 1, new List<BaseAction>() { new MoveForwardAction(1f), new TurnAction(0.5) });
        readonly ActorType _flyingActorType = new ActorType("tank", 1, 1, new List<BaseAction>() { new MoveForwardAction(1f), new TurnAction(0.5) }, terrainHandler: new FlyingTerrainHandler());

        [SetUp]
        public void SetUp()
        {
            List<Polygon> terrain = new List<Polygon> { PolyFactory.CreatePoly(0, 0, 10) };
            
            World.SetMap(terrain);
        }


        [Test]
        public void Test_NoCollision_MovementSuccess()
        {
            var actorId = AddActorWithType(_actorType, 5, 5, 0);

            World.MoveForward(actorId, 1);

            World.Tick();

            Assert.AreEqual(new Point(6, 5), World.GetActor(actorId).Location);
        }

        [Test]
        public void Test_Collision_NoMovement()
        {
            var originalPoint = new Point(8.5, 5);

            var actorId = AddActorWithType(_actorType, originalPoint.X, originalPoint.Y, 0);

            World.MoveForward(actorId, 1);

            World.Tick();

            Assert.AreEqual(originalPoint, World.GetActor(actorId).Location);
        }

        [Test]
        public void Test_Collision_Flying_MovementSuccess()
        {
            var actorId = AddActorWithType(_flyingActorType, 8.5, 5, 0);

            World.MoveForward(actorId, 1);

            World.Tick();

            Assert.AreEqual(new Point(9.5, 5), World.GetActor(actorId).Location);
        }




    }
}
