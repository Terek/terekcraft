﻿using System;
using NUnit.Framework;
using TerekCraftEngine;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class PointTest : BaseTest
    {
        [Test]
        public void PointTest_Create()
        {
            double x = GetUniqueInt();
            double y = GetUniqueInt();
            var point = new Point(x, y);

            Assert.AreEqual(x , point.X);
            Assert.AreEqual(y , point.Y);
        }

        [Test]
        public void PointTest_Equals()
        {
            double x = GetUniqueInt();
            double y = GetUniqueInt();
            var point1 = new Point(x, y);
            var point2 = new Point(x, y);
            var point3 = new Point(GetUniqueInt(), GetUniqueInt());

            Assert.AreEqual(point1, point2);
            Assert.AreNotEqual(point1, point3);
        }

        [Test]
        public void PointTest_HashCode()
        {
            double x = GetUniqueInt();
            double y = GetUniqueInt();
            var point1 = new Point(x, y);
            var point2 = new Point(x, y);
            var point3 = new Point(GetUniqueInt(), GetUniqueInt());

            Assert.AreEqual(point1.GetHashCode(), point2.GetHashCode());
            Assert.AreNotEqual(point1.GetHashCode(), point3.GetHashCode());
        }

        [Test]
        public void PointTest_Move()
        {
            double x = GetUniqueInt();
            double y = GetUniqueInt();
            var point = new Point(x, y);

            double direction = GetUniqueInt();
            double distance = GetUniqueInt();

            double newX = point.X + Math.Cos(direction) * distance;
            double newY = point.Y + Math.Sin(direction) * distance;

            var expectedPoint = new Point(newX, newY);

            var actualPoint = point.Move(direction, distance);

            Assert.AreEqual(expectedPoint, actualPoint);
        }

        [Test]
        public void PointTest_GetDistance_0_0_to_1_0()
        {
            var pointFrom = new Point(0, 0);
            var pointTo = new Point(1, 0);

            double distance = Math.Sqrt(pointFrom.GetDistanceSquaredTo(pointTo));

            Assert.AreEqual(1, distance);
        }

        [Test]
        public void PointTest_GetDirection_0_0_to_1_0()
        {
            TestGetDirectionAndDistanceWithTargetPoint(1, 0);
        }

        [Test]
        public void PointTest_GetDirection_0_0_to_0_1()
        {
            TestGetDirectionAndDistanceWithTargetPoint(0, 1);
        }

        [Test]
        public void PointTest_GetDirection_0_0_to_0_m1()
        {
            TestGetDirectionAndDistanceWithTargetPoint(0, -1);
        }


        [Test]
        public void PointTest_GetDirection_0_0_to_m1_0()
        {
            TestGetDirectionAndDistanceWithTargetPoint(-1, 0);
        }


        [Test]
        public void PointTest_GetDirection_0_0_to_1_2()
        {
            TestGetDirectionAndDistanceWithTargetPoint(1, 2);
        }

        [Test]
        public void PointTest_GetDirection_0_0_to_m1_2()
        {
            TestGetDirectionAndDistanceWithTargetPoint(-1, 2);
        }

        [Test]
        public void PointTest_GetDirection_0_0_to_1_m2()
        {
            TestGetDirectionAndDistanceWithTargetPoint(1, -2);
        }


        [Test]
        public void PointTest_GetDirection_0_0_to_m1_m2()
        {
            TestGetDirectionAndDistanceWithTargetPoint(-1, -2);
        }

        static void TestGetDirectionAndDistanceWithTargetPoint(int newX, int newY)
        {
            var pointFrom = new Point(0, 0);
            var pointTo = new Point(newX, newY);

            double direction = pointFrom.GetDirectionTo(pointTo);
            double distance = Math.Sqrt(pointFrom.GetDistanceSquaredTo(pointTo));

            var newPoint = pointFrom.Move(direction, distance);

            Assert.AreEqual(pointTo, newPoint);
        }

        [Test]
        public void PointTest_CrossProduct_vertical()
        {
            var point1 = new Point(1, 0);
            var point2 = new Point(0, 1);

            Assert.AreEqual(1, point1.Cross(point2));
        }

        [Test]
        public void PointTest_CrossProduct_parallel()
        {
            var point1 = new Point(1, 1);
            var point2 = new Point(1, 1);

            Assert.AreEqual(0, point1.Cross(point2));
        }

        [Test]
        public void PointTest_CrossProduct_parallel_differentDirection()
        {
            var point1 = new Point(1, 1);
            var point2 = new Point(-1, -1);

            Assert.AreEqual(0, point1.Cross(point2));
        }

        [Test]
        public void PointTest_CrossProduct_anticommutative()
        {
            var point1 = new Point(1, 1);
            var point2 = new Point(2, 2);

            Assert.AreEqual(point1.Cross(point2), point2.GetNegative().Cross(point1));
        }

        [Test]
        public void PointTest_Negative_01()
        {
            var point1 = new Point(0, 1);

            Assert.AreEqual(new Point(0, -1), point1.GetNegative());
        }

        [Test]
        public void PointTest_Negative_10()
        {
            var point1 = new Point(1, 0);

            Assert.AreEqual(new Point(-1, 0), point1.GetNegative());
        }

        [Test]
        public void PointTest_Negative_11()
        {
            var point1 = new Point(1, 1);

            Assert.AreEqual(new Point(-1, -1), point1.GetNegative());
        }

        [Test]
        public void PointTest_GetVectorTo_00()
        {
            var point1 = new Point(0, 0);

            Assert.AreEqual(new Point(0, 0), point1.GetVectorTo(new Point(0, 0)));
            Assert.AreEqual(new Point(1, 0), point1.GetVectorTo(new Point(1, 0)));
            Assert.AreEqual(new Point(0, 1), point1.GetVectorTo(new Point(0, 1)));
        }

        [Test]
        public void PointTest_GetVectorTo_10()
        {
            var point1 = new Point(1, 0);

            Assert.AreEqual(new Point(0, 0), point1.GetVectorTo(new Point(1, 0)));
            Assert.AreEqual(new Point(1, 0), point1.GetVectorTo(new Point(2, 0)));
            Assert.AreEqual(new Point(0, 1), point1.GetVectorTo(new Point(1, 1)));
        }

        [Test]
        public void PointTest_GetVectorTo_01()
        {
            var point1 = new Point(0, 1);

            Assert.AreEqual(new Point(0, 0), point1.GetVectorTo(new Point(0, 1)));
            Assert.AreEqual(new Point(1, 0), point1.GetVectorTo(new Point(1, 1)));
            Assert.AreEqual(new Point(0, 1), point1.GetVectorTo(new Point(0, 2)));
        }

        [Test]
        public void PointTest_Dot()
        {
            AssertDotProduct(new Point(0, 1), new Point(0, 0), 0);
            AssertDotProduct(new Point(0, 1), new Point(0, 1), 1);
            AssertDotProduct(new Point(0, 1), new Point(1, 1), 1);
            AssertDotProduct(new Point(1, 1), new Point(1, 1), 2);
        }

        static void AssertDotProduct(Point point1, Point point2, double expected)
        {
            Assert.AreEqual(expected, point1.Dot(point2));
            Assert.AreEqual(expected, point2.Dot(point1));
        }

    }
}
