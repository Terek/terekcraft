﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class AIToolsTest : BaseTest
    {
        readonly ActorType _actorType = new ActorType("actor", 10, 10, new List<BaseAction>() { new MoveAction(_maxSpeed) } );

        Point target = new Point(100, 0);
        static int _maxSpeed = 5;


        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void Test_NoObstacleAtAll()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);

            Actor actor = World.GetActor(actorId);
            double directionToTarget = actor.Location.GetDirectionTo(target);

            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(directionToTarget, direction);
        }


        [Test]
        public void Test_NoObstacleAtAll_otherDirection()
        {
            string actorId = AddActorWithType(_actorType, 15, 20, 0);

            Actor actor = World.GetActor(actorId);
            double directionToTarget = actor.Location.GetDirectionTo(target);

            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(directionToTarget, direction);
        }

        [Test]
        public void Test_ObstacleTooFar()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);
            AddActorWithType(_actorType, 50, -5, 0);

            Actor actor = World.GetActor(actorId);
            double expectedDirection = actor.Location.GetDirectionTo(target);

            Point nextStep = actor.Location.Move(expectedDirection, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(expectedDirection, direction);
        }

        [Test]
        public void Test_ObstacleClose_AvoidNorthward()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);
            string obstacleId = AddActorWithType(_actorType, 24, -5, 0);

            Actor actor = World.GetActor(actorId);
            Actor obstacle = World.GetActor(obstacleId);

            double distanceToObstacle = Math.Sqrt(actor.Location.GetDistanceSquaredTo(obstacle.Location));
            double directionToObstacle = actor.Location.GetDirectionTo(obstacle.Location);

            double expectedDirection = Math.Asin(2 * _actorType.Size / distanceToObstacle) + directionToObstacle;

            double directionToTarget = actor.Location.GetDirectionTo(target);
            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(expectedDirection, direction);
        }


        [Test]
        public void Test_ObstacleClose_AvoidSouthward()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);
            string obstacleId = AddActorWithType(_actorType, 24, 5, 0);

            Actor actor = World.GetActor(actorId);
            Actor obstacle = World.GetActor(obstacleId);

            double distanceToObstacle = Math.Sqrt(actor.Location.GetDistanceSquaredTo(obstacle.Location));
            double directionToObstacle = actor.Location.GetDirectionTo(obstacle.Location);

            double expectedDirection = - Math.Asin(2 * _actorType.Size / distanceToObstacle) + directionToObstacle;

            double directionToTarget = actor.Location.GetDirectionTo(target);
            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(expectedDirection, direction);
        }


        [Test]
        public void Test_ObstacleClose_AvoidSouthward_SameDirectionAsObstacle()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);
            string obstacleId = AddActorWithType(_actorType, 24, 0, 0);

            Actor actor = World.GetActor(actorId);
            Actor obstacle = World.GetActor(obstacleId);

            double distanceToObstacle = Math.Sqrt(actor.Location.GetDistanceSquaredTo(obstacle.Location));
            double directionToObstacle = actor.Location.GetDirectionTo(obstacle.Location);

            double expectedDirection = Math.Asin(2 * _actorType.Size / distanceToObstacle) + directionToObstacle;

            double directionToTarget = actor.Location.GetDirectionTo(target);
            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(expectedDirection, direction);
        }


        [Test]
        public void Test_ObstacleClose_AvoidSouthward_SameDirectionAsObstacle_Touching()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);
            string obstacleId = AddActorWithType(_actorType, 20, 0, 0);

            Actor actor = World.GetActor(actorId);
            Actor obstacle = World.GetActor(obstacleId);

            double distanceToObstacle = Math.Sqrt(actor.Location.GetDistanceSquaredTo(obstacle.Location));
            double directionToObstacle = actor.Location.GetDirectionTo(obstacle.Location);

            double expectedDirection = Math.Asin(2 * _actorType.Size / distanceToObstacle) + directionToObstacle;

            double directionToTarget = actor.Location.GetDirectionTo(target);
            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(expectedDirection, direction);
        }

        [Test]
        public void Test_ObstacleClose_AvoidSouthward_SameDirectionAsObstacle_Intersecting()
        {
            string actorId = AddActorWithType(_actorType, 0, 0, 0);
            string obstacleId = AddActorWithType(_actorType, 15, 0, 0);

            Actor actor = World.GetActor(actorId);
            World.GetActor(obstacleId);

            const double expectedDirection = Math.PI;

            double directionToTarget = actor.Location.GetDirectionTo(target);
            Point nextStep = actor.Location.Move(directionToTarget, _maxSpeed);

            double direction = World.AiTools.GetNewDirection(actor, nextStep, target);

            Assert.AreEqual(expectedDirection, direction);
        }


    }
}
