﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.spatial;
using TerekCraftEngine.terrain;
using System.Diagnostics;
using System.IO;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class TerrainTest : BaseTest
    {

        [Test]
        public void Test_NonConvex_PointFinding_WithReferenceImpl()
        {
            List<Polygon> polyList = CreateComplexMap();

            var terrain1 = new ReferenceTerrain<Polygon>(polyList);
            var terrain2 = new BSPTree<Polygon>(polyList);

            Test_Point(terrain1, terrain2, 20, polyList);
        }


        [Test]
        public void Test_NonConvex_PointFinding_WithReferenceImpl_largeMap()
        {
            List<Polygon> polyList = new PolyImporter(0, 2).ConvertInput(new StreamReader(TestContext.CurrentContext.TestDirectory + "/testAssets/map0.obj"));

            var terrain1 = new ReferenceTerrain<Polygon>(polyList);
            var terrain2 = new BSPTree<Polygon>(polyList);

             Test_Point(terrain1, terrain2, 20, polyList);
        }



        void Test_Point(ITerrain<Polygon> impl1, ITerrain<Polygon> impl2, int testSize, List<Polygon> polyList)
        {

            double minx = polyList.SelectMany(item => item.Points).Min(item => item.X) + 0.000001;
            double maxx = polyList.SelectMany(item => item.Points).Max(item => item.X) + 0.000001;

            double miny = polyList.SelectMany(item => item.Points).Min(item => item.Y) + 0.000001;
            double maxy = polyList.SelectMany(item => item.Points).Max(item => item.Y) + 0.000001;

            System.Console.WriteLine("minx :" + minx);
            System.Console.WriteLine("maxx :" + maxx);
            System.Console.WriteLine("miny :" + miny);
            System.Console.WriteLine("maxy :" + maxy);

            Stopwatch sw = new Stopwatch();

            List<Point> checkedPoints = new List<Point>(testSize * testSize);

            double diffX = (maxx - minx) / testSize;
            double diffY = (maxy - miny) / testSize;

            for (double x = minx; x <= maxx; x += diffX)
            {
                for (double y = miny; y <= maxy; y += diffY)
                {
                    Point point = new Point(x, y);
                    checkedPoints.Add(point);
                }
            }


            List<Polygon> impl1Result = RunImplOnPoints(impl1, checkedPoints, out long referenceMillis);
            List<Polygon> impl2Result = RunImplOnPoints(impl2, checkedPoints, out long enhancedMillis);

            double ratio = (referenceMillis * 1.0) / enhancedMillis;

            Console.WriteLine("Implementation speed difference ratio : " + ratio);

            ValudateResults(checkedPoints, impl1Result, impl2Result);

            List<bool> impl1ResultCircle = RunImplOnCircles(impl1, checkedPoints, 5 * diffX, out referenceMillis);
            List<bool> impl2ResultCircle = RunImplOnCircles(impl2, checkedPoints, 5 * diffX, out enhancedMillis);

            ratio = (referenceMillis * 1.0) / enhancedMillis;

            Console.WriteLine("Implementation speed difference ratio : " + ratio);

            ValudateResults(checkedPoints, impl1ResultCircle, impl2ResultCircle);

        }

        static void ValudateResults<T>(List<Point> checkedPoints, List<T> impl1Result, List<T> impl2Result)
        {
            List<int> failedIndices = new List<int>();

            for (int i = 0; i < checkedPoints.Count; i++)
            {
                if (!NullSafeEquals(impl1Result[i], impl2Result[i]))
                {
                    failedIndices.Add(i);
                }
            }

            Console.WriteLine("Number of checked points: " + checkedPoints.Count);
            Console.WriteLine("Discrepancies found: " + failedIndices.Count);
            var errorRatio = (failedIndices.Count * 1.0 / checkedPoints.Count);
            var errorPercent = errorRatio * 100;
            System.Console.WriteLine("Error ratio: " + errorPercent + " percent");

            if (failedIndices.Count > 0)
            {
                int index = failedIndices[0];
                System.Console.WriteLine("Discrepancy found for point: " + checkedPoints[index]);
               System.Console.WriteLine("Expected: " + impl1Result[index]);
               System.Console.WriteLine("Was: " + impl2Result[index]);
                Assert.Fail("Discrepancy found for point: " + checkedPoints[index] + " \r\n expected: " + impl1Result[index] + " \r\n actual: " + impl2Result[index]);
            }

        }

        static bool NullSafeEquals<T>(T value1, T value2)
        {
            if(!ReferenceEquals(value1, null))
            {
                return value1.Equals(value2);
            }
            else
            {
                return ReferenceEquals(value1, null);
            }
        }

        static List<Polygon> RunImplOnPoints(ITerrain<Polygon> impl, List<Point> checkedPoints, out long millis)
        {
            List<Polygon> result = new List<Polygon>(checkedPoints.Count);

            Stopwatch sw = new Stopwatch();

            sw.Start();

            foreach (Point point in checkedPoints)
            {
                result.Add(impl.GetPolygonForPoint(point));
            }

            sw.Stop();

            Console.WriteLine(impl + " took: " + sw.Elapsed );

            millis = sw.ElapsedTicks;

            return result;
        }

        static List<Boolean> RunImplOnCircles(ITerrain<Polygon> impl, List<Point> checkedPoints, double radius, out long millis)
        {
            List<Boolean> result = new List<Boolean>(checkedPoints.Count);

            Stopwatch sw = new Stopwatch();

            sw.Start();

            foreach (Point point in checkedPoints)
            {
                result.Add(impl.HasCollision(point, radius));
            }

            sw.Stop();

            Console.WriteLine("circle collision calculation with " + impl + " took: " + sw.Elapsed );

            millis = sw.ElapsedTicks;

            return result;
        }


        static List<Polygon> CreateConvexPolygonFromPoints(params Point[] points)
        {
            var polygon = new Polygon(points);

            var polyList = new List<Polygon> { polygon };
            List<Polygon> convexSubPolygons = new PolygonSplitter(polygon).GetConvexSubPolygons();
            return convexSubPolygons;
        }


        static List<Polygon> CreateSimpleMap()
        {
            return CreateConvexPolygonFromPoints(
                new Point(0, 0),
                new Point(10, 10),
                new Point(20, 10),
                new Point(30, 0),
                new Point(30, 30),
                new Point(20, 20),
                new Point(10, 20),
                new Point(0, 30));
        }

        static List<Polygon> CreateComplexMap()
        {
            List<Polygon> map = new List<Polygon>
            {
                new Polygon(
                    new Point(0, 0),
                    new Point(4, 0),
                    new Point(4, 3),
                    new Point(2, 3),
                    new Point(1, 3),
                    new Point(0, 3)
                ),
                new Polygon(
                    new Point(1, 3),
                    new Point(2, 3),
                    new Point(2, 4),
                    new Point(2, 5),
                    new Point(1, 5),
                    new Point(1, 4)
                ),
                new Polygon(
                    new Point(2, 4),
                    new Point(5, 4),
                    new Point(5, 5),
                    new Point(2, 5)
                ),

                new Polygon(
                    new Point(5, 4),
                    new Point(5, 1),
                    new Point(6, 1),
                    new Point(6, 2),
                    new Point(6, 5),
                    new Point(5, 5)
                ),

                new Polygon(
                    new Point(6, 1),
                    new Point(7, 1),
                    new Point(7, 2),
                    new Point(6, 2)
                ),

                new Polygon(
                    new Point(7, 1),
                    new Point(8, 1),
                    new Point(8, 7),
                    new Point(7, 7),
                    new Point(7, 6),
                    new Point(7, 2)
                ),

                new Polygon(
                    new Point(6, 6),
                    new Point(7, 6),
                    new Point(7, 7),
                    new Point(6, 7)
                ),


                new Polygon(
                    new Point(3, 6),
                    new Point(6, 6),
                    new Point(6, 7),
                    new Point(6, 8),
                    new Point(5, 9),
                    new Point(4, 9),
                    new Point(3, 9)
                ),

                new Polygon(
                    new Point(3, 9),
                    new Point(4, 9),
                    new Point(4, 10),
                    new Point(3, 10)
                ),


                new Polygon(
                    new Point(6, 8),
                    new Point(8, 8),
                    new Point(8, 11),
                    new Point(5, 11),
                    new Point(5, 9)
                )
            };
            return map;

        }



    }
}
