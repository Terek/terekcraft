﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{
    public class PolygonSplitterTest
    {
        [Test]
        public void Test_Polygon_GetConvexSubPolygons_IfConvex_Return_Self()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1));

            TestExpectedPolygons(polygon, polygon);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(10, 0), new Point(1, 1), new Point(0, 10), new Point(-10, 1));

            Polygon expectedPolygon1 = new Polygon(new Point(0, 0), new Point(1, 1), new Point(0, 10), new Point(-10, 1));
            Polygon expectedPolygon2 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(1, 1));

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_lastPointIsFirstValidPoint()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(10, 0), new Point(1, 1), new Point(0, 10));

            Polygon expectedPolygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(1, 1));
            Polygon expectedPolygon2 = new Polygon(new Point(1, 1), new Point(0, 10), new Point(0, 0));

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_lastPointIsNonConvexPoint()
        {
            Polygon polygon = new Polygon(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(9, 1));

            Polygon expectedPolygon1 = new Polygon(new Point(0, 0), new Point(10, 0), new Point(9, 1));
            Polygon expectedPolygon2 = new Polygon(new Point(10, 0), new Point(10, 10), new Point(9, 1));

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_0()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0), 
                new Point(1, 0), 
                new Point(1, 1), 
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 2),
                new Point(0, 2),
                new Point(0, 1)
                );

            Polygon expectedPolygon1 = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(1, 2),
                new Point(0, 2),
                new Point(0, 1)
                );

            Polygon expectedPolygon2 = new Polygon(
                new Point(1, 1),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 2)
                );

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2);
        }

        static void TestExpectedPolygons(Polygon input, params Polygon[] expectedPolygons)
        {
            List<Polygon> subPolygons = new PolygonSplitter(input).GetConvexSubPolygons();

            foreach (var subPolygon in subPolygons)
            {
                Assert.IsTrue(subPolygon.IsConvex());
            }

            Assert.AreEqual(expectedPolygons.Length, subPolygons.Count());

            foreach (var expectedPolygon in expectedPolygons)
            {
                Assert.IsTrue(subPolygons.Contains(expectedPolygon), "Expected polygon not found: " + expectedPolygon );
            }

        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_1()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 2),
                new Point(1, 3),
                new Point(0, 3),
                new Point(0, 2),
                new Point(0, 1)
                );

            Polygon expectedPolygon1 = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(1, 2),
                new Point(1, 3),
                new Point(0, 3),
                new Point(0, 2),
                new Point(0, 1)
                );

            Polygon expectedPolygon2 = new Polygon(
                new Point(1, 1),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 2)
                );


            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_2()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 2),
                new Point(1, 3),
                new Point(0, 3),
                new Point(0, 2),
                new Point(-1, 2),
                new Point(-1, 1),
                new Point(0, 1)
                );


            Polygon expectedPolygon1 = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(1, 2),
                new Point(1, 3),
                new Point(0, 3),
                new Point(0, 2),
                new Point(0, 1)
                );

            Polygon expectedPolygon2 = new Polygon(
                new Point(1, 1),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 2)
                );

            Polygon expectedPolygon3 = new Polygon(
                new Point(0, 2),
                new Point(-1, 2),
                new Point(-1, 1),
                new Point(0, 1)
                );

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2, expectedPolygon3);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_3()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(2, 1),

                new Point(2, 0),
                new Point(3, 0),
                new Point(3, 1),
                new Point(4, 1),
                new Point(4, 2),
                new Point(3, 2),
                new Point(3, 3),
                new Point(2, 3),

                new Point(2, 2),
                new Point(1, 2),
                new Point(1, 3),
                new Point(0, 3),
                new Point(0, 2),
                new Point(-1, 2),
                new Point(-1, 1),
                new Point(0, 1)
                );


            Polygon expectedPolygon1 = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(1, 1),
                new Point(1, 2),
                new Point(1, 3),
                new Point(0, 3),
                new Point(0, 2),
                new Point(0, 1)
                );

            Polygon expectedPolygon2 = new Polygon(
                new Point(1, 1),
                new Point(2, 1),
                new Point(3, 1),
                new Point(4, 1),
                new Point(4, 2),
                new Point(3, 2),
                new Point(2, 2),
                new Point(1, 2)
                );

            Polygon expectedPolygon3 = new Polygon(
                new Point(0, 2),
                new Point(-1, 2),
                new Point(-1, 1),
                new Point(0, 1)
                );

            Polygon expectedPolygon4 = new Polygon(
                new Point(2, 1),
                new Point(2, 0),
                new Point(3, 0),
                new Point(3, 1)
                );

            Polygon expectedPolygon5 = new Polygon(
                new Point(3, 2),
                new Point(3, 3),
                new Point(2, 3),
                new Point(2, 2)
                );

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2, expectedPolygon3, expectedPolygon4, expectedPolygon5);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_4()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(2, 1),
                new Point(3, 1),
                new Point(4, 0),
                new Point(5, 0),
                new Point(5, 3),
                new Point(4, 3),
                new Point(3, 2),
                new Point(2, 2),
                new Point(1, 3),
                new Point(0, 3)
                );


            Polygon expectedPolygon1 = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 3),
                new Point(0, 3)
                );

            Polygon expectedPolygon2 = new Polygon(
                new Point(2, 1),
                new Point(3, 1),
                new Point(3, 2),
                new Point(2, 2)
                );

            Polygon expectedPolygon3 = new Polygon(
                new Point(3, 1),
                new Point(4, 0),
                new Point(5, 0),
                new Point(5, 3),
                new Point(4, 3),
                new Point(3, 2)
                );

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2, expectedPolygon3);
        }

        [Test]
        public void Test_Polygon_GetConvexSubPolygons_5()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(2, 1),
                new Point(3, 1),
                new Point(4, 0),
                new Point(5, 0),
                new Point(5, 3),
                new Point(4, 3),
                new Point(4, 2),
                new Point(2, 2),
                new Point(1, 3),
                new Point(0, 3)
                );


            Polygon expectedPolygon1 = new Polygon(
                new Point(0, 0),
                new Point(1, 0),
                new Point(2, 1),
                new Point(2, 2),
                new Point(1, 3),
                new Point(0, 3)
                );

            Polygon expectedPolygon2 = new Polygon(
                new Point(2, 1),
                new Point(3, 1),
                new Point(4, 2),
                new Point(2, 2)
                );

            Polygon expectedPolygon3 = new Polygon(
                new Point(3, 1),
                new Point(4, 0),
                new Point(5, 0),
                new Point(5, 3),
                new Point(4, 2)
                );

            Polygon expectedPolygon4 = new Polygon(
                new Point(5, 3),
                new Point(4, 3),
                new Point(4, 2)
                );

            TestExpectedPolygons(polygon, expectedPolygon1, expectedPolygon2, expectedPolygon3, expectedPolygon4);
        }



        [Test]
        public void Test_Polygon_GetConvexSubPolygons_Problem1()
        {
            Polygon polygon = new Polygon(
                new Point(10,10),
                new Point(30,10),
                new Point(40, 0),
                new Point(40,30),
                new Point(30, 20)
                );

            new PolygonSplitter(polygon).GetConvexSubPolygons();
        }


        [Test]
        public void Test_Polygon_GetConvexSubPolygons_Problem2()
        {
            Polygon polygon = new Polygon(
                new Point(0, 0),
                new Point(10, 10),
                new Point(30, 10),
                new Point(40, 0),
                new Point(40, 30),
                new Point(30, 20),
                new Point(10, 20),
                new Point(0, 30),
                new Point(0, 0)
                );

            new PolygonSplitter(polygon).GetConvexSubPolygons();
        }



        [Test]
        public void Test_Polygon_GetConvexSubPolygons_StraightLine()
        {
            Polygon polygon = new Polygon(
                new Point(63.08, -153.01),
                new Point(54.66, -153.01),
                new Point(39.46, -153.06),
                new Point(31.03, -153.06),
                new Point(17.67, -168.44),
                new Point(76.45, -168.39)
                );

            new PolygonSplitter(polygon).GetConvexSubPolygons();
        }



    }
}