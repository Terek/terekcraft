﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.collisionhandlers;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class CollisionTest : BaseTest
    {
        static readonly ICollisionHandler CollisionHandler = new DespawnCollisionHandler(GetUniqueString(), GetUniqueString(), GetUniqueString());
        static readonly ExplosionCollisionHandler ExplosionHandler = new ExplosionCollisionHandler(5, 10, GetUniqueString(), GetUniqueString(), GetUniqueString());

        ActorType _type = new ActorType("tank", 10, 10, new List<BaseAction>() { new MoveAction(10) });
        ActorType _typeWithCollisionHandler = new ActorType("tank", 10, 10, new List<BaseAction>() { new MoveAction(10) }, collisionHandler : CollisionHandler);
        ActorType _typeWithExplosion = new ActorType("tank", 10, 10, new List<BaseAction>() { new MoveAction(10) }, collisionHandler : ExplosionHandler);

        [Test]
        public void TestSimpleCollision()
        {
            var actorToMove = AddActorWithType(_type, 0, 0, 0);
            AddActorWithType(_type, 21, 0, 0);

            World.Move(actorToMove, 0, 10);

            World.Tick();

            Assert.AreEqual(0, World.GetTickEvents().Count);
        }

        [Test]
        public void TestNoCollision()
        {
            var actorToMove = AddActorWithType(_type, 0, 0, 0);
            AddActorWithType(_type, 30, 0, 0);

            World.Move(actorToMove, 0, 5);

            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(MoveEvent));
            Assert.AreEqual(1, World.GetTickEvents().Count);
        }

        [Test]
        public void TestSimpleCollisionWithDespawnCollisionHandler()
        {
            var originalLocation = new Point(0,0);
            const double direction = 0;
            const double speed = 10;

            string actorToMove = World.AddActor(new Actor(_typeWithCollisionHandler) { Location = originalLocation, Direction = direction, Aim = 0 }).ActorId;

            AddActorWithType(_type, 10, 0, 0);

            World.Move(actorToMove, 0, speed);

            World.Tick();

            Point expectedDespawnLocation = originalLocation.Move(direction, speed);

            ValidateEvents(World.GetTickEvents(), typeof(DespawnEvent), typeof(MoveEvent));
            var despawnEvent = GetSingleEventOfType<DespawnEvent>(World.GetTickEvents());
            Assert.AreEqual(actorToMove, despawnEvent.ActorId);
            Assert.AreEqual(expectedDespawnLocation, despawnEvent.Location);
            Assert.IsTrue(CollisionHandler.HasAllTag(despawnEvent.Tags));

            var moveEvent = GetSingleEventOfType<MoveEvent>(World.GetTickEvents());
            Assert.AreEqual(actorToMove, moveEvent.ActorId);
            Assert.AreEqual(expectedDespawnLocation, moveEvent.NewLocation);
            Assert.AreEqual(originalLocation, moveEvent.OldLocation);
            
            Assert.IsNull(World.GetActor(actorToMove));
        }

        [Test]
        public void TestCollisionWithExplosionCollisionHandler()
        {
            var originalLocation = new Point(0, 0);
            const double direction = 0;
            const double speed = 10;

            string actorToMove = World.AddActor(new Actor(_typeWithExplosion) { Location = originalLocation, Direction = direction, Aim = 0 }).ActorId;

            string victimActor = AddActorWithType(_type, 10, 0, 0);

            World.Move(actorToMove, 0, speed);

            World.Tick();

            Point expectedDespawnLocation = originalLocation.Move(direction, speed);

            ValidateEvents(World.GetTickEvents(), typeof(DespawnEvent), typeof(MoveEvent), typeof(DamageEvent));
            var despawnEvent = GetSingleEventOfType<DespawnEvent>(World.GetTickEvents());
            Assert.AreEqual(actorToMove, despawnEvent.ActorId);
            Assert.AreEqual(expectedDespawnLocation, despawnEvent.Location);
            Assert.IsTrue(ExplosionHandler.HasAllTag(despawnEvent.Tags));

            var moveEvent = GetSingleEventOfType<MoveEvent>(World.GetTickEvents());
            Assert.AreEqual(actorToMove, moveEvent.ActorId);
            Assert.AreEqual(expectedDespawnLocation, moveEvent.NewLocation);
            Assert.AreEqual(originalLocation, moveEvent.OldLocation);

            var damageEvent = GetSingleEventOfType<DamageEvent>(World.GetTickEvents());
            Assert.IsTrue(ExplosionHandler.HasAllTag(damageEvent.Tags));
            Assert.AreEqual(actorToMove, damageEvent.ActorId);
            Assert.AreEqual(ExplosionHandler.Damage, damageEvent.Damage);
            Assert.AreEqual(victimActor, damageEvent.Target);

            Assert.IsNull(World.GetActor(actorToMove));
        }


        [Test]
        public void TestCollisionWithExplosionCollisionHandler_noCollisionOnSameTeam()
        {
            var originalLocation = new Point(0, 0);
            const double direction = 0;
            const double speed = 10;

            string team = GetUniqueString();

            string actorToMove = World.AddActor(new Actor( _typeWithExplosion, team ) { Location = originalLocation, Direction = direction, Aim = 0 }).ActorId;

            World.AddActor(new Actor(_typeWithExplosion, team) { Location = new Point(10, 0), Direction = direction, Aim = 0 });

            World.Tick();

            World.Move(actorToMove, 0, speed);

            World.Tick();

            ValidateEvents(World.GetTickEvents(), typeof(MoveEvent));
        }

    }
}
