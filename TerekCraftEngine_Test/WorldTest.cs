﻿using System;
using System.Linq;
using NUnit.Framework;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using System.Collections.Generic;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class WorldTest : BaseTest
    {
        readonly ActorType _actorType = new ActorType("tank", 10, 1, new List<BaseAction>() { new MoveForwardAction(1f), new TurnAction(0.5), new ShootForwardAction(1, 20) { Cooldown = 5 } });

        [Test]
        public void TestCreateWorld()
        {
            Assert.IsNotNull(World.Actors);
            Assert.IsTrue(!World.Actors.Any());
            Assert.AreEqual(0, World.FrameNumber);
        }


        [Test]
        public void Tick_increases_frame_number()
        {
            World.Tick();
            Assert.AreEqual(1, World.FrameNumber);
        }

        [Test]
        public void New_actor_added_is_in_actors_list()
        {
            var actor = new Actor(_actorType);
            string actorId = World.AddActor( actor ).ActorId;
            World.Tick();
            Assert.AreEqual(1, World.Actors.Count());
            Assert.AreEqual(actor.GetNew(actorId), World.Actors.First());
        }


        [Test]
        public void New_actor_added_new_actor_id()
        {
            string newId1 = World.AddActor(new Actor(_actorType)).ActorId;
            string newId2 = World.AddActor(new Actor(_actorType)).ActorId;

            Assert.IsNotNull(newId1);
            Assert.IsNotNull(newId2);

            Assert.AreNotEqual(newId1, newId2);
        }

        [Test]
        public void Retreive_actor_by_id()
        {
            var actor = new Actor(_actorType);
            string newId1 = World.AddActor(actor).ActorId;

            Actor retreivedActor = World.GetActor(newId1);

            Assert.IsNotNull(retreivedActor);
            Assert.AreEqual(actor.GetNew(newId1), retreivedActor);
        }


        [Test]
        public void Retreive_actor_by_id_no_result()
        {
            Actor retreivedActor = World.GetActor(GetUniqueString());

            Assert.IsNull(retreivedActor);
        }

        [Test]
        public void Invalid_action_for_actor()
        {
            string actorId = World.AddActor(new Actor(_actorType)).ActorId;
            World.AddCommand(new AimCommand(actorId, 0, new AimCommand.CommandParameter(0)));
            World.Tick();
        }
    }
}
