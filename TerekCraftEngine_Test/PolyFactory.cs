﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine_Test
{
    public class PolyFactory
    {

        public static Polygon CreatePoly(int x, int y)
        {
            return CreatePoly(x, y, 1);
        }



        public static Polygon CreatePoly(int x, int y, int size)
        {
            return new Polygon(
                new Point(x, y),
                new Point(x + size, y),
                new Point(x + size, y + size),
                new Point(x, y + size));
        }

    }
}
