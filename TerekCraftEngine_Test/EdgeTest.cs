﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TerekCraftEngine;

namespace TerekCraftEngine_Test
{
    [TestFixture]
    public class EdgeTest : BaseTest
    {
        const int PERFORMANCE_TEST_COUNT = 1000;

        [Test]
        public void Test_EdgeEqual()
        {
            var edge1 = new Edge(new Point(0, 1), new Point( 1, 0));
            var edge2 = new Edge(new Point(0, 1), new Point(1, 0));
            Assert.AreEqual(edge1, edge2);
        }

        [Test]
        public void Test_GetSide()
        {
            Edge edge = new Edge(new Point(0, 0), new Point(0, 1));
            Assert.IsTrue(edge.GetSide(new Point(-1, 0)) > 0);
            Assert.IsTrue(edge.GetSide(new Point(1, 0)) < 0);
            Assert.AreEqual(0, edge.GetSide(new Point(0, -1)));
        }

        [Test]
        public void Test_IsLeft_BaseVectors()
        {
            AssertIsOnLeft(new Point(0, 0), new Point(0, 1), new Point(-1, 0));
            AssertIsOnLeft(new Point(0, 0), new Point(0, 1), new Point(-10, 0));
            AssertIsOnLeft(new Point(0, 0), new Point(0, 1), new Point(-10, -10));
            AssertIsOnLeft(new Point(0, 0), new Point(0, 1), new Point(-10, 10));

            AssertIsOnLeft(new Point(0, 0), new Point(1, 0), new Point(0, 1));
            AssertIsOnLeft(new Point(0, 0), new Point(1, 0), new Point(0, 10));
            AssertIsOnLeft(new Point(0, 0), new Point(1, 0), new Point(10, 10));
            AssertIsOnLeft(new Point(0, 0), new Point(1, 0), new Point(-10, 10));

            AssertIsOnLeft(new Point(0, 0), new Point(0, -1), new Point(1, 0));
            AssertIsOnLeft(new Point(0, 0), new Point(0, -1), new Point(10, 0));
            AssertIsOnLeft(new Point(0, 0), new Point(0, -1), new Point(10, -10));
            AssertIsOnLeft(new Point(0, 0), new Point(0, -1), new Point(10, 10));

            AssertIsOnLeft(new Point(0, 0), new Point(-1, 0), new Point(0, -1));
            AssertIsOnLeft(new Point(0, 0), new Point(-1, 0), new Point(0, -10));
            AssertIsOnLeft(new Point(0, 0), new Point(-1, 0), new Point(10, -10));
            AssertIsOnLeft(new Point(0, 0), new Point(-1, 0), new Point(-10, -10));
        }

        [Test]
        public void Test_IsLeft_OtherVectors()
        {
            AssertIsOnLeft(new Point(0, 0), new Point(1, 1), new Point(-1, 0));
            AssertIsOnLeft(new Point(0, 0), new Point(1, 1), new Point(0, 1));
            AssertIsOnLeft(new Point(0, 0), new Point(-1, -1), new Point(1, 0));
            AssertIsOnLeft(new Point(0, 0), new Point(-1, -1), new Point(0, -1));
        }

        [Test]
        public void Test_EdgeIsParallel()
        {
            AssertIsParallel(new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(1, 1));
            AssertIsParallel(new Point(1, 0), new Point(2, 0), new Point(-2, 2), new Point(0, 2));

            AssertIsNotParallel(new Point(0, 1), new Point(1, 0), new Point(0, 0), new Point(1, 0));

        }

        [Test]
        public void Test_EdgeIsColinear()
        {
            AssertIsColinear(new Point(0, 0), new Point(1, 0), new Point(0, 0), new Point(1, 0));
            AssertIsColinear(new Point(-1, 0), new Point(1, 0), new Point(5, 0), new Point(6, 0));
            AssertIsColinear(new Point(1, 0), new Point(0, 0), new Point(5, 0), new Point(6, 0));
            AssertIsColinear(new Point(1, 1), new Point(0, 1), new Point(5, 1), new Point(6, 1));

            AssertIsNotColinear(new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(1, 0));
            AssertIsNotColinear(new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(1, 1));
        }

        [Test]
        public void Test_IsPointOnEdge()
        {
            AssertPointOnEdge(new Point(0, 0), new Point(1, 0), new Point(0.5, 0));
            AssertPointNotOnEdge(new Point(0, 0), new Point(1, 0), new Point(1.5, 0));
            AssertPointOnEdge(new Point(0, 0), new Point(1, 0), new Point(1, 0));
            AssertPointOnEdge(new Point(0, 0), new Point(1, 0), new Point(0, 0));

            AssertPointOnEdge(new Point(0, 0), new Point(0, 1), new Point(0, 0.5));
            AssertPointNotOnEdge(new Point(0, 0), new Point(0, 1), new Point(0, 1.5));
            AssertPointOnEdge(new Point(0, 0), new Point(0, 1), new Point(0, 1));
            AssertPointOnEdge(new Point(0, 0), new Point(0, 1), new Point(0, 0));

            AssertPointOnEdge(new Point(0, 0), new Point(1, 1), new Point(0.5, 0.5));
            AssertPointNotOnEdge(new Point(0, 0), new Point(1, 1), new Point(-0.5, -0.5));
            AssertPointNotOnEdge(new Point(0, 0), new Point(1, 1), new Point(1.5, 1.5));
            AssertPointOnEdge(new Point(0, 0), new Point(1, 1), new Point(1, 1));
            AssertPointOnEdge(new Point(0, 0), new Point(1, 1), new Point(0, 0));

            AssertPointNotOnEdge(new Point(0, 0), new Point(1, 1), new Point(2, 1));
            AssertPointNotOnEdge(new Point(0, 0), new Point(1, 1), new Point(1, 2));

            AssertPointOnEdge(new Point(1, 1), new Point(7, 5), new Point(4, 3));
            AssertPointNotOnEdge(new Point(1, 1), new Point(7, 5), new Point(3, 3));
        }

        [Test]
        public void Test_IsIntersecting()
        {
            AssertIsNotIntersecting(new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(1, 1));
            AssertIsNotIntersecting(new Point(0, 1), new Point(1, 1), new Point(0, 2), new Point(1, 2));

            AssertIsIntersecting(new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1));
            AssertIsIntersecting(new Point(-1, -1), new Point(1, 1), new Point(-1, 1), new Point(1, -1));

            AssertIsNotIntersecting(new Point(-1, 0), new Point(1, 0), new Point(10, 1), new Point(10, -1));
        }


        [Test]
        public void Test_IsIntersecting_Colinear()
        {
            AssertIsIntersecting(new Point(0, 0), new Point(1, 0), new Point(0, 0), new Point(1, 0));
            AssertIsIntersecting(new Point(0, 0), new Point(1, 0), new Point(-1, 0), new Point(1, 0));
            AssertIsIntersecting(new Point(0, 0), new Point(1, 0), new Point(-1, 0), new Point(2, 0));

            AssertIsNotIntersecting(new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(3, 0));
        }



        [Test]
        public void Test_GetSlicePoint_noResult()
        {
            AssertSlicePoint(new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(1, 1), null);
            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1), null);


            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(2, 0), null);
            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(-1, 0), new Point(-2, 0), null);

            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(0, 0), new Point(0, 1), null);
            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(0, 0), new Point(0, 1), null);
        }

        [Test]
        public void Test_GetSlicePoint_CommonPoint_HasResult()
        {
            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(0, 0), new Point(1, 0), new Point(0, 0));
            AssertSlicePoint(new Point(0, 1), new Point(1, 0), new Point(0, 0), new Point(1, 0), new Point(1, 0));
            AssertSlicePoint(new Point(0, 0), new Point(1, 1), new Point(0, 0), new Point(0, 1), new Point(0, 0));
            AssertSlicePoint(new Point(1, 1), new Point(0, 0), new Point(0, 0), new Point(0, 1), new Point(0, 0));
            AssertSlicePoint(new Point(1, 1), new Point(1, 0), new Point(1, 1), new Point(0, 1), new Point(1, 1));

            AssertSlicePoint(new Point(11, 11), new Point(11, 10), new Point(11, 11), new Point(10, 11), new Point(11, 11));

        }

        [Test]
        public void Test_GetSlicePoint_NoCommonPoint_Touching()
        {
            AssertSlicePoint(new Point(0, 0), new Point(0, 1), new Point(-1, 0), new Point(1, 0), new Point(0, 0));
            AssertSlicePoint(new Point(0, 0), new Point(1, 1), new Point(-1, 0), new Point(1, 0), new Point(0, 0));

            AssertSlicePoint(new Point(0, 0), new Point(1, 0), new Point(0, -1), new Point(0, 1), new Point(0, 0));
            AssertSlicePoint(new Point(0, 0), new Point(1, 1), new Point(0, -1), new Point(0, 1), new Point(0, 0));
        }


        [Test]
        public void Test_GetSlicePoint_NoCommonPoint_NoTouching()
        {
            AssertSlicePoint(new Point(0, 1), new Point(0, 2), new Point(-1, 0), new Point(1, 0), new Point(0, 0));
            AssertSlicePoint(new Point(1, 1), new Point(2, 2), new Point(-1, 0), new Point(1, 0), new Point(0, 0));

            AssertSlicePoint(new Point(1, 0), new Point(2, 0), new Point(0, -1), new Point(0, 1), new Point(0, 0));
            AssertSlicePoint(new Point(1, 1), new Point(2, 2), new Point(0, -1), new Point(0, 1), new Point(0, 0));

            AssertSlicePoint(new Point(2, 2), new Point(3, 3), new Point(0, 2), new Point(2, 0), new Point(1, 1));
            AssertSlicePoint(new Point(2, 2), new Point(3, 3), new Point(0, 3), new Point(3, 0), new Point(1.5, 1.5));
        }


        [Test]
        public void Test_GetSlice_without_slice()
        {
            var firstEdge = new Edge(new Point(0, 0), new Point(0,1));
            var secondEdge = new Edge(new Point(1, 0), new Point(2, 0));

            AssertEdgeSlicing(firstEdge, secondEdge, null, secondEdge);
        }


        [Test]
        public void Test_GetSlice_common_point()
        {
            var firstEdge = new Edge(new Point(0, 0), new Point(0, 1));
            var secondEdge = new Edge(new Point(0, 0), new Point(2, 0));

            AssertEdgeSlicing(firstEdge, secondEdge, null, secondEdge);
        }




        [Test]
        public void Test_GetSlice_hasPointOnEdge()
        {
            var firstEdge = new Edge(new Point(0, 0), new Point(0, 1));
            var secondEdge = new Edge(new Point(-1, 0), new Point(1, 0));

            var expectedLeftSlice = new Edge(new Point(-1, 0), new Point(0, 0) );
            var expectedRightSlice = new Edge(new Point(0, 0), new Point(1, 0));

            AssertEdgeSlicing(firstEdge, secondEdge, expectedLeftSlice, expectedRightSlice);
        }



        [Test]
        public void Test_GetSlice()
        {
            var firstEdge = new Edge(new Point(0, -1), new Point(0, 1));
            var secondEdge = new Edge(new Point(-1, 0), new Point(1, 0));

            var expectedLeftSlice = new Edge(new Point(-1, 0), new Point(0, 0));
            var expectedRightSlice = new Edge(new Point(0, 0), new Point(1, 0));

            AssertEdgeSlicing(firstEdge, secondEdge, expectedLeftSlice, expectedRightSlice);
        }


        [Test]
        public void Test_GetSlice_parallel()
        {
            var firstEdge = new Edge(new Point(0, -1), new Point(0, 1));
            var secondEdge = new Edge(new Point(1, -1), new Point(1, 1));

            AssertEdgeSlicing(firstEdge, secondEdge, null, secondEdge);
        }


        [Test]
        public void Test_GetSlices()
        {
            var slicerEdge = new Edge(new Point(0, -1), new Point(0, 1));

            var leftEdge = new Edge(new Point(-2, 0), new Point(-1, 0));
            var rightEdge = new Edge(new Point(1, 0), new Point(2, 0));

            var colinearEdge = new Edge(new Point(0, -3), new Point(0, -5));

            var splittedEdge = new Edge(new Point(-1, 0), new Point(1, 0));


            var expectedSplitLeft = new Edge(new Point(-1, 0), new Point(0, 0));
            var expectedSplitRight = new Edge(new Point(0, 0), new Point(1, 0));

            List<Edge> inputEdges = new List<Edge> {leftEdge, rightEdge, colinearEdge, splittedEdge};

            var leftEdges = slicerEdge.GetLeftSlices(inputEdges);
            var rightEdges = slicerEdge.GetRightSlices(inputEdges);

            Assert.NotNull(leftEdges);
            Assert.NotNull(rightEdges);

            Assert.False( leftEdges.Contains(null));
            Assert.False( rightEdges.Contains(null));

            Assert.True(leftEdges.Contains(leftEdge));
            Assert.False(rightEdges.Contains(leftEdge));

            Assert.True(rightEdges.Contains(rightEdge));
            Assert.False(leftEdges.Contains(rightEdge));

            Assert.False(leftEdges.Contains(colinearEdge));
            Assert.False(rightEdges.Contains(colinearEdge));


            Assert.True(leftEdges.Contains(expectedSplitLeft));
            Assert.False(rightEdges.Contains(expectedSplitLeft));

            Assert.True(rightEdges.Contains(expectedSplitRight));
            Assert.False(leftEdges.Contains(expectedSplitRight));

            Assert.AreEqual(2, rightEdges.Count());
            Assert.AreEqual(2, leftEdges.Count());

        }



        static void AssertEdgeSlicing(Edge firstEdge, Edge secondEdge, Edge expectedLeftSlice, Edge expectedRightSlice)
        {
            Assert.AreEqual(expectedLeftSlice, firstEdge.GetLeftSlice(secondEdge));
            Assert.AreEqual(expectedRightSlice, firstEdge.GetRightSlice(secondEdge));
            Assert.AreEqual(expectedRightSlice, firstEdge.Reverse().GetLeftSlice(secondEdge));
            Assert.AreEqual(expectedLeftSlice, firstEdge.Reverse().GetRightSlice(secondEdge));
        }


        [Test]
        public void Test_GetSlice_colinear()
        {
            var firstEdge = new Edge(new Point(0, 1), new Point(0, -1));
            var secondEdge = new Edge(new Point(0, 2), new Point(0, 3));

            Edge expectedLeftSlice = null;
            Edge expectedRightSlice = null;

            Assert.AreEqual(expectedLeftSlice, firstEdge.GetLeftSlice(secondEdge));
            Assert.AreEqual(expectedRightSlice, firstEdge.GetRightSlice(secondEdge));
        }


        [Test]
        public void Test_IsIntersecting_Circle_No_Intersection()
        {
            var center = new Point(3, 3);
            double radius = 1;

            var edge = new Edge(new Point(0, -1), new Point(0, 1));

            Assert.AreEqual(edge.IsIntersecting(center, radius), Intersection.None);
        }

        [Test]
        public void Test_IsIntersecting_Circle_Intersection()
        {
            var center = new Point(3, 3);
            double radius = 4;

            var edge = new Edge(new Point(0, -10), new Point(0, 10));

            Assert.AreEqual(Intersection.IntersectSegment, edge.IsIntersecting(center, radius));
        }

        [Test]
        public void Test_IsIntersecting_Circle_Touch()
        {
            var center = new Point(3, 4);
            double radius = 4;

            var edge = new Edge(new Point(0, -10), new Point(0, 10));

            Assert.AreEqual(Intersection.IntersectSegment, edge.IsIntersecting(center, radius));
        }

        [Test]
        public void Test_IsIntersecting_Circle_IntersectLine()
        {
            var center = new Point(30, 3);
            double radius = 4;

            var edge = new Edge(new Point(-10, 0), new Point(10, 0));

            TestIntersection(edge, center, radius);
        }

        [Test]
        public void Test_IsIntersecting_Circle()
        {

            var originalStartPoint = new Point(5, 5);
            double distance = 8;
            double radius = distance / 2;

            for(double direction = 0; direction < 7; direction += 0.2)
            {
                var endpoint = originalStartPoint.Move(direction, distance);

                var edge = new Edge(originalStartPoint, endpoint);

                var center = originalStartPoint;

                TestOrthogonalToEdge(direction, distance, radius, edge, center);
            }


        }

        [Test]
        public void Test_IsIntersecting_Circle_edgeCases()
        {
            TestIntersection(new Edge(new Point(5, 5), new Point(13, 5)), new Point(5, 1), 4);
            TestIntersection(new Edge(new Point(5, 5), new Point(13, 5)), new Point(-9, 9), 4);
            TestIntersection(new Edge(new Point(5, 5), new Point(13, 5)), new Point(1, 5), 4);

            TestIntersection(new Edge(new Point(5, 5), new Point(13, 5)), new Point(1, 5), 20);

            TestIntersection(new Edge(new Point(5, 5), new Point(13, 5)), new Point(7, 9), 4);

        }



        [Test]
        public void Test_IsIntersecting_Performance()
        {
            var center = new Point(3, 3);
            double radius = 4;

            var edge = new Edge(new Point(0, -10), new Point(0, 10));

            for(int i = 0; i < PERFORMANCE_TEST_COUNT; i++)
            {
                edge.IsIntersectingSlow(center, radius);
            }
        }


        [Test]
        public void Test_IsIntersecting_Performance_Fast()
        {
            var center = new Point(3, 3);
            double radius = 4;

            var edge = new Edge(new Point(0, -10), new Point(0, 10));

            for (int i = 0; i < PERFORMANCE_TEST_COUNT; i++)
            {
                edge.IsIntersecting(center, radius);
            }
        }



        static void TestOrthogonalToEdge(double direction, double distance, double radius, Edge edge, Point center)
        {
            var orthogonalDirection = direction + Math.PI / 2;

            center = center.Move(orthogonalDirection, -distance);

            for (int i = 0; i < 8; i++)
            {
                center = center.Move(orthogonalDirection, distance / 4 + 0.001);

                TestAlongEdge(direction, distance, radius, edge, center);
            }
        }

        static void TestAlongEdge(double direction, double distance, double radius, Edge edge, Point center)
        {
            center = center.Move(direction, -distance);

            for (int i = 0; i < 8; i++)
            {
                center = center.Move(direction, distance / 4);
                TestIntersection(edge, center, radius);
            }
        }



        static void TestIntersection(Edge edge, Point center, double radius)
        {
            var message = edge + " - " + center + " - radius = " + radius;
            Assert.AreEqual( edge.IsIntersectingSlow(center, radius), edge.IsIntersecting(center, radius), message);
        }

        void AssertSlicePoint(Point startPoint1, Point endPoint1, Point startPoint2, Point endPoint2, Point slicePoint)
        {
            Assert.AreEqual(slicePoint, new Edge(startPoint1, endPoint1).GetSlicePoint(new Edge(startPoint2, endPoint2)));
            Assert.AreEqual(slicePoint, new Edge(endPoint1, startPoint1).GetSlicePoint(new Edge(startPoint2, endPoint2)));
            Assert.AreEqual(slicePoint, new Edge(startPoint1, endPoint1).GetSlicePoint(new Edge(endPoint2, startPoint2)));
            Assert.AreEqual(slicePoint, new Edge(endPoint1, startPoint1).GetSlicePoint(new Edge(endPoint2, startPoint2)));
        }

        void AssertIsIntersecting(Point startPoint1, Point endPoint1, Point startPoint2, Point endPoint2)
        {
            Assert.IsTrue(new Edge(startPoint1 ,endPoint1).IsIntersecting(new Edge(startPoint2, endPoint2)));
            Assert.IsTrue(new Edge(startPoint2, endPoint2).IsIntersecting(new Edge(startPoint1, endPoint1)));
            Assert.IsTrue(new Edge(endPoint1, startPoint1).IsIntersecting(new Edge(endPoint2, startPoint2)));
        }

        void AssertIsNotIntersecting(Point startPoint1, Point endPoint1, Point startPoint2, Point endPoint2)
        {
            Assert.IsFalse(new Edge(startPoint1, endPoint1).IsIntersecting(new Edge(startPoint2, endPoint2)));
            Assert.IsFalse(new Edge(startPoint2, endPoint2).IsIntersecting(new Edge(startPoint1, endPoint1)));
            Assert.IsFalse(new Edge(endPoint1, startPoint1).IsIntersecting(new Edge(endPoint2, startPoint2)));
        }


        void AssertPointOnEdge(Point startPoint, Point endPoint, Point point)
        {
            Assert.IsTrue(new Edge(startPoint, endPoint).IsPointOnEdge(point));
            Assert.IsTrue(new Edge(endPoint, startPoint).IsPointOnEdge(point));
        }

        void AssertPointNotOnEdge(Point startPoint, Point endPoint, Point point)
        {
            Assert.IsFalse(new Edge(startPoint, endPoint).IsPointOnEdge(point));
            Assert.IsFalse(new Edge(endPoint, startPoint).IsPointOnEdge(point));
        }


        static void AssertIsParallel(Point startPoint, Point endPoint, Point statPoint2, Point endPoint2)
        {
            Assert.IsTrue(new Edge(startPoint, endPoint).IsParallel(new Edge(statPoint2, endPoint2)));
        }

        static void AssertIsNotParallel(Point startPoint, Point endPoint, Point statPoint2, Point endPoint2)
        {
            Assert.IsFalse(new Edge(startPoint, endPoint).IsParallel(new Edge(statPoint2, endPoint2)));
        }

        static void AssertIsColinear(Point startPoint, Point endPoint, Point statPoint2, Point endPoint2)
        {
            Assert.IsTrue(new Edge(startPoint, endPoint).IsColinear(new Edge(statPoint2, endPoint2)));
        }

        static void AssertIsNotColinear(Point startPoint, Point endPoint, Point statPoint2, Point endPoint2)
        {
            Assert.IsFalse(new Edge(startPoint, endPoint).IsColinear(new Edge(statPoint2, endPoint2)));
        }


        static void AssertIsOnLeft(Point startPoint, Point endPoint, Point point)
        {
            Edge edge = new Edge(startPoint, endPoint);
            Assert.IsTrue(edge.IsLeft(point));
            Assert.IsFalse(edge.IsRight(point));

            Edge reverseEdge = new Edge(endPoint, startPoint);
            Assert.IsFalse(reverseEdge.IsLeft(point));
            Assert.IsTrue(reverseEdge.IsRight(point));
        }

    }
}
