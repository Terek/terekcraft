﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerekCraftEngine
{
    public class EngineMath
    {
        public const double EPSILON = 1e-8;
        const double FullCircleAngle = 2 * Math.PI;


        public static double sqr(double value)
        {
            return value * value;
        }

        public static bool Equals0(double v)
        {
            return v < EngineMath.EPSILON && v > -EngineMath.EPSILON;
        }

        public static double NormalizeAngleDiff(double diff)
        {
            while (diff > Math.PI)
            {
                diff = diff - FullCircleAngle;
            }
            while (diff < -Math.PI)
            {
                diff = FullCircleAngle + diff;
            }

            return diff;
        }

        public static double ProcessDiff(double diff, double maxDiff)
        {
            if (diff > maxDiff)
            {
                diff = maxDiff;
            }
            else if (diff < -maxDiff)
            {
                diff = -maxDiff;
            }
            return diff;
        }

        public static double NormalizeAngle(double angle)
        {
            while (angle < 0)
            {
                angle += FullCircleAngle;
            }
            while (angle >= FullCircleAngle)
            {
                angle -= FullCircleAngle;
            }

            return angle;
        }




    }
}
