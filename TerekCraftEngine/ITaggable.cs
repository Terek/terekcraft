﻿using System.Collections.Generic;

namespace TerekCraftEngine
{
    public interface ITaggable
    {

        IEnumerable<string> Tags  { get; }


        bool HasTag(string tag);
        bool HasAllTag(IEnumerable<string> tags);


    }
}