﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TerekCraftEngine
{
    public class Taggable : ITaggable
    {
        HashSet<string> _tags = new HashSet<string>();

        public Taggable(params string[] tags)
        {
            this._tags = new HashSet<string>(tags);
        }

        public Taggable(IEnumerable<string> tags)
        {
            this._tags = new HashSet<string>(tags ?? new List<string>());
        }

        public IEnumerable<string> Tags
        {
            get { return _tags; }
        }

        public bool HasAllTag(IEnumerable<string> tags)
        {
            return _tags.All(item => _tags.Contains(item));
        }

        public bool HasTag(string tag)
        {
            return _tags.Contains(tag);
        }
    }
}