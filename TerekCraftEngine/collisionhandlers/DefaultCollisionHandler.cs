﻿using System;
using System.Collections.Generic;
using TerekCraftEngine.events;

namespace TerekCraftEngine.collisionhandlers
{
    public class DefaultCollisionHandler : Taggable, ICollisionHandler, ITerrainHandler
    {
        public IEnumerable<Event> HandleCollision(Actor actor, MoveEvent moveEvent, World world)
        {
            yield break;
        }

        public IEnumerable<Event> HandleCollision(Actor actor, Actor otherActor, MoveEvent moveEvent, World world)
        {
            yield break;
        }
    }
}