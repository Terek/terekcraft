﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.collisionhandlers
{
    public interface ITerrainHandler : ITaggable
    {
        IEnumerable<Event> HandleCollision(Actor actor, MoveEvent moveEvent, World world);
    }
}
