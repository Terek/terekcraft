﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.collisionhandlers
{
    public class ExplosionCollisionHandler : Taggable, ICollisionHandler, ITerrainHandler
    {
        readonly int _damage;
        readonly double _radius;


        public ExplosionCollisionHandler(int damage, double radius, params string[] tags) : this(damage, radius, new HashSet<string>(tags))
        {
        }

        public ExplosionCollisionHandler(int damage, double radius, HashSet<string> tags) : base(tags)
        {
            _damage = damage;
            _radius = radius;
        }

        public double Damage { get { return _damage; } }
        public double Radius { get { return _radius; } }


        public IEnumerable<Event> HandleCollision(Actor actor, MoveEvent moveEvent, World world)
        {
            return Handle(actor, null, moveEvent, world);
        }

        public IEnumerable<Event> HandleCollision(Actor actor, Actor otherActor, MoveEvent moveEvent, World world)
        {
            return Handle(actor, otherActor, moveEvent, world);
        }

        public IEnumerable<Event> Handle(Actor actor, Actor otherActor, MoveEvent moveEvent, World world)
        {
            yield return moveEvent;

            if (actor.Team == null || otherActor != null && actor.Team != otherActor.Team)
            {
                yield return new DespawnEvent(actor.Id, moveEvent.NewLocation, Tags);

                foreach (var victim in world.SpatialDatabase.RetreiveInRange(moveEvent.NewLocation, _radius))
                {
                    if (victim.Id != actor.Id)
                    {
                        yield return
                            new DamageEvent(actor.Id, Tags)
                            {
                                Damage = _damage,
                                Target = victim.Id
                            };
                    }
                }
            }
        }

    }
}
