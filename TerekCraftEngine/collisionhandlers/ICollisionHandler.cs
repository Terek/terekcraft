﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.collisionhandlers
{
    public interface ICollisionHandler : ITaggable
    {
        IEnumerable<Event> HandleCollision(Actor actor, Actor otherActor, MoveEvent moveEvent, World world);
    }
}
