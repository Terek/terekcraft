﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.collisionhandlers
{
    public class FlyingTerrainHandler : Taggable, ICollisionHandler, ITerrainHandler
    {
        public IEnumerable<Event> HandleCollision(Actor actor, MoveEvent moveEvent, World world)
        {
            yield return moveEvent;
        }

        public IEnumerable<Event> HandleCollision(Actor actor, Actor otherActor, MoveEvent moveEvent, World world)
        {
            yield return moveEvent;
        }
    }
}
