﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.collisionhandlers
{
    public class DespawnCollisionHandler : Taggable, ICollisionHandler, ITerrainHandler
    {
        public DespawnCollisionHandler(params string[] tags) : base(tags)
        {
        }

        public IEnumerable<Event> HandleCollision(Actor actor, MoveEvent moveEvent, World world)
        {
            return Handle(actor, moveEvent);
        }

        public IEnumerable<Event> HandleCollision(Actor actor, Actor otherActor, MoveEvent moveEvent, World world)
        {
            return Handle(actor, moveEvent);
        }

        public IEnumerable<Event> Handle(Actor actor, MoveEvent moveEvent)
        {
            yield return moveEvent;
            yield return new DespawnEvent(actor.Id, moveEvent.NewLocation, Tags);
        }

    }
}
