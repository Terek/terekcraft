﻿using System;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.actions;
using TerekCraftEngine.collisionhandlers;
using TerekCraftEngine.functional;

namespace TerekCraftEngine
{
    public class ActorType : Taggable
    {
        readonly double _size;
        readonly int _maxHitPoints;
        readonly string _name;

        readonly ICollisionHandler _collisionHandler;
        readonly ITerrainHandler _terrainHandler;

        public int MaxHitPoints { get { return _maxHitPoints; } }
        public string Name { get { return _name; } }
        public double Size { get { return _size; } }
        public ICollisionHandler CollisionHandler { get { return _collisionHandler; } }
        public ITerrainHandler TerrainHandler { get { return _terrainHandler; } }

        readonly Dictionary<int, BaseAction> _actions = new Dictionary<int, BaseAction>();

        public ActorType(string name, int maxHitPoints, double size, IEnumerable<BaseAction> actions = null, IEnumerable<string> tags = null, ICollisionHandler collisionHandler = null, ITerrainHandler terrainHandler = null) : base(tags)
        {
            _collisionHandler = collisionHandler ?? new DefaultCollisionHandler();
            _terrainHandler = terrainHandler ?? new DefaultCollisionHandler();

            if (actions == null) actions = new List<BaseAction>();

            _maxHitPoints = maxHitPoints;
            _size = size;
            _name = name;

            foreach (var action in actions)
            {
                _actions.Add(action.ActionId, action);
            }

        }

        public T GetActionByType<T>() where T : BaseAction
        {
            T action = (T) _actions.Values.FirstOrDefault(item => item.GetType() == typeof(T));
            return action;
        }

        public Option<BaseAction> FindActionById(int actionId)
        {
            return _actions.Find(actionId);
        }

        public BaseAction GetActionById(int actionId)
        {
            return _actions.GetOrNull(actionId);
        }


        public Actor CreateActor()
        {
            return new Actor(this);
        }
    }
}