﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TerekCraftEngine.actions;
using TerekCraftEngine.events;
using TerekCraftEngine.functional;
using TerekCraftEngine.modifiers;
using TerekCraftEngine.spatial;
using TerekCraftEngine.commands;



namespace TerekCraftEngine
{
    public class Actor : ISpatialData
    {
        Dictionary<int, int> _cooldowns = new Dictionary<int, int>(); 

        readonly List<BaseModifier> _modifiers = new List<BaseModifier>();

        readonly string id;

        public string Id { get { return id; } }

        public Point Location { get; set; }

        public double Size
        {
            get { return _type.Size; }
        }

        public double Direction { get; set; }
        public int HitPoints { get; set; }
        public double Aim { get; set; }
        public double Speed { get; set; }

        readonly ActorType _type;

        public string Team { get; private set; }

        public ActorType Type
        {
            get { return _type; }
        }

        public Actor(ActorType type)
        {
            _type = type;
            HitPoints = type.MaxHitPoints;
            Team = null;
            id = "";
        }
        public Actor(ActorType type, string team)
        {
            _type = type;
            HitPoints = type.MaxHitPoints;
            Team = team;
            id = "";
        }

        private Actor(Actor actor, string newId)
        {
            id = newId;
            Direction = actor.Direction;
            Location = actor.Location;
            HitPoints = actor.HitPoints;
            Speed = actor.Speed;
            _type = actor.Type;
            Team = actor.Team;
            _modifiers = new List<BaseModifier>(actor._modifiers);
        }

        public Actor GetNew(String newId)
        {
            return new Actor(this, newId);
        }
  
        public override string ToString()
        {
            return "Actor '" + Id + "' hitPoints: " + HitPoints + ", " + Location;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Actor;
            if (other == null)
            {
                return false;
            }
            return Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() + GetType().GetHashCode() * 31;
        }

        public bool CompletelyEquals(object obj)
        {
            var other = obj as Actor;
            if (other == null)
            {
                return false;
            }
            return Id.Equals(other.Id)
                   && Location.Equals(other.Location)
                   && Direction.Equals(other.Direction)
                   && HitPoints.Equals(other.HitPoints)
                   && Size.Equals(other.Size);

        }


        public bool HasCooldown(int actionId)
        {
            return _cooldowns.ContainsKey(actionId);
        }

        public bool IsDespawning
        {
            get { return HitPoints <= 0;  }
        }

        public void AddCooldown(int actionId, int cooldown)
        {
            _cooldowns.Add(actionId, cooldown);
        }

        public void UpdateCooldowns()
        {
            var newCooldowns = new Dictionary<int, int>();

            foreach (var entry in _cooldowns)
            {
                int newCooldown = entry.Value - 1;
                if (newCooldown > 0)
                {
                    newCooldowns[entry.Key] = newCooldown;                    
                }

            }

            _cooldowns = newCooldowns;
        }



        public void AddModifier(BaseModifier modifier)
        {
            _modifiers.Add(modifier);
        }


        public IEnumerable<Event> TickModifiers(World world)
        {
            List<Event> events = new List<Event>();

            foreach (var modifier in _modifiers)
            {
                events.AddRange(modifier.Tick(this, world));
            }

            return events;
        }


        public IEnumerable<Event> ProcessSpeed()
        {
            if (Speed != 0)
            {
                Point newLocation = Location.Move(Direction, Speed);

                yield return new MoveEvent(Id, Location, newLocation);
            }
        }


        public IEnumerable<Event> HandleCommand<T>(T command, World world) where T : Command
        {
            return InternalHandleCommand(command, world) ?? Enumerable.Empty<Event>();
        }

        private IEnumerable<Event> InternalHandleCommand<T>(T command, World world) where T : Command
        {
            if (!HasCooldown(command.ActionId))
            {
                return Type.GetActionById(command.ActionId)?.ProcessCommand(command, this, world);
            }
            return null;
        }
    }
}