﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;
using TerekCraftEngine.functional;
using TerekCraftEngine.spatial;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine
{
    public class World
    {
        readonly Random _random = new Random();

        Dictionary<String, Actor> _actorDictionary = new Dictionary<string, Actor>();
        Dictionary<Type, List<Command>> _commands = new Dictionary<Type, List<Command>>();
        List<Event> _tickEvents = new List<Event>();
        List<SpawnEvent> _spawnEvents = new List<SpawnEvent>();

        SpatialDatabase<Actor> _spatialDatabase = CreateSpatialDatabase();

        ITerrain<Polygon> terrain = new DefaultTerrain<Polygon>();

        private IList<Polygon> _map = null;

        int _frameNumber = 0;
        int _actorCount = 0;
        
        public AiTools AiTools;

        public IList<Polygon> Map { get { return _map; } }

        public SpatialDatabase<Actor> SpatialDatabase {get { return _spatialDatabase; } }

        public IEnumerable<Actor> Actors { get { return _actorDictionary.Values; } }

        public int FrameNumber
        {
            get { return _frameNumber; }
        }

        public Random Random { get { return _random; } }

        public void SetMap(List<Polygon> terrain)
        {
            this._map = new List<Polygon>(terrain).AsReadOnly();
            this.terrain = new BSPTree<Polygon>(terrain);
        }

        public World()
        {
            AiTools = new AiTools(this);
        }
        
        public void Tick()
        {
            _frameNumber++;

            var commandsByActors = _commands.Values
                .SelectMany(commandList => commandList)
                .GroupBy(command => command.ActorId)
                .ToDictionary(item => item.Key, item => item.ToList());

            var events = this.Actors.ToList().SelectMany(actor =>
            {
                var dictionary = commandsByActors.GetOrDefault(actor.Id, new List<Command>())
                    .GroupBy(command => command.GetType())
                    .ToDictionary(item => item.Key, item => item.ToList());

                return TickActor(actor, dictionary);
            });


            events = events.Concat(ProcessDeadActors());
            events = events.Concat(_spawnEvents);

            _tickEvents = events.ToList();      
            _tickEvents.AsParallel().GroupBy(actorEvent => actorEvent.ActorId).ForAll(ProcessTickGroupEvent);

            _spawnEvents = new List<SpawnEvent>();
      
            _commands = new Dictionary<Type, List<Command>>();

            RebuildSpatialDatabase();
        }

        private IEnumerable<Event> TickActor(Actor actor, Dictionary<Type, List<Command>> commands)
        {
            List<Event> results = new List<Event>();

            results.AddRange(HandleActorEvents(actor, commands, typeof(AimCommand), typeof(TurnCommand)));
            results.AddRange(HandleActorEvents(actor, commands, typeof(ShootMissileCommand)));
            results.AddRange(ProcessMoveCommands(actor, commands, typeof(MoveCommand), typeof(MoveForwardCommand)));
            results.AddRange(HandleActorEvents(actor, commands, typeof(ShootAimedCommand), typeof(ShootForwardCommand), typeof(ShootCommand)));
            results.AddRange(actor.TickModifiers(this));
            actor.UpdateCooldowns();

            return results;
        }

        private IEnumerable<Event> HandleActorEvents(Actor actor, Dictionary<Type, List<Command>> commands, params Type[] types)
        {
            return types
                .SelectMany(type => commands.GetOrDefault(type, new List<Command>()))
                .SelectMany(command => actor.HandleCommand(command, this));
        }

        private void RebuildSpatialDatabase()
        {
            lock (_actorDictionary)
            {
                _spatialDatabase = CreateSpatialDatabase();
                foreach (var actor in _actorDictionary.Values)
                {
                    _spatialDatabase.Add(actor);
                }
            }
        }

        private static SpatialDatabase<Actor> CreateSpatialDatabase()
        {
            return new SpatialDatabase<Actor>(new QuadTree<Actor>(10, -100, -100, 100, 100));
            // return new SpatialDatabase<Actor>(new NaiveSpatialDatabase<Actor>());
        }

        private void StoreActor(Actor actor)
        {
            lock (_actorDictionary)
            {
                _actorDictionary.Add(actor.Id, actor);
            }
        }

        private void RemoveActor(string actorId)
        {
            lock (_actorDictionary)
            {
                _actorDictionary.Remove(actorId);
            }
        }

        public Actor GetActor(string newId)
        {
            lock (_actorDictionary)
            {
                return _actorDictionary.GetOrNull(newId);
            }
        }


        public Option<Actor> FindActor(string newId)
        {
            lock (_actorDictionary)
            {
                return _actorDictionary.Find(newId);
            }
        }


        private void ProcessTickGroupEvent(IGrouping<string, Event> actorEvents)
        {
            foreach (var tickEvent in actorEvents)
            {
                ProcessTickEvent(tickEvent);
            }
        }

        private void ProcessTickEvent(Event tickEvent)
        {
            if (tickEvent is AimEvent)
            {
                AimActor(tickEvent as AimEvent);
            }
            else if (tickEvent is MoveEvent)
            {
                MoveActor(tickEvent as MoveEvent);
            }
            else if (tickEvent is TurnEvent)
            {
                TurnActor(tickEvent as TurnEvent);
            }
            else if (tickEvent is DamageEvent)
            {
                DamageActor(tickEvent as DamageEvent);
            }
            else if (tickEvent is DespawnEvent)
            {
                DespawnActor(tickEvent as DespawnEvent);
            }
        }

        private void DespawnActor(DespawnEvent despawnEvent) => RemoveActor(despawnEvent.ActorId);

        private void DamageActor(DamageEvent damageEvent) => GetActor(damageEvent.Target).HitPoints -= damageEvent.Damage;

        private void AimActor(AimEvent aimEvent) => GetActor(aimEvent.ActorId).Aim = aimEvent.NewDirection;

        private void MoveActor(MoveEvent moveEvent) => GetActor(moveEvent.ActorId).Location = moveEvent.NewLocation;

        private void TurnActor(TurnEvent turnEvent) => GetActor(turnEvent.ActorId).Direction = turnEvent.NewDirection;

        private IEnumerable<Event> ProcessDeadActors()
        {
            return Actors
                .Where(actor => actor.IsDespawning)
                .Select(actor => new DespawnEvent(actor.Id, actor.Location));
        }

        private IEnumerable<Event> ProcessMoveCommands(Actor actor, Dictionary<Type, List<Command>> commands, params Type[] eventTypes)
        {
            var events = HandleActorEvents(actor, commands, eventTypes).ToList();
            var speedEvents = actor.ProcessSpeed();

            events.AddRange(speedEvents);

            var processedEvents = ProcessTerrainCollisions(events).ToList();
            processedEvents = ProcessUnitCollisions(processedEvents).ToList();

            return processedEvents;
        }

        private IEnumerable<Event> ProcessTerrainCollisions(List<Event> events)
        {
            return events.SelectMany(tickEvent =>
            {
                var moveEvent = (MoveEvent)tickEvent;
                var actor = GetActor(moveEvent.ActorId);

                if (terrain.HasCollision(moveEvent.NewLocation, actor.Size))
                {
                    return actor.Type.TerrainHandler.HandleCollision(actor, moveEvent, this);
                }
                else
                {
                    return Option<Event>.Create(tickEvent);
                }
            });

        }


        private IEnumerable<Event> ProcessUnitCollisions(IEnumerable<Event> events)
        {
            return events.AsParallel().SelectMany(tickEvent =>
            {
                var moveEvent = (MoveEvent)tickEvent;

                var movingActor = GetActor(moveEvent.ActorId);

                var collisionActors = SpatialDatabase.RetreiveInRange(moveEvent.NewLocation, movingActor.Size)
                    .Where(otherActor => otherActor.Id != movingActor.Id);

                if (collisionActors.Any())
                {
                    return collisionActors.SelectMany(collisionActor =>
                    {
                        return movingActor.Type.CollisionHandler.HandleCollision(movingActor,
                            collisionActor, moveEvent, this);

                    });
                }
                else
                {
                    return MyExtensions.ToEnumerable(tickEvent);
                }

            });
        }

        public SpawnEvent AddActor(Actor actor)
        {
            SpawnEvent spawnEvent = SpawnActor(actor);

            this._spawnEvents.Add(spawnEvent);

            return spawnEvent;
        }

        public SpawnEvent SpawnActor(Actor actor)
        {
            actor = actor.GetNew("actor" + GetNextActorId());
            StoreActor(actor);

            return new SpawnEvent(actor);
        }

        public void Turn(string actorId, double newDirection)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<TurnAction>().CreateCommand(actorId, newDirection));
        }

        public void MoveForward(string actorId, double distance)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<MoveForwardAction>().CreateCommand(actorId, distance));
        }

        public void Move(string actorId, double direction, double distance)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<MoveAction>().CreateCommand(actorId, direction, distance));
        }

        public void ShootForward(string actorId)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<ShootForwardAction>().CreateCommand(actorId));
        }


        public void Shoot(string actorId, double direction)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<ShootAction>().CreateCommand(actorId, direction));
        }

        public void ShootMissile(string actorId, double direction)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<ShootMissileAction>().CreateCommand(actorId, direction));
        }


        public void Aim(string actorId, double newDirection)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<AimAction>().CreateCommand(actorId, newDirection));
        }

        public void ShootAimed(string actorId)
        {
            AddCommand(_actorDictionary[actorId].Type.GetActionByType<ShootAimedAction>().CreateCommand(actorId));
        }

        public void AddCommand(IEnumerable<Command> commands)
        {
            foreach (var command in commands)
            {
                AddCommand(command);
            }
        }

        public void AddCommand(Command command)
        {
            List<Command> list;
            Type commandType = command.GetType();

            if (!_commands.TryGetValue(commandType, out list))
            {
                list = new List<Command>();
                _commands.Add(commandType, list);
            }

            list.Add(command);
        }

        public List<Event> GetTickEvents()
        {
            return _tickEvents;
        }

        private int GetNextActorId()
        {
            return Interlocked.Increment(ref _actorCount);
        }
    }
}
