﻿namespace TerekCraftEngine
{
    public interface IScenario
    {
        void Init(World world);

        void Update(World world);
    }
}