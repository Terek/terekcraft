﻿using TerekCraftEngine.terrain;

namespace TerekCraftEngine.terrain
{
    public interface ITerrain<T> where T : Polygon
    {
        T GetPolygonForPoint(Point point);
        bool IsOutside(Point point);

        bool HasCollision(Point center, double radius);

    }
}