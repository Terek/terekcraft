﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.spatial;

namespace TerekCraftEngine.terrain
{
    public class Polygon
    {
        readonly List<Point> _points;
        readonly List<Edge> _edges;

        public Polygon(params Point[] points)
        {
            _points = new List<Point>(points);
            _edges = CreateFromPointList();
        }

        public Polygon(IEnumerable<Point> points)
        {
            _points = new List<Point>(points);
            _edges = CreateFromPointList();            
        }

        public List<Edge> Edges
        {
            get { return _edges; }
        }

        List<Edge> CreateFromPointList()
        {
            var edges = new List<Edge>();

            if (_points.Count == 0)
            {
                return edges;
            }

            for (int i = 1; i < _points.Count; i++)
            {
                edges.Add(new Edge(_points[i - 1], _points[i]));
            }

            edges.Add(new Edge(_points[_points.Count - 1], _points[0]));

            return edges;
        }

        public bool IsConvex()
        {
            Edge lastEdge = null;

            foreach (var edge in Edges)
            {
                if (lastEdge != null)
                {
                    if (lastEdge.IsRight(edge.EndPoint))
                    {
                        return false;
                    }
                }
                lastEdge = edge;
            }

            return true;
        }

        public List<Point> Points
        {
            get
            {
                return _points;
            }
        }

        protected bool Equals(Polygon other)
        {
            if (Points.Count != other.Points.Count)
            {
                return false;
            }

            int offset = other.Points.IndexOf(this.Points[0]);

            if (offset < 0)
            {
                return false;
            }

            if (offset == 0) return Points.SequenceEqual(other.Points);

            for (int i = 0; i < Points.Count; i++)
            {
                int index = i + offset;
                if (index >= other.Points.Count)
                {
                    index -= other.Points.Count;
                }

                if (!Points[i].Equals(other.Points[index]))
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Polygon) obj);
        }

        public override int GetHashCode()
        {
            return (Points != null ? Points.GetHashCode() : 0);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append("<polygon>");
            builder.Append("\n");
            foreach (var point in Points)
            {
                builder.Append(point);
                builder.Append("\n");
            }
            builder.Append("</polygon>");
            builder.Append("\n");

            return builder.ToString();
        }

        public bool HasCommonEdge(Polygon polygon2)
        {
            return CommonEdge(polygon2) != null;
        }

        public Edge CommonEdge(Polygon polygon2)
        {
            foreach (var edge in Edges)
            {
                foreach (var otherEdge in polygon2.Edges)
                {
                    if (edge.Equals(otherEdge) || edge.Equals(otherEdge.Reverse()))
                    {
                        return edge;
                    }
                }
            }

            return null;
        }

        public bool IsInside(Point point)
        {
            bool isInside = true;

            foreach (Edge edge in Edges)
            {
                isInside = isInside && ! edge.IsRight(point);
            }

            return isInside;
        }

        public Point GetCentroid()
        {
            double signedArea = 0.0;

            double centroidX = 0;
            double centroidY = 0;

            // For all vertices
            foreach (Edge edge in _edges)
            {
                double a = edge.StartPoint.X * edge.EndPoint.Y - edge.EndPoint.X * edge.StartPoint.Y;
                signedArea += a;
                centroidX += (edge.StartPoint.X + edge.EndPoint.X) * a;
                centroidY += (edge.StartPoint.Y + edge.EndPoint.Y) * a;
            }

            signedArea *= 0.5;
            centroidX /= (6.0 * signedArea);
            centroidY /= (6.0 * signedArea);

            return new Point(centroidX, centroidY);
        }
    }

}
