﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.spatial;

namespace TerekCraftEngine.terrain
{
    class DefaultTerrain<T> : ITerrain<T> where T : Polygon

    {
        public T GetPolygonForPoint(Point point)
        {
            return null;
        }

        public bool HasCollision(Point center, double radius)
        {
            return false;
        }

        public bool IsOutside(Point point)
        {
            return false;
        }
    }
}
