﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine.terrain
{
    public class BSPTree<T> : ITerrain<T> where T : Polygon
    {
        readonly BSPTree<T> _leftTree = null;
        readonly BSPTree<T> _rightTree = null;

        readonly Edge _splitterEdge = null;

        readonly bool _isPortal;

        readonly List<T> _polygonList;

        readonly Dictionary<Edge, T> _polygonsForEdges;


        public BSPTree(IList<T> polygons) : this(polygons, GetEdges(polygons), GetPolygonsForEdges(polygons) )
        {
        }

        static ICollection<Edge> GetEdges(IEnumerable<T> polygons)
        {
            ICollection<Edge> edges = new HashSet<Edge>();

            foreach (var polygon in polygons)
            {
                foreach (var edge in polygon.Edges)
                {
                    edges.Add(edge);
                }
            }

            return edges;
        }

        static Dictionary<Edge, T> GetPolygonsForEdges(IEnumerable<T> polygons)
        {
            var polygonsForEdges = new Dictionary<Edge, T>();

            foreach(T polygon in polygons)
            {
                foreach (Edge edge in polygon.Edges)
                {
                    polygonsForEdges.Add(edge, polygon);
                }
            }


            return polygonsForEdges;
        }


        BSPTree(IList<T> polygons, ICollection<Edge> edges, Dictionary<Edge, T> polygonsForEdges)
        {
            _polygonsForEdges = polygonsForEdges;
            _polygonList = new List<T>(polygons);


            if(_polygonList.Count == 1)
            {
                return;
            }
            else if(edges.Count > 0)
            {
                _splitterEdge = GetSpliterEdge(edges);
            }
            else if (_polygonList.Count > 1)
            {
                _splitterEdge = GetSpliterEdge(_polygonList);
            }
            else
            {
                return;
            }

            _isPortal = IsPortal(_splitterEdge, edges);

            var leftPolys = GetLeftPolys(_splitterEdge, polygons);
            var rightPolys = GetRightPolys(_splitterEdge, polygons);

            var leftEdges = _splitterEdge.GetLeftSlices(edges);
            var rightEdges = _splitterEdge.GetRightSlices(edges);

            _leftTree = new BSPTree<T>(leftPolys, leftEdges, polygonsForEdges);
            _rightTree = new BSPTree<T>(rightPolys, rightEdges, polygonsForEdges);
        }

        static bool IsPortal(Edge edge, ICollection<Edge> edges)
        {
            return edges.Any(item => item.StartPoint.Equals(edge.EndPoint) && item.EndPoint.Equals(edge.StartPoint));
        }

        List<T> GetLeftPolys(Edge splitterEdge, IEnumerable<T> polygons)
        {
            return polygons.Where(splitterEdge.IsPolyOnLeft).ToList();
        }


        List<T> GetRightPolys(Edge splitterEdge, IEnumerable<T> polygons)
        {
            return polygons.Where(splitterEdge.IsPolyOnRight).ToList();
        }


        Edge GetSpliterEdge(ICollection<Edge> edges)
        {
            var candidate = GetSpliterEdge(edges.Where(edge => IsPortal(edge, edges)).ToList(), _polygonList);

            return candidate ?? GetSpliterEdge(edges, _polygonList);
        }


        Edge GetSpliterEdge(List<T> polygonList)
        {
            return GetSpliterEdge(GetEdges(polygonList), polygonList);
        }

        Edge GetSpliterEdge(ICollection<Edge> edges, List<T> polygonList)
        {
            return edges.OrderBy(item => SplitGoodness(item, polygonList)).FirstOrDefault();
        }


        int SplitGoodness(Edge edge, List<T> polygonList)
        {
            int leftPolyCount = GetLeftPolys(edge, polygonList).Count;
            int rightPolyCount = GetRightPolys(edge, polygonList).Count;

            var leftRightDiff = Math.Abs(leftPolyCount - rightPolyCount);
            var additionalPolys = leftPolyCount + rightPolyCount - polygonList.Count ;

            return leftRightDiff + additionalPolys;
        }


        ICollection<T> GetPolygonsForPointNonRecursive(Point point)
        {
            var treeNode = this;

            while (treeNode._splitterEdge != null)
            {
                // System.Console.WriteLine("splitter edge: " + treeNode._splitterEdge);

                if (treeNode._splitterEdge.IsRight(point))
                {
                    //System.Console.WriteLine("going right");
                    treeNode = treeNode._rightTree;
                }
                else
                {
                    //System.Console.WriteLine("going left");
                    treeNode = treeNode._leftTree;
                }

            }

            return new List<T>(treeNode._polygonList);

        }

        public bool IsOutside(Point point)
        {
            return GetPolygonForPoint(point) == null;
        }

        public T GetPolygonForPoint(Point point)
        {
            return GetPolygonsForPointNonRecursive(point).FirstOrDefault( item => item.IsInside(point));
        }

        public bool HasCollision(Point center, double radius)
        {
            var polygon = GetPolygonForPoint(center);

            if (polygon == null)
            {
                return true;
            }

            bool retValue = false;

            retValue = CheckPolygonForCollision(center, radius, polygon, new HashSet<Edge>());

            return retValue;

        }

        bool CheckPolygonForCollision(Point center, double radius, T polygon, HashSet<Edge> portalsAlreadyChecked)
        {
            foreach (Edge edge in polygon.Edges)
            {
                if (edge.IsIntersecting(center, radius) == Intersection.IntersectSegment)
                {
                    T neighbor = null;
                    _polygonsForEdges.TryGetValue(edge.Reverse(), out neighbor);

                    if (neighbor == null)
                    {
                        return true;
                    }
                    else
                    {
                        if (!portalsAlreadyChecked.Contains(edge))
                        {
                            portalsAlreadyChecked.Add(edge);
                            if (CheckPolygonForCollision(center, radius, neighbor, portalsAlreadyChecked))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
    }
}
