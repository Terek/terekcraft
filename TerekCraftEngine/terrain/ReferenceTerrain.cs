﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine.terrain
{
    public class ReferenceTerrain<T> : ITerrain<T> where T : Polygon
    {
        readonly List<T> polygons;

        public ReferenceTerrain(List<T> polygons)
        {
            this.polygons = polygons;
        }


        public T GetPolygonForPoint(Point point)
        {
            foreach(var polygon in polygons)
            {
                if (IsInside(point, polygon)) return polygon;
            }

            return null;
        }


        static bool IsInside(Point point, T polygon)
        {
            bool isInside = true;

            foreach (Edge edge in polygon.Edges)
            {
                isInside = isInside && ! edge.IsRight(point);
            }

            return isInside;
        }

        public bool HasCollision(Point center, double radius)
        {
            var poly = GetPolygonForPoint(center);

            if (poly == null)
            {
                return true;
            }

            return CheckPolyForCollision(center, radius, poly, new HashSet<Edge>());
        }

        bool CheckPolyForCollision(Point center, double radius, T poly, HashSet<Edge> visitedPortals)
        {
            bool hasCollision = false;
            foreach (Edge edge in poly.Edges)
            {
                if (edge.IsIntersecting(center, radius) == Intersection.IntersectSegment)
                {
                    var otherPoly = this.polygons.Find(item => item.Edges.Contains(edge.Reverse()));

                    if (otherPoly == null)
                    {
                        hasCollision = true;
                        break;
                    }
                    else
                    {
                        if (!visitedPortals.Contains(edge))
                        {
                            visitedPortals.Add(edge);
                            hasCollision = CheckPolyForCollision(center, radius, otherPoly, visitedPortals);
                            if (hasCollision) break;
                        }
                    }
                }
            }

            return hasCollision;
        }

        public bool IsOutside(Point point)
        {
            return GetPolygonForPoint(point) == null;
        }
    }
}
