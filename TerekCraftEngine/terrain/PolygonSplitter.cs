﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TerekCraftEngine.terrain
{
    public class PolygonSplitter
    {
        Polygon _polygon;

        public PolygonSplitter(Polygon polygon)
        {
            _polygon = polygon;
        }

        public List<Polygon> GetConvexSubPolygons()
        {
            return SplitANonConvexSubPolygon();
        }

        List<Polygon> SplitANonConvexSubPolygon()
        {
            //System.Console.WriteLine("splitting: " + _polygon);

            var splitPolygons = new List<Polygon>();
            if (_polygon.IsConvex())
            {
                splitPolygons.Add(_polygon);
            }
            else
            {
                var points = _polygon.Points.ToList();
                points.Add( points[0]);
                if (points.Count >= 3)
                {
                    for (int i = 2; i < points.Count; i++)
                    {
                        var lastValidEdge = new Edge(points[i-2], points[i-1]);
                        var point = points[i];
                        if (lastValidEdge.IsRight(point))
                        {
                            var splitEdge = FindSplitEdge(lastValidEdge, i);

                            IEnumerable<Polygon> subpolygons = SplitPolygon(_polygon, splitEdge);

                            foreach (var subpolygon in subpolygons)
                            {
                                splitPolygons.AddRange(new PolygonSplitter(subpolygon).GetConvexSubPolygons());                                
                            }
                            break;
                        }
                    }
                }
            }

            return splitPolygons;
        }

        Edge FindSplitEdge(Edge lastValidEdge, int i)
        {
            IEnumerable<int> candidateIndices = FindSplitCandidateEdges(lastValidEdge, i);

            if (!candidateIndices.Any())
            {
                throw new Exception();
            }

            int bestCandidate = candidateIndices.OrderBy(item => lastValidEdge.EndPoint.GetDistanceSquaredTo(_polygon.Points[item])).FirstOrDefault();

            return GetSplittingEdge(i - 1, bestCandidate);
        }

        Edge GetSplittingEdge(int point1Index, int point2Index)
        {
            int startIndex = Math.Min(point1Index, point2Index);
            int endIndex = Math.Max(point1Index, point2Index);

            return new Edge(_polygon.Points[startIndex], _polygon.Points[endIndex]);
        }

        IEnumerable<Polygon> SplitPolygon(Polygon polygon, Edge splittingEdge)
        {
            var poly1Points = GetLeftSplitSubPolygon(polygon, splittingEdge);
            var poly2Points = GetRightSplitSubPolygon(polygon, splittingEdge);

            var list = new List<Polygon>
                {
                    new Polygon(poly1Points.ToArray()), new Polygon(poly2Points.ToArray())
                };

            return list;

        }

        static List<Point> GetSplitSubPolygon(Polygon polygon, Edge splittingEdge, bool between)
        {
            var newPoints = new List<Point>();

            bool polyStarted = !between;

            foreach (var point in polygon.Points)
            {
                if (splittingEdge.StartPoint.Equals(point) || splittingEdge.EndPoint.Equals(point))
                {
                    newPoints.Add(point);
                    polyStarted = !polyStarted;
                }
                else if (polyStarted)
                {
                    newPoints.Add(point);
                }
            }

            return newPoints;
        }

        static List<Point> GetRightSplitSubPolygon(Polygon polygon, Edge splittingEdge)
        {
            return GetSplitSubPolygon(polygon, splittingEdge, true);
        }

        static List<Point> GetLeftSplitSubPolygon(Polygon polygon, Edge splittingEdge)
        {
            return GetSplitSubPolygon(polygon, splittingEdge, false);
        }

        IEnumerable<int> FindSplitCandidateEdges(Edge lastValidEdge, int firstNonConvexIndex)
        {
            if (firstNonConvexIndex >= _polygon.Points.Count)
            {
                firstNonConvexIndex -= _polygon.Points.Count;
            }

            for(int i = firstNonConvexIndex; i < _polygon.Points.Count; i++)
            {
                Point point = _polygon.Points[i];
                if (IsValidCandidateEdgeForSplit(lastValidEdge, point))
                {
                    yield return i;
                }
            }

            for (int i = 0; i < firstNonConvexIndex - 2; i++)
            {
                Point point = _polygon.Points[i];
                if (IsValidCandidateEdgeForSplit(lastValidEdge, point))
                {
                    yield return i;
                }
            }
        }

        bool IsValidCandidateEdgeForSplit(Edge lastValidEdge, Point point)
        {
            if (point.Equals(lastValidEdge.EndPoint))
            {
                return false;
            }

            if (point.Equals(lastValidEdge.StartPoint))
            {
                return false;
            }

            if(lastValidEdge.IsRight(point))
            {
                return false;
            }

            Edge candidateEdge = new Edge(lastValidEdge.EndPoint, point);
            foreach (var otherPoint in _polygon.Points)
            {
                if (!otherPoint.Equals(lastValidEdge.EndPoint) && !otherPoint.Equals(point) &&
                    candidateEdge.IsPointOnEdge(otherPoint))
                {
                    return false;
                }
            }

            foreach (var otherEdge in _polygon.Edges)
            {
                if (candidateEdge.IsIntersecting(otherEdge) && !candidateEdge.HasCommonEndpoints(otherEdge))
                {
                    return false;
                }
            }


            return true;
        }
    }
}