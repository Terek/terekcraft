﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine.terrain
{
    public class PolyImporter
    {
        int xIndex = 0;
        int yIndex = 1;

        public PolyImporter() { }

        public PolyImporter(int xIndex, int yIndex)
        {
            this.xIndex = xIndex;
            this.yIndex = yIndex;
        }


        public List<Polygon> ConvertInput(String input)
        {
            String[] lines = input.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            return ConvertInput(lines);
        }

        public List<Polygon> ConvertInput(string[] lines)
        {
            List<Polygon> polys = new List<Polygon>();
            List<Point> points = new List<Point>();

            foreach(var line in lines)
            {
                ProcessLine(polys, points, line);
            }

            return polys;
        }


        public List<Polygon> ConvertInput(StreamReader reader)
        {
            List<Polygon> polys = new List<Polygon>();
            List<Point> points = new List<Point>();

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                // Console.WriteLine(line);
                ProcessLine(polys, points, line);

            }

            reader.Close();

            return polys;
        }



        public void ProcessLine(List<Polygon> polys, List<Point> points, string line)
        {
            if (line.StartsWith("v "))
            {
                points.Add(ConvertPoint(line));
            }
            else if (line.StartsWith("f "))
            {
                var polygon = ConvertPoly(line, points);

                if (!polygon.IsConvex())
                {
                    // System.Console.WriteLine("Polygon is not convex, splitting up : " + polygon);
                    polys.AddRange(new PolygonSplitter(polygon).GetConvexSubPolygons());
                }
                else
                {
                    polys.Add(polygon);
                }


            }
        }

        public Polygon ConvertPoly(string line, List<Point> points)
        {
            if (string.IsNullOrEmpty(line))
            {
                throw new ArgumentException("Illegal input: " + line);
            }

            string[] splits = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);

            if (splits.Length < 4 || splits[0] != "f")
            {
                throw new ArgumentException("Illegal input: " + line);
            }

            List<Point> polyPoints = new List<Point>();

            for(int i = 1; i < splits.Length; i++)
            {
                int index = GetFirstIndex(splits[i]);
                polyPoints.Add( points[index - 1]);
            }

            var polygon = new Polygon(polyPoints);

            return polygon;

        }

        int GetFirstIndex(string input)
        {
            var splits = input.Split('/');
            try { 
                return Int32.Parse(splits[0]);
            }
            catch(Exception e)
            {
                throw new ArgumentException("Could not parsse: " + input, e);
            }
        }

        public Point ConvertPoint(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException("Illegal input: " + input);
            }

            string[] splits = input.Split(null);

            if(splits.Length != 4  || splits[0] != "v")
            {
                throw new ArgumentException("Illegal input: " + input);
            }

            double x = Double.Parse(splits[ xIndex + 1], CultureInfo.InvariantCulture);
            double y = Double.Parse(splits[ yIndex + 1], CultureInfo.InvariantCulture);

            return new Point(x, y);
        }
    }
}