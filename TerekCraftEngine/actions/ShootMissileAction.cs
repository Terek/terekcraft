﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.collisionhandlers;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;
using TerekCraftEngine.modifiers;

namespace TerekCraftEngine.actions
{
    public class ShootMissileAction : TypedBaseAction<ShootMissileCommand, ShootMissileCommand.CommandParameter>
    {
        readonly double _speed;
        readonly double _radius;
        readonly int _damage;
        readonly int _timeToLive;

        readonly ActorType _missileType;

        public double Speed { get { return _speed; } }
        public double Radius { get { return _radius; } }
        public int Damage { get { return _damage; } }
        public int TimeToLive { get { return _timeToLive; } }

        public ShootMissileAction(double speed, double radius, int damage, int timeToLive, params string[] tags) 
            : this(speed, radius, damage, timeToLive, new HashSet<string>(tags))
        {
        }

        public ShootMissileAction(double speed, double radius, int damage, int timeToLive, HashSet<string> tags) : base(tags)
        {
            _speed = speed;
            _radius = radius;
            _damage = damage;
            _timeToLive = timeToLive;

            var explosionCollisionHandler = new ExplosionCollisionHandler(_damage, _radius, tags);
            _missileType = new ActorType("missile", 1, 0.1f, collisionHandler : explosionCollisionHandler);
        }


        public override IEnumerable<Event> ProcessCommand(ShootMissileCommand command, Actor actor, World world)
        {
            Actor missile = new Actor(_missileType, actor.Team)
            {
                Direction = command.Direction,
                Location = actor.Location,
                Speed = _speed
            };

            missile.AddModifier(new TimeToExplode(_timeToLive, _damage, _radius, Tags));

            yield return world.SpawnActor(missile);

            yield break;
        }

        public override ShootMissileCommand CreateCommand(string actorId, ShootMissileCommand.CommandParameter parameter)
        {
            return new ShootMissileCommand(actorId, this.ActionId, parameter);
        }


        public ShootMissileCommand CreateCommand(string actorId, double direction)
        {
            return new ShootMissileCommand(actorId, this.ActionId, new ShootMissileCommand.CommandParameter(direction));
        }


    }
}
