﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine.actions
{
    public abstract class TypedBaseAction<T, U> : BaseAction where T : Command  where U : CommandParameter<T>
    {
        public TypedBaseAction(params string[] tags) : base(new HashSet<string>(tags))
        {

        }

        public TypedBaseAction(IEnumerable<string> tags) : base(tags)
        {

        }

        protected override IEnumerable<Event> InternalProcessCommand(Command command, Actor actor, World world)
        {
            return ProcessCommand((T)command, actor, world);
        }

        public override Type GetCommandType()
        {
            return typeof (T);
        }

        public abstract IEnumerable<Event> ProcessCommand(T command, Actor actor, World world);

        public abstract T CreateCommand(string actorId, U parameter);

    }
}
