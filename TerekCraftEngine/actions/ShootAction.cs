﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine.actions
{
    public class ShootForwardAction : ShootActionBase<ShootForwardCommand, ShootForwardCommand.CommandParameter>
    {
        public ShootForwardAction(int damage, double range, params string[] tags) : base(damage, range, new HashSet<string>(tags))
        {
        }

        public ShootForwardAction(int damage, double range, IEnumerable<string> tags) : base(damage, range, tags)
        {
        }

        public override ShootForwardCommand CreateCommand(string actorId, ShootForwardCommand.CommandParameter parameter)
        {
            return new ShootForwardCommand(actorId, this.ActionId);
        }

        public ShootForwardCommand CreateCommand(string actorId)
        {
            return new ShootForwardCommand(actorId, this.ActionId);
        }

        protected override double GetDirection(Actor actor, ShootForwardCommand command)
        {
            return actor.Direction;
        }
    }

    public class ShootAction : ShootActionBase<ShootCommand, ShootCommand.CommandParameter>
    {

        public ShootAction(int damage, double range, params string[] tags) : base(damage, range, new HashSet<string>(tags))
        {
        }

        public ShootAction(int damage, double range, IEnumerable<string> tags) : base(damage, range, tags)
        {
        }

        public override ShootCommand CreateCommand(string actorId, ShootCommand.CommandParameter parameter)
        {
            return new ShootCommand(actorId, this.ActionId, parameter);
        }

        public ShootCommand CreateCommand(string actorId, double direction)
        {
            return new ShootCommand(actorId, this.ActionId, new ShootCommand.CommandParameter(direction));
        }

        protected override double GetDirection(Actor actor, ShootCommand command)
        {
            return command.Direction;
        }
    }


    public class ShootAimedAction : ShootActionBase<ShootAimedCommand, ShootAimedCommand.CommandParameter>
    {


        public ShootAimedAction(int damage, double range, params string[] tags) : base(damage, range, new HashSet<string>(tags))
        {
        }

        public ShootAimedAction(int damage, double range, IEnumerable<string> tags) : base(damage, range, tags)
        {
        }


        public ShootAimedAction(int damage, double range) 
            : base(damage, range)
        {
        }

        protected override double GetDirection(Actor actor, ShootAimedCommand command)
        {
            return actor.Aim + actor.Direction;
        }

        public override ShootAimedCommand CreateCommand(string actorId, ShootAimedCommand.CommandParameter parameter)
        {
            return new ShootAimedCommand(actorId, this.ActionId);
        }

        public ShootAimedCommand CreateCommand(string actorId)
        {
            return new ShootAimedCommand(actorId, this.ActionId);
        }

    }



    public abstract class ShootActionBase<T, U> : TypedBaseAction<T, U> where T : Command where U : CommandParameter<T>
    {
        readonly int _damage;
        readonly double _range;


        public ShootActionBase(int damage, double range, params string[] tags) : base(new HashSet<string>(tags))
        {
            _damage = damage;
            _range = range;
        }

        public ShootActionBase(int damage, double range, IEnumerable<string> tags) : base(tags)
        {
            _damage = damage;
            _range = range;
        }

        public double Range{get { return _range; }}
        public int Damage { get { return _damage; } }

        protected abstract double GetDirection(Actor actor, T command);

        public override IEnumerable<Event> ProcessCommand(T command, Actor actor, World world)
        {
            var events = new List<Event>(); 

            double direction = GetDirection(actor, command);

            var shootEvent = new ShootEvent(actor.Id);
            shootEvent.FromLocation = actor.Location;
            shootEvent.ToLocation = actor.Location.Move(direction, Range);
            
            events.Add( shootEvent );
            events.AddRange(FindVictim(actor, world, direction));

            return events;
        }

        IEnumerable<Event> FindVictim(Actor actor, World world, double direction)
        {
            var events = new List<Event>(); 
            foreach (var victim in world.SpatialDatabase.RetreiveInRange(actor.Location, _range))
            {
                if (IsHit(actor, victim, direction))
                {
                    var damageEvent = new DamageEvent(actor.Id) {Target = victim.Id, Damage = Damage};
                    events.Add(damageEvent);
                }
            }

            return events;
        }

        bool IsHit(Actor actor, Actor potentialVictim, double direction)
        {
            if (actor.Equals(potentialVictim)) return false;

            double distance = Math.Sqrt(actor.Location.GetDistanceSquaredTo(potentialVictim.Location));

            if (distance > Range + potentialVictim.Size) return false;

            Point target = actor.Location.Move(direction, distance);

            if (target.GetDistanceSquaredTo(potentialVictim.Location) < EngineMath.sqr(potentialVictim.Size))
            {
                return true;
            }

            return false;
        }

    }

}
