﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine.actions
{
    public class TurnAction : TypedBaseAction<TurnCommand, TurnCommand.CommandParameter>
    {
        readonly double _maxTurnRate;

        public double MaxTurnRate
        {
            get { return _maxTurnRate; }
        }

        public TurnAction(double maxTurnRate, params string[] tags) : base(new HashSet<string>(tags))
        {
            _maxTurnRate = maxTurnRate;
        }

        public TurnAction(double maxTurnRate, IEnumerable<string> tags) : base(tags)
        {
            _maxTurnRate = maxTurnRate;
        }

        public override IEnumerable<Event> ProcessCommand(TurnCommand command, Actor actor, World world)
        {
            double oldDirection = actor.Direction;
            double newDirection = CalculateNewDirection(actor.Type, command.NewDirection, actor.Direction);
            yield return new TurnEvent(actor.Id, oldDirection, newDirection);
        }

        double CalculateNewDirection(ActorType actorType, double newDirection, double oldDirection)
        {
            double diff = EngineMath.NormalizeAngleDiff(newDirection - oldDirection);
            diff = EngineMath.ProcessDiff(diff, MaxTurnRate);

            newDirection = oldDirection + diff;
            newDirection = EngineMath.NormalizeAngle(newDirection);

            return newDirection;
        }

        public override TurnCommand CreateCommand(string actorId, TurnCommand.CommandParameter parameter)
        {
            return new TurnCommand(actorId, this.ActionId, parameter);
        }


        public TurnCommand CreateCommand(string actorId, double newDirection)
        {
            return new TurnCommand(actorId, this.ActionId, new TurnCommand.CommandParameter(newDirection));
        }
    }
}
