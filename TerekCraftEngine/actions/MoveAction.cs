﻿using System;
using System.Collections.Generic;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine.actions
{
    public abstract class BaseMoveAction<T, U> : TypedBaseAction<T, U> where T : Command where U : CommandParameter<T>
    {
        protected BaseMoveAction(double maxSpeed, IEnumerable<string> tags) : base(tags)
        {
            MaxSpeed = maxSpeed;
        }

        protected BaseMoveAction(double maxSpeed, params string[] tags) : base(tags)
        {
            MaxSpeed = maxSpeed;
        }


        public double MaxSpeed { get; private set; }

        public override IEnumerable<Event> ProcessCommand(T command, Actor actor, World world)
        {
            var distance = EngineMath.ProcessDiff(GetDistance(command, actor), MaxSpeed);

            Point oldLocation = actor.Location;
            Point newLocation = oldLocation.Move(GetDirection(command, actor), distance);
            yield return new MoveEvent(actor.Id, oldLocation, newLocation);
        }

        protected abstract double GetDirection(T command, Actor actor);
        protected abstract double GetDistance(T command, Actor actor);

    }

    public class MoveAction : BaseMoveAction<MoveCommand, MoveCommand.CommandParameter>
    {
        public MoveAction(double maxSpeed, IEnumerable<string> tags) : base(maxSpeed, tags)
        {
        }

        public MoveAction(double maxSpeed, params string[] tags) : base(maxSpeed, tags)
        {
        }

        public override MoveCommand CreateCommand(string actorId, MoveCommand.CommandParameter parameter)
        {
            return new MoveCommand(actorId, this.ActionId, parameter);
        }

        public MoveCommand CreateCommand(string actorId, double direction, double distance)
        {
            return new MoveCommand(actorId, this.ActionId, new MoveCommand.CommandParameter(direction, distance));
        }

        protected override double GetDirection(MoveCommand command, Actor actor)
        {
            return command.Direction;
        }

        protected override double GetDistance(MoveCommand command, Actor actor)
        {
            return command.Distance;
        }
    }

    public class MoveForwardAction : BaseMoveAction<MoveForwardCommand, MoveForwardCommand.CommandParameter>
    {
        public MoveForwardAction(double maxSpeed, IEnumerable<string> tags) : base(maxSpeed, tags)
        {
        }

        public MoveForwardAction(double maxSpeed, params string[] tags) : base(maxSpeed, tags)
        {
        }

        public override MoveForwardCommand CreateCommand(string actorId, MoveForwardCommand.CommandParameter parameter)
        {
            return new MoveForwardCommand(actorId, this.ActionId, parameter);
        }


        public MoveForwardCommand CreateCommand(string actorId, double distance)
        {
            return new MoveForwardCommand(actorId, this.ActionId, new MoveForwardCommand.CommandParameter(distance));
        }

        protected override double GetDirection(MoveForwardCommand command, Actor actor)
        {
            return actor.Direction;
        }

        protected override double GetDistance(MoveForwardCommand command, Actor actor)
        {
            return command.Distance;
        }
    }
}