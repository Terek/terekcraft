﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine.actions
{
    public abstract class BaseAction : Taggable
    {
        private static int actionCounter = 1000;

        private readonly int actionId = GetNextActionId();

        public int ActionId { get{ return actionId; } }

        public int Cooldown { get; set; }

        public BaseAction(params string[] tags) : base(new HashSet<string>(tags))
        {

        }

        public BaseAction(IEnumerable<string> tags) : base(new HashSet<string>(tags))
        {

        }

        public IEnumerable<Event> ProcessCommand(Command command, Actor actor, World world)
        {
            actor.AddCooldown(command.ActionId, Cooldown + 1);

            return InternalProcessCommand(command, actor, world);
        }

        protected abstract IEnumerable<Event> InternalProcessCommand(Command command, Actor actor, World world);
        
        public abstract Type GetCommandType();



        private static int GetNextActionId()
        {
            return Interlocked.Increment(ref actionCounter);
        }
    }
}
