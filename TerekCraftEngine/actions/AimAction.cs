﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.commands;
using TerekCraftEngine.events;

namespace TerekCraftEngine.actions
{
    public class AimAction : TypedBaseAction<AimCommand, AimCommand.CommandParameter>
    {
        readonly double _maxTurnRate;

        public double MaxTurnRate { get { return _maxTurnRate; } }

        public AimAction(double maxTurnRate, params string[] tags) : base(new HashSet<string>(tags))
        {
            _maxTurnRate = maxTurnRate;
        }

        public AimAction(double maxTurnRate, IEnumerable<string> tags) : base(tags)
        {
            _maxTurnRate = maxTurnRate;
        }

        public override IEnumerable<Event> ProcessCommand(AimCommand command, Actor actor, World world)
        {
            double oldDirection = actor.Aim;
            double newAim = CalculateNewDirection(command.NewDirection, actor.Aim);
            yield return new AimEvent(actor.Id, oldDirection, newAim);
        }

        double CalculateNewDirection(double newDirection, double oldDirection)
        {
            double diff = EngineMath.NormalizeAngleDiff(newDirection - oldDirection);
            diff = EngineMath.ProcessDiff(diff, MaxTurnRate);

            newDirection = oldDirection + diff;
            newDirection = EngineMath.NormalizeAngle(newDirection);
            return newDirection;
        }

        public override AimCommand CreateCommand(string actorId, AimCommand.CommandParameter parameter)
        {
            return new AimCommand(actorId, this.ActionId, parameter);
        }

        public AimCommand CreateCommand(string actorId, double newDirection)
        {
            return new AimCommand(actorId, this.ActionId, new AimCommand.CommandParameter(newDirection));
        }
    }
}
