﻿using System;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.actions;

namespace TerekCraftEngine.scenarios
{
    public class OneOnOneScenario : IScenario
    {
        readonly int _teamSize;
    
        readonly string _team1;
        readonly string _team2;

        readonly ActorType _missileTankType;
        readonly ActorType _normalTankType;

        const int GenerationRate = 10;

        public OneOnOneScenario(int teamSize, string team1, string team2)
        {
            _teamSize = teamSize;
            _team1 = team1;
            _team2 = team2;

            _normalTankType = new ActorType("tank", 15, 1.5f, new List<BaseAction>() { new MoveForwardAction(1f), new TurnAction(0.5), new AimAction(1f), new ShootAimedAction(1, 30, "tank") { Cooldown = 10 } });

            var shootMissileAction = new ShootMissileAction(3f, 5f, 3, 20, "explode", "explode")
            {
                Cooldown = 30
            };

            _missileTankType = new ActorType("launcher", 5, 1.5f, new List<BaseAction>() { new MoveForwardAction(1f), new TurnAction(0.5), shootMissileAction }, new List<string> {"tank" } );
        }

        public void Init(World world)
        {
            InitTeam(world, _team1, -100, 0f);
            InitTeam(world, _team2, +20, Math.PI);
        }

        void InitTeam(World world, string team, float xOffset, double facing)
        {
            for (int i = 0; i < _teamSize * 3 / 4; i++)
            {
                CreateActor(_normalTankType, world, team, xOffset, facing);
            }
            for (int i = 0; i < _teamSize / 4; i++)
            {
                CreateActor(_missileTankType, world, team, xOffset, facing);
            }
        }

        void CreateActor(ActorType actorType, World world, string team, double xOffset, double facing)
        {
            var actor = new Actor(actorType, team);
            actor.Location = GetFreeLocation(world, xOffset);
            actor.Direction = facing;
            world.AddActor(actor);
        }

        Point GetFreeLocation(World world, double xOffset)
        {
            var point = new Point(world.Random.NextDouble() * 80 + xOffset, world.Random.NextDouble() * 200 - 100);
            while (world.SpatialDatabase.RetreiveInRange(point, 12).Any())
            {
                point = new Point(world.Random.NextDouble() * 80 + xOffset, world.Random.NextDouble() * 200 - 100);
            }

            return point;
        }

        public void Update(World world)
        {
            if (world.FrameNumber % GenerationRate == 0)
            {
                CreateActor(GetRandomActorType(world), world, _team1, -140, 0f);
                CreateActor(GetRandomActorType(world), world, _team2, +60, Math.PI);
            }
        }

        ActorType GetRandomActorType(World world)
        {
            if (world.Random.Next(4) < 3)
            {
                return _normalTankType;
            }

            return _missileTankType;
        }
    }
}
