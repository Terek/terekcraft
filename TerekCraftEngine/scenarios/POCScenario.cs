﻿using System;
using System.Collections.Generic;
using TerekCraftEngine.actions;

namespace TerekCraftEngine.scenarios
{
    public class PocScenario : IScenario
    {
        readonly int pocSize;

        Random random = new Random();

        public PocScenario(int pocSize)
        {
            this.pocSize = pocSize;
        }

        public void Init(World world)
        {
            var actorType = new ActorType("tank", 1000000, 0.01, new List<BaseAction>() { new MoveForwardAction(0f), new TurnAction(0.5), new AimAction(1f), new ShootAimedAction(1, 20) { Cooldown = 5 } });

            for (int i = 0; i < pocSize; i++)
            {
                Actor actor = new Actor( actorType );
                actor.Location = new Point(random.NextDouble() * 100 , random.NextDouble() * 100);
                actor.Direction = 0;

                world.AddActor(actor);
            }
        }

        public void Update(World world)
        {
        }
    }
}