﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;
using TerekCraftEngine.scenarios;
using TerekCraftEngine.strategies;

namespace TerekCraftEngine
{
    public class POC
    {
        readonly World _world = new World();
        readonly IStrategy _strategy;
        readonly PocScenario _scenario;

        public POC(int pocSize = 3000)
        {
            _scenario = new PocScenario(pocSize);
            _strategy = new SimpleStrategy();
        }

        public void RunPoc()
        {
            _scenario.Init(_world);
            _world.Tick();
//            PrintState();

            while (_world.Actors.Count() > 1)
            {
                DoTick();

                if (_world.FrameNumber > 100) return;
            }
        }

        void DoTick()
        {
            _world.AddCommand(_strategy.DoTick(_world));

            _world.Tick();

            // PrintState();
        }

        void PrintState()
        {
            Console.WriteLine("Frame - " + _world.FrameNumber);
            foreach (var actor in _world.Actors)
            {
                Console.WriteLine(actor);
            }
        }
    }
}
