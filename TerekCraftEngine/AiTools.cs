using System;
using System.Linq;

namespace TerekCraftEngine
{
    public class AiTools
    {
        public readonly World _world;

        public AiTools(World world)
        {
            _world = world;
        }

        public double GetNewDirection(Actor actor, Point nextMovement, Point target)
        {
            double direction = actor.Location.GetDirectionTo(target);

            var obstacles = _world.SpatialDatabase.RetreiveInRange(nextMovement, actor.Type.Size);

            obstacles.Remove(actor);

            if (obstacles.Any())
            {
                Actor obstacle = obstacles.First();

                double distanceToObstacleSquared = actor.Location.GetDistanceSquaredTo(obstacle.Location);
                double directionToObstacle = actor.Location.GetDirectionTo(obstacle.Location);
                double touchDistance = obstacle.Size + actor.Size;

                if (touchDistance * touchDistance > distanceToObstacleSquared)
                {
                    return directionToObstacle + Math.PI;
                }
                
                double angleToAvoidObstacle;
                angleToAvoidObstacle = Math.Asin(touchDistance / Math.Sqrt(distanceToObstacleSquared));                    


                double newDirection1 = directionToObstacle - angleToAvoidObstacle;
                double newDirection2 = directionToObstacle + angleToAvoidObstacle;

                if (GetSuggestionGoodness(actor, target, newDirection1) < GetSuggestionGoodness(actor, target, newDirection2))
                {
                    return newDirection1;
                }
                else
                {
                    return newDirection2;
                }

            }

            return direction;
        }

        static double GetSuggestionGoodness(Actor actor, Point target, double newDirection1)
        {
            Point suggestedNextPath1 = actor.Location.Move(newDirection1, 1f);
            return suggestedNextPath1.GetDistanceSquaredTo(target);
        }
    }
}