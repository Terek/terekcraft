﻿using System;
using System.Collections;
using TerekCraftEngine;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.events;
using TerekCraftEngine.actions;
using TerekCraftEngine.strategies;
using TerekCraftEngine.commands;
using System.Threading;

public abstract class Game
{
    readonly IScenario _scenario;
    readonly World _world = new World();

    public double TargetUpdatePerSeconds { get; set; }

    public double TargetFrameTime { get { return 1/TargetUpdatePerSeconds; } }

    double _acommulatedFrameTime = 0;

    protected Game(IScenario scenario)
    {
        this._scenario = scenario;
    }


    public void StartGame()
    {
        _scenario.Init(_world);
    }

    // Update is called once per frame
    public List<Event> UpdateGame(double lastFrameTime)
    {
        _acommulatedFrameTime += lastFrameTime;

        var events = new List<Event>();

        while (_acommulatedFrameTime >= TargetFrameTime)
        {
            _scenario.Update(_world);
            _world.Tick();

            events.AddRange(_world.GetTickEvents());

            _acommulatedFrameTime -= TargetFrameTime;
        }

        return events;
    }

}
