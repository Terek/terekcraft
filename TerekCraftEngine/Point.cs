﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerekCraftEngine
{
    public class Point
    {
        const double Epsilon = 1E-15;

        public readonly double X;
        public readonly double Y;

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            var otherPoint = obj as Point;
            if (otherPoint != null)
            {
                return Math.Abs(otherPoint.X - X) < Epsilon && Math.Abs(otherPoint.Y - Y) < Epsilon;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return 31 * X.GetHashCode() + Y.GetHashCode();
        }

        public override string ToString()
        {
            return "<point x=" + X.ToString("0.00") + ", y=" + Y.ToString("0.00") + "/>";
        }


        public Point Move(double direction, double distance)
        {
            double newX = X + Math.Cos(direction) * distance;
            double newY = Y + Math.Sin(direction) * distance;

            return new Point(newX, newY);
        }

        public double GetDirectionTo(Point pointTo)
        {
            double diffY = pointTo.Y - Y;

            if (EpsilonEquals(pointTo.X, X))
            {
                return Math.Sign(diffY) * Math.PI / 2;
            }

            double diffX = pointTo.X - X;
            if (EpsilonEquals(pointTo.Y, Y))
            {
                return diffX < 0 ? Math.PI : 0 ;

            }

            double offset = diffX < 0 ? Math.PI : 0;

            return Math.Atan(diffY / diffX) + offset;
        }

        static bool EpsilonEquals(double first, double second)
        {
            return Math.Abs(first - second) < Epsilon;
        }

        public double GetDistanceSquaredTo(Point pointTo)
        {
            double diffY = pointTo.Y - Y;
            double diffX = pointTo.X - X;
            return diffX*diffX + diffY*diffY;
        }

        public double Cross(Point other)
        {
            return X * other.Y - Y * other.X;
        }

        public Point GetNegative()
        {
            return new Point(-X, -Y);
        }

        public Point GetVectorTo(Point point)
        {
            return new Point( point.X - X, point.Y-Y);
        }

        public double Dot(Point point2)
        {
            return X * point2.X + Y * point2.Y;
        }

        public Point Times(double times)
        {
            return new Point(X * times, Y * times);
        }

        public Point Add(Point point2)
        {
            return new Point(X + point2.X, Y + point2.Y);
        }

        public double GetLengthSquared()
        {
            return X * X + Y * Y;
        }


    }
}
