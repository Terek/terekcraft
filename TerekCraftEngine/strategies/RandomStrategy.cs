﻿using System;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;

namespace TerekCraftEngine.strategies
{
    public class RandomStrategy : IStrategy
    {
        readonly Random _random = new Random();

        public IEnumerable<Command> DoTick(World world)
        {
            foreach (var actor in world.Actors)
            {
                AimAction aimAction = actor.Type.GetActionByType<AimAction>();
                TurnAction turnAction = actor.Type.GetActionByType<TurnAction>();
                ShootAimedAction shootAimedAction = actor.Type.GetActionByType<ShootAimedAction>();
                MoveForwardAction moveForwardAction = actor.Type.GetActionByType<MoveForwardAction>();


                yield return aimAction.CreateCommand(actor.Id, _random.NextDouble());
                yield return shootAimedAction.CreateCommand(actor.Id);
                yield return turnAction.CreateCommand(actor.Id, _random.NextDouble());
                yield return moveForwardAction.CreateCommand(actor.Id, 10);
            }
        }

        public string Name { get { return "RandomStrategy"; } }
    }
}