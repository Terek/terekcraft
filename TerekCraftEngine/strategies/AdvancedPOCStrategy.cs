﻿using System;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;

namespace TerekCraftEngine.strategies
{
    public class AdvancedPocStrategy : IStrategy
    {
        readonly string _team;
        readonly string _name;

        public AdvancedPocStrategy(string team, string name)
        {
            _team = team;
            _name = name;
        }

        public IEnumerable<Command> DoTick(World world)
        {
            foreach (var actor in world.Actors.Where(item => item.Team == _team))
            {
                TurnAction turnAction = actor.Type.GetActionByType<TurnAction>();
                ShootMissileAction shootMissileAction = actor.Type.GetActionByType<ShootMissileAction>();
                ShootAimedAction shootAimedAction = actor.Type.GetActionByType<ShootAimedAction>();
                AimAction aimAction = actor.Type.GetActionByType<AimAction>();
                MoveForwardAction moveForwardAction = actor.Type.GetActionByType<MoveForwardAction>();

                var victim = world.Actors.Where(item => item.Team != _team && item.Type.HasTag("tank"))
                    .OrderBy(item => actor.Location.GetDistanceSquaredTo(item.Location))
                    .FirstOrDefault();
                if (victim == null)
                {
                    yield return shootMissileAction.CreateCommand( actor.Id, 0 ) ;
                    continue;
                }

                double direction = actor.Location.GetDirectionTo(victim.Location);
                double distanceSquared = actor.Location.GetDistanceSquaredTo(victim.Location);


                var shootMissile = shootMissileAction;
                if (shootAimedAction != null)
                {
                    if (distanceSquared < EngineMath.sqr(shootAimedAction.Range))
                    {
                        yield return aimAction.CreateCommand(actor.Id, direction - actor.Direction);
                        if (world.Random.Next(4) == 0)
                        {
                            yield return shootAimedAction.CreateCommand(actor.Id);
                        }
                    }
                    if (distanceSquared > EngineMath.sqr(shootAimedAction.Range / 2f))
                    {
                        yield return turnAction.CreateCommand(actor.Id, direction);
                        yield return moveForwardAction.CreateCommand(actor.Id, moveForwardAction.MaxSpeed);
                    }
                }
                if (shootMissile != null)
                {
                    if (distanceSquared < EngineMath.sqr(shootMissile.Speed * shootMissile.TimeToLive))
                    {
                        yield return aimAction.CreateCommand(actor.Id, direction - actor.Direction);
                        if (world.Random.Next(4) == 0)
                        {
                            yield return shootMissileAction.CreateCommand(actor.Id, direction);
                        }
                    }
                    if (distanceSquared > EngineMath.sqr(shootMissile.Speed * shootMissile.TimeToLive))
                    {
                        yield return turnAction.CreateCommand(actor.Id, direction);
                        yield return moveForwardAction.CreateCommand(actor.Id, moveForwardAction.MaxSpeed);
                    }
                }
            }
        }

        public string Name { get { return _name; } }
    }
}

