﻿using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.actions;
using TerekCraftEngine.commands;

namespace TerekCraftEngine.strategies
{
    public class SimpleStrategy : IStrategy
    {

        public IEnumerable<Command> DoTick(World world)
        {
            foreach (var actor in world.Actors)
            {
                TurnAction turnAction = actor.Type.GetActionByType<TurnAction>();
                ShootAimedAction shootAimedAction = actor.Type.GetActionByType<ShootAimedAction>();
                AimAction aimAction = actor.Type.GetActionByType<AimAction>();
                MoveForwardAction moveForwardAction = actor.Type.GetActionByType<MoveForwardAction>();
            
                var victim = world.SpatialDatabase.FindClosest(actor.Location, item => item != actor);

                double direction = actor.Location.GetDirectionTo(victim.Location);
                double distanceSquared = actor.Location.GetDistanceSquaredTo(victim.Location);

                var range = ((ShootAimedAction)(actor.Type.GetActionByType<ShootAimedAction>())).Range;
                if (distanceSquared < range * range)
                {
                    yield return aimAction.CreateCommand(actor.Id, direction - actor.Direction);
                    yield return shootAimedAction.CreateCommand(actor.Id);
                }
                else
                {
                    yield return turnAction.CreateCommand(actor.Id, direction);
                    yield return moveForwardAction.CreateCommand(actor.Id, moveForwardAction.MaxSpeed);
                }
            }
        }

        public string Name { get { return "SimpleStrategy"; } }
    }
}