namespace TerekCraftEngine.commands
{
    public abstract class Command
    {

        private readonly string actorId;
        private readonly int actionId;
    
        public Command(string actorId, int actionId)
        {
            this.actorId = actorId;
            this.actionId = actionId;
        }

        public string ActorId { get { return actorId; } }
        public int ActionId { get { return actionId; } }

    }


    public class CommandParameter<T> where T : Command
    {

    }


}