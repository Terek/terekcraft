﻿namespace TerekCraftEngine.commands
{
    public class MoveForwardCommand : Command
    {
        public double Distance { get; private set; }

        public MoveForwardCommand(string actorId, int actionId, CommandParameter parameter) : base(actorId, actionId)
        {
            Distance = parameter.Distance;
        }

        public class CommandParameter : CommandParameter<MoveForwardCommand>
        {
            public double Distance { get; private set; }

            public CommandParameter(double distance)
            {
                Distance = distance;
            }
        }
    }



}