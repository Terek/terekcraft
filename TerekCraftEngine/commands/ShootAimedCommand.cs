namespace TerekCraftEngine.commands
{
    public class ShootAimedCommand : Command
    {
        public ShootAimedCommand(string actorId, int actionId) : base(actorId, actionId)
        {
        }

        public class CommandParameter : CommandParameter<ShootAimedCommand>  {  }

    }


}