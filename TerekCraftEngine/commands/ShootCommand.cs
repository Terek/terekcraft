namespace TerekCraftEngine.commands
{
    public class ShootCommand : Command
    {
        public double Direction { get; private set; }

        public ShootCommand(string actorId, int actionId, CommandParameter parameter) : base(actorId, actionId)
        {
            Direction = parameter.Direction;
        }

        public class CommandParameter : CommandParameter<ShootCommand>
        {
            public double Direction { get; private set; }

            public CommandParameter(double direction)
            {
                Direction = direction;
            }
        }

    }

}