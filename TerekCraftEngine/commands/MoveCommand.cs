namespace TerekCraftEngine.commands
{
    public class MoveCommand : Command
    {
        public double Distance { get; private set; }
        public double Direction { get; private set; }

        public MoveCommand(string actorId, int actionId, CommandParameter parameter) : base(actorId, actionId)
        {
            Distance = parameter.Distance;
            Direction = parameter.Direction;
        }

        public class CommandParameter : CommandParameter<MoveCommand>
        {
            public double Distance { get; private set; }
            public double Direction { get; private set; }

            public CommandParameter(double direction, double distance)
            {
                Distance = distance;
                Direction = direction;
            }
        }

    }


}