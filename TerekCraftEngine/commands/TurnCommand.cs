namespace TerekCraftEngine.commands
{
    public class TurnCommand : Command
    {
        public double NewDirection { get; private set; }

        public TurnCommand(string actorId, int actionId, CommandParameter parameter) : base(actorId, actionId)
        {
            NewDirection = parameter.NewDirection;
        }


        public class CommandParameter : CommandParameter<TurnCommand>
        {
            public double NewDirection { get; private set; }

            public CommandParameter(double newDirection)
            {
                NewDirection = newDirection;
            }
        }
    }
}