namespace TerekCraftEngine.commands
{
    public class ShootMissileCommand : Command
    {
        public double Direction { get; private set; }

        public ShootMissileCommand(string actorId, int actionId, CommandParameter parameter) : base(actorId, actionId)
        {
            Direction = parameter.Direction;
        }

        public class CommandParameter : CommandParameter<ShootMissileCommand>
        {
            public double Direction { get; private set; }

            public CommandParameter(double direction)
            {
                Direction = direction;
            }
        }

    }
}