namespace TerekCraftEngine.commands
{
    public class ShootForwardCommand : Command
    {
        public ShootForwardCommand(string actorId, int actionId) : base(actorId, actionId)
        {
        }

        public class CommandParameter : CommandParameter<ShootForwardCommand> { }

    }
}