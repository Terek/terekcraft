namespace TerekCraftEngine.commands
{
    public class AimCommand : Command
    {
        public double NewDirection { get; private set; }

        public AimCommand(string actorId, int actionId, CommandParameter parameter) : base(actorId, actionId)
        {
            NewDirection = parameter.NewDirection;
        }


        public class CommandParameter : CommandParameter<AimCommand>
        {
            public double NewDirection { get; private set; }

            public CommandParameter(double newDirection)
            {
                NewDirection = newDirection;
            }
        }
    }
}