﻿namespace TerekCraftEngine.events
{
    public class MoveEvent : Event
    {
        readonly Point _oldLocation;
        readonly Point _newLocation;

        public MoveEvent(string actorId, Point oldLocation, Point newLocation)
            : base(actorId)
        {
            _oldLocation = oldLocation;
            _newLocation = newLocation;
        }

        public Point NewLocation { get { return _newLocation; } }
        public Point OldLocation { get { return _oldLocation; } }

        public override string ToString()
        {
            return base.ToString() + " - oldlocation:" + _oldLocation + " - newLocation:" + _newLocation;
        }
    }
}