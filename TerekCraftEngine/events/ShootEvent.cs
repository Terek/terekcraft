﻿namespace TerekCraftEngine.events
{
    public class ShootEvent : Event
    {
        public ShootEvent(string actorId)
            : base(actorId)
        {
        }

        public Point FromLocation { get; set; }
        public Point ToLocation { get; set; }
    }
}