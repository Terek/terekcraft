﻿using System.Collections.Generic;

namespace TerekCraftEngine.events
{
    public class SpawnEvent : Event
    {
        readonly Actor _actor;


        public SpawnEvent(Actor actor, params string[] tags) : base(actor.Id, new HashSet<string>(tags))
        {
            _actor = actor;
        }

        public SpawnEvent(Actor actor, IEnumerable<string> tags) : base(actor.Id, new HashSet<string>(tags))
        {
            _actor = actor;
        }

        public Actor Actor { get { return _actor; } }

        public override string ToString()
        {
            return this.GetType().Name + " - " + _actor.Id;
        }
    }
}