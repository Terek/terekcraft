﻿using System.Collections.Generic;

namespace TerekCraftEngine.events
{
    public class DespawnEvent : Event
    {
        readonly Point _location;

        public DespawnEvent(string actorId, Point location, params string[] tags) : base(actorId, tags)
        {
            _location = location;
        }

        public DespawnEvent(string actorId, Point location, IEnumerable<string> tags) : base(actorId, tags)
        {
            _location = location;

        }

        public Point Location { get { return _location; } }
    }
}