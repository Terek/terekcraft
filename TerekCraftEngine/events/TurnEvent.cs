﻿namespace TerekCraftEngine.events
{
    public class TurnEvent : Event
    {
        readonly double _oldDirection;
        readonly double _newDirection;

        public TurnEvent(string actorId, double oldDirection, double newDirection) : base(actorId)
        {
            _oldDirection = oldDirection;
            _newDirection = newDirection;
        }

        public double NewDirection { get { return _newDirection; } }
        public double OldDirection { get { return _oldDirection; } }

        public override string ToString()
        {
            return base.ToString() + " - _oldDirection:" + _oldDirection + " - _newDirection:" + _newDirection;
        }
    }
}