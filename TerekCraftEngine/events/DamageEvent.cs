﻿using System.Collections.Generic;

namespace TerekCraftEngine.events
{
    public class DamageEvent : Event
    {
        public DamageEvent(string actorId, params string[] tags)  : base(actorId, tags)
        {
        }

        public DamageEvent(string actorId, IEnumerable<string> tags) : base(actorId, tags)
        {
        }

        public string Target { get; set; }
        public int Damage { get; set; }

        public override string ToString()
        {
            return base.ToString() + " - Damage:" + Damage;
        }
    }
}