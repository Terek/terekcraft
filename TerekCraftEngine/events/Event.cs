﻿using System.Collections.Generic;

namespace TerekCraftEngine.events
{
    public class Event : Taggable
    {

        readonly string _actorId;

        public Event(string actorId, params string[] tags) : base(new HashSet<string>(tags))
        {
            _actorId = actorId;

        }

        public Event(string actorId, IEnumerable<string> tags) : base(new HashSet<string>(tags))
        {
            _actorId = actorId;
        }

        public string ActorId { get { return _actorId; } }

        public override string ToString()
        {
            return this.GetType().Name + " - " + _actorId;
        }




    }
}