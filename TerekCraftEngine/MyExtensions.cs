﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.functional;

namespace TerekCraftEngine
{
    static class MyExtensions
    {
        public static Option<V> Find<T, V>(this Dictionary<T, V> dict, T key)
        {
            if (dict.ContainsKey(key))
            {
                return Option<V>.Create(dict[key]);
            }
            else
            {
                return Option<V>.CreateEmpty();
            }
        }


        public static V GetOrNull<T, V>(this Dictionary<T, V> dict, T key)
        {
            dict.TryGetValue(key, out var value);
            return value;
        }

        public static V GetOrDefault<T, V>(this Dictionary<T, V> dict, T key, V alternateValue)
        {
            return dict.TryGetValue(key, out var value) ? value : alternateValue;           
        }


        public static E GetOrEmpty<T, V, E>(this Dictionary<T, E> dict, T key) where E : IEnumerable<V>
        {
            E defaultValue = (E)Enumerable.Empty<V>();

            return dict.GetOrDefault<T, E>(key, defaultValue);
        }


        public static IEnumerable<T> ToEnumerable<T>(params T[] items)
        {
            return items;
        }



    }
}
