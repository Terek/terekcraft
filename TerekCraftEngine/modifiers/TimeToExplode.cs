﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.modifiers
{
    public class TimeToExplode : BaseModifier
    {
        int _timeToLive;
        readonly int _damage;
        readonly double _radius;

        public double Damage { get { return _damage; } }
        public double Radius { get { return _radius; } }

        public TimeToExplode(int startTimeToLive, int damage, double radius, params string[] tags) 
            : this(startTimeToLive, damage, radius, new HashSet<string>(tags))
        {

        }


        public TimeToExplode(int startTimeToLive, int damage, double radius, IEnumerable<string> tags) : base(tags)
        {
            _timeToLive = startTimeToLive;
            _damage = damage;
            _radius = radius;
        }

        public override IEnumerable<Event> Tick(Actor actor, World world)
        {
            if (--_timeToLive <= 0)
            {
                yield return new DespawnEvent(actor.Id, actor.Location, Tags);

                foreach (var victim in world.SpatialDatabase.RetreiveInRange(actor.Location, _radius))
                {
                    if (victim.Id != actor.Id)
                    {
                        yield return
                            new DamageEvent(actor.Id, Tags)
                            {
                                Damage = _damage,
                                Target = victim.Id
                            };
                    }
                }

            }
        }
    }
}
