﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.modifiers
{
    public class TimeToLive : BaseModifier
    {
        int _timeToLive;

        public TimeToLive(int startTimeToLive, params string[] tags) : base(tags)
        {
            _timeToLive = startTimeToLive;
        }

        public TimeToLive(int startTimeToLive, IEnumerable<string> tags) : base(tags)
        {
            _timeToLive = startTimeToLive;
        }

        public override IEnumerable<Event> Tick(Actor actor, World world)
        {
            if (--_timeToLive <= 0)
            {
                yield return new DespawnEvent(actor.Id, actor.Location, Tags);
            }
        }
    }
}
