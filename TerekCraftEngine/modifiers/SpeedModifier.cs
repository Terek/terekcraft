﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.modifiers
{
    public class SpeedModifier : BaseModifier
    {
        private readonly double speed;


        public SpeedModifier(double speed, params string[] tags) : base(tags)
        {
            this.speed = speed;
        }

        public override IEnumerable<Event> Tick(Actor actor, World world)
        {
            if (speed != 0)
            {
                Point newLocation = actor.Location.Move(actor.Direction, speed);

                yield return new MoveEvent(actor.Id, actor.Location, newLocation);
            }
        }


    }
}
