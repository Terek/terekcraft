﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.modifiers
{
    public abstract class BaseModifier : Taggable
    {
        public BaseModifier(params string[] tags) : base(new HashSet<string>(tags))
        {

        }

        public BaseModifier(IEnumerable<string> tags) : base(new HashSet<string>(tags))
        {

        }


        abstract public IEnumerable<Event> Tick(Actor actor, World world);
    }
}
