﻿using System.Collections.Generic;
using TerekCraftEngine.commands;

namespace TerekCraftEngine
{
    public interface IStrategy
    {
        IEnumerable<Command> DoTick(World world);
        string Name { get; }

    }
}