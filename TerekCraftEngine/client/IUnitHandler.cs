﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.events;

namespace TerekCraftEngine.client
{
    public interface IUnitHandler
    {
        void HandleEvent(MoveEvent moveEvent, float lerp);
        void HandleEvent(AimEvent aimEvent, float lerp);
        void HandleEvent(TurnEvent turnEvent, float lerp);
        void HandleEvent(DamageEvent damageEvent);
        void HandleEvent(DespawnEvent despawnEvent);
        void HandleEvent(ShootEvent despawnEvent);

        bool Select();
        bool Deselect();
    }
}
