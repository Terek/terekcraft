﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TerekCraftEngine.spatial
{
    public class QuadTree<T> : ISpatialDataBase<T> where T : ISpatialData
    {
        readonly List<T> _store = new List<T>();

        readonly List<T> _allItems = new List<T>(); 

        readonly List<QuadTree<T>> _subsections = new List<QuadTree<T>>(); 

        readonly int _maxCapacity;
        readonly double _minX;
        readonly double _minY;
        readonly double _maxX;
        readonly double _maxY;

        public QuadTree(int maxCapacity, double minX, double minY, double maxX, double maxY)
        {
            _maxCapacity = maxCapacity;
            _minX = minX;
            _minY = minY;
            _maxX = maxX;
            _maxY = maxY;
        }

        public ICollection<T> ListAll()
        {
            return _allItems;
        }

        public void Add(T item)
        {
            _allItems.Add(item);

            if (IsSplit())
            {
                InsertIntoSubRegions(item);
            }
            else
            {
                _store.Add(item);

                if (_store.Count > _maxCapacity)
                {
                    SplitIntoSubregions();
                }
            }
        }

        bool IsSplit()
        {
            return this._subsections.Count > 0;
        }

        void SplitIntoSubregions()
        {
            double xSplit = (_maxX + _minX)/2;
            double ySplit = (_maxY + _minY)/2;

            this._subsections.Add(new QuadTree<T>(_maxCapacity, _minX, ySplit, xSplit, _maxY));
            this._subsections.Add(new QuadTree<T>(_maxCapacity, xSplit, ySplit, _maxX, _maxY));
            this._subsections.Add(new QuadTree<T>(_maxCapacity, _minX, _minY, xSplit, ySplit));
            this._subsections.Add(new QuadTree<T>(_maxCapacity, xSplit, _minY, _maxX, ySplit));

            foreach (var item in _store)
            {
                InsertIntoSubRegions(item);
            }

            _store.Clear();
        }

        void InsertIntoSubRegions(T item)
        {
            foreach (var subsection in _subsections)
            {
                if (subsection.Contains(item))
                {
                    subsection.Add(item);
                }
            }
        }

        public bool Contains(T item)
        {
            return Contains(item.Location, item.Size);
        }

        public bool Contains(Point location, double range)
        {
            if (location.X + range < this._minX)
            {
                return false;
            }
            if (location.X - range > this._maxX)
            {
                return false;
            }
            if (location.Y + range < this._minY)
            {
                return false;
            }
            if (location.Y - range > this._maxY)
            {
                return false;
            }

            return true;
        }


        public ICollection<T> RetreiveInRange(Point location, double range, Func<T, bool> predicate)
        {
            if (IsSplit())
            {
                List<T> foundItems = new List<T>();

                foreach (var subsection in _subsections)
                {
                    if (subsection.Contains(location, range))
                    {
                        foundItems.AddRange(subsection.RetreiveInRange(location, range, predicate));
                    }
                }

                return new HashSet<T>(foundItems);
            }
            else
            {
                return ListByPredicate(predicate).Where(data => data.Location.GetDistanceSquaredTo(location) <= EngineMath.sqr(data.Size + range)).ToList();                
            }
        }

        IEnumerable<T> ListByPredicate(Func<T, bool> predicate)
        {
            return ListByPredicate(_store, predicate);
        }

        IEnumerable<T> ListByPredicate(List<T> items, Func<T, bool> predicate)
        {
            IEnumerable<T> filtered;
            if (predicate != null)
            {
                filtered = items.Where(predicate);
            }
            else
            {
                filtered = items;
            }
            return filtered;
        }


        public T FindClosest(Point location, Func<T, bool> predicate)
        {
            if (IsSplit())
            {
                T closestInSameRegion = FindClosestInSameOrParentRegionNonRecursive(this, location, predicate);

                ICollection<T> possibleCandidates = RetreiveInRange(location, Math.Sqrt(closestInSameRegion.Location.GetDistanceSquaredTo(location)), predicate);

                return possibleCandidates.OrderBy(item => item.Location.GetDistanceSquaredTo(location)).FirstOrDefault();
            }

            return
                ListByPredicate(_allItems, predicate).OrderBy(item => item.Location.GetDistanceSquaredTo(location)).FirstOrDefault();
        }

        T FindClosestInSameOrParentRegion(QuadTree<T> quadTree, Point location, Func<T, bool> predicate)
        {
            T foundItem = default(T);

            if (quadTree.IsSplit())
            {
                foreach (var subsection in quadTree._subsections)
                {
                    if (subsection.Contains(location, 0))
                    {
                        foundItem = FindClosestInSameOrParentRegion(subsection, location, predicate);
                    }
                }
            }

            if (foundItem != null)
            {
                return foundItem;
            }

            return ListByPredicate(quadTree._allItems, predicate).OrderBy(item => item.Location.GetDistanceSquaredTo(location)).FirstOrDefault();
        
        }

        T FindClosestInSameOrParentRegionNonRecursive(QuadTree<T> quadTree, Point location, Func<T, bool> predicate)
        {
            Stack<QuadTree<T>> stack = new Stack<QuadTree<T>>();
            QuadTree<T> current = quadTree;
            
            stack.Push(quadTree);

            while (current.IsSplit())
            {
                foreach (var subsection in current._subsections)
                {
                    if (subsection.Contains(location, 0))
                    {
                        stack.Push(subsection);
                        current = subsection;
                        break;
                    }
                }
            }

            foreach (var subsection in stack)
            {
                T found = ListByPredicate(subsection._allItems, predicate).OrderBy(item => item.Location.GetDistanceSquaredTo(location)).FirstOrDefault();
                if (found != null)
                {
                    return found;
                }
            }

            return default(T);
        }

    }
}
