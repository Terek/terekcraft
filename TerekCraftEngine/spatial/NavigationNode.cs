﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine.spatial
{
    public class NavigationNode<T> where T : Polygon
    {
        readonly int _id;

        readonly T _navigable;
        readonly ICollection<NavigationNode<T>> _neighbors;

        public ICollection<NavigationNode<T>> Neighbors { get { return _neighbors; } }

        public NavigationNode(int id, T navigable)
        {
            _id = id;
            _navigable = navigable;
            _neighbors = new List<NavigationNode<T>>();
        }

        public T Navigable { get { return _navigable; } }


        public bool AreNeighbors(NavigationNode<T> other)
        {
            return this.Neighbors.Contains(other);
        }

        public bool AreNeighbors(T other)
        {
            return this.Neighbors.Any(item => item.Navigable.Equals(other));
        }

        public int Id
        {
            get { return _id; }
        }


    }
}
