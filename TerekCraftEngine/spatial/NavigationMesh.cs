﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine.spatial
{
    public class NavigationMesh<T> where T : Polygon
    {
        public Dictionary<T, NavigationNode<T>> NodeMap; 

        public NavigationMesh(ICollection<T> polygons)
        {
            NodeMap = new Dictionary<T, NavigationNode<T>>();

            foreach (var polygon in polygons)
            {
                NodeMap.Add(polygon, new NavigationNode<T>(NodeMap.Count, polygon));
            }

            foreach(var navNode1 in NodeMap.Values)
            {
                foreach (var navNode2 in NodeMap.Values.Where(item => AreNeighbors(navNode1, item)))
                {
                    navNode1.Neighbors.Add(navNode2);
                }
            }

        }


        static bool AreNeighbors(NavigationNode<T> navNode1, NavigationNode<T> navNode2)
        {
            return navNode1.Navigable.HasCommonEdge(navNode2.Navigable) && navNode1.Navigable != navNode2.Navigable;
        }

        public List<Edge> GetPath(T poly1, T poly2)
        {
            return GetPath(NodeMap[poly1], NodeMap[poly2]);
        }


        public bool HasPath(T poly1, T poly2)
        {
            return HasPath(NodeMap[poly1], NodeMap[poly2]);
        }



        public List<Edge> GetPath(NavigationNode<T> source, NavigationNode<T> target)
        {
            var list = new List<Edge>();

            Dictionary<NavigationNode<T>, PathNode> pathNodes = CalculateNavigationFrom(source);

            var current = pathNodes[target];
            var previous = current.Previous;

            while(previous != null)
            {
                list.Add(previous.Node.Navigable.CommonEdge(current.Node.Navigable));

                current = previous;
                previous = current.Previous;
            }

            list.Reverse();

            return list;
        }

        public bool HasPath(NavigationNode<T> source, NavigationNode<T> target)
        {
            Dictionary<NavigationNode<T>, PathNode> pathNodes = CalculateNavigationFrom(source);

            return pathNodes[target].Previous != null;
        }

        Dictionary<NavigationNode<T>, PathNode> CalculateNavigationFrom(NavigationNode<T> source)
        {
            Dictionary<NavigationNode<T>, PathNode> pathNodes = new Dictionary<NavigationNode<T>, PathNode>();
            SortedDictionary<PathNode, PathNode> priorityQueue = new SortedDictionary<PathNode, PathNode>();

            foreach (var navNode in this.NodeMap.Values)
            {
                var pathNode = new PathNode(navNode);
                if (navNode == source)
                {
                    pathNode.Distance = 0;
                }
                pathNodes.Add(navNode, pathNode);
                priorityQueue.Add(pathNode, pathNode);
            }

            while (priorityQueue.Count > 0)
            {
                var node = priorityQueue.First().Key;
                priorityQueue.Remove(node);

                if (node.Distance == Int32.MaxValue)
                {
                    // no path to here
                    continue;
                }

                foreach (var neighbor in node.Node.Neighbors)
                {
                    var newDistance = node.Distance + 1;

                    var neighborPathNode = pathNodes[neighbor];

                    if (neighborPathNode.Distance > newDistance)
                    {
                        priorityQueue.Remove(neighborPathNode);

                        neighborPathNode.Distance = newDistance;
                        neighborPathNode.Previous = node;

                        priorityQueue.Add(neighborPathNode, neighborPathNode);
                    }
                }
            }

            return pathNodes;
        }

        class PathNode : IComparable<PathNode>
        {
            int distance = Int32.MaxValue;
            readonly NavigationNode<T> node;
            PathNode previous = null;

            public PathNode(NavigationNode<T> node)
            {
                this.node = node;
            }

            public NavigationNode<T> Node { get { return node; } }

            public PathNode Previous
            {
                get { return previous; }
                set { this.previous = value; }
            }

            public int Distance
            {
                get { return this.distance; }
                set { this.distance = value; }
            }

            public int CompareTo(PathNode other)
            {
                var distanceDiff = distance.CompareTo(other.distance);

                if(distanceDiff != 0)
                {
                    return distanceDiff;
                }
                else
                {
                    return Node.Id.CompareTo(other.Node.Id);
                }
            }
        }

    }
}
