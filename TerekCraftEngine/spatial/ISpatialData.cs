﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerekCraftEngine.spatial
{
    public interface ISpatialData
    {
        Point Location { get; }

        double Size { get; }
    }
}
