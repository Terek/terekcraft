﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerekCraftEngine.spatial
{
    public interface ISpatialDataBase<T> where T : ISpatialData
    {
        ICollection<T> ListAll(); 
        void Add(T item);
        ICollection<T> RetreiveInRange(Point location, double range, Func<T, bool> predicate);
        T FindClosest(Point location, Func<T, bool> predicate);
    }
}
