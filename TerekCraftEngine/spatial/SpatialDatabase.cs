﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerekCraftEngine.spatial
{
    public class SpatialDatabase<T> : ISpatialDataBase<T> where T : ISpatialData
    {
        readonly ISpatialDataBase<T> _implementation;

        public SpatialDatabase(ISpatialDataBase<T> implementation)
        {
            _implementation = implementation;
        }

        public ICollection<T> ListAll()
        {
            return _implementation.ListAll();
        }

        public void Add(T item)
        {
            _implementation.Add(item);
        }

        public ICollection<T> RetreiveInRange(Point location, double range, Func<T, bool> predicate)
        {
            return _implementation.RetreiveInRange(location, range, predicate);
        }

        public ICollection<T> RetreiveInRange(Point location, double range)
        {
            return _implementation.RetreiveInRange(location, range, null);
        }

        public ICollection<T> RetreiveAt(Point location, Func<T, bool> predicate)
        {
            return _implementation.RetreiveInRange(location, 0, predicate);
        }

        public ICollection<T> RetreiveAt(Point location)
        {
            return _implementation.RetreiveInRange(location, 0, null);
        }

        public T FindClosest(Point location, Func<T, bool> predicate)
        {
            return _implementation.FindClosest(location, predicate);
        }

        public T FindClosest(Point location)
        {
            return _implementation.FindClosest(location, null);
        }

    }
}
