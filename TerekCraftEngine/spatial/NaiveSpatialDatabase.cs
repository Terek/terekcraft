﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TerekCraftEngine.spatial
{
    public class NaiveSpatialDatabase<T> : ISpatialDataBase<T> where T : ISpatialData
    {
        readonly List<T> _store = new List<T>();

        public ICollection<T> ListAll()
        {
            return _store;
        }

        public void Add(T item)
        {
            _store.Add(item);
        }

        public ICollection<T> RetreiveInRange(Point location, double range, Func<T, bool> predicate)
        {
            return ListByPredicate(predicate).Where(data => data.Location.GetDistanceSquaredTo(location) <= (data.Size + range) * (data.Size + range)).ToList();
        }

        IEnumerable<T> ListByPredicate(Func<T, bool> predicate)
        {
            IEnumerable<T> filtered;
            if (predicate != null)
            {
                filtered = _store.Where(predicate);
            }
            else
            {
                filtered = _store;
            }
            return filtered;
        }

        public T FindClosest(Point location, Func<T, bool> predicate)
        {
            return ListByPredicate(predicate).OrderBy(item => item.Location.GetDistanceSquaredTo(location)).FirstOrDefault();
        }
    }
}
