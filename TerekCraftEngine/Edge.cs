﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TerekCraftEngine.terrain;

namespace TerekCraftEngine
{

    public enum Intersection
    {
        None,
        IntersectSegment,
        IntersectLine
    }



    public class Edge
    {
        public readonly Point EndPoint;
        public readonly Point StartPoint;

        public Edge(Point startPoint, Point endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        public double GetLengthSquared()
        {
            return StartPoint.GetDistanceSquaredTo(EndPoint);
        }

        public double GetSide(Point point)
        {
            double side = ((EndPoint.X - StartPoint.X)*(point.Y - StartPoint.Y) -
                           (EndPoint.Y - StartPoint.Y)*(point.X - StartPoint.X));
            return side;
        }

        public bool IsLeft(Point point)
        {
            return GetSide(point) > EngineMath.EPSILON;
        }

        public bool IsRight(Point point)
        {
            return GetSide(point) < - EngineMath.EPSILON;
        }

        protected bool Equals(Edge other)
        {
            return Equals(EndPoint, other.EndPoint) && Equals(StartPoint, other.StartPoint);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Edge) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((EndPoint != null ? EndPoint.GetHashCode() : 0)*397) ^
                       (StartPoint != null ? StartPoint.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return "<edge><startPoint>" + StartPoint + "</startPoint><endPoint>" + EndPoint + "</endPoint></edge>";
        }

        public bool IsParallel(Edge edge2)
        {
            Point vector = StartPoint.GetVectorTo(EndPoint);
            Point vector2 = edge2.StartPoint.GetVectorTo(edge2.EndPoint);

            var v = vector.Cross(vector2);
            return EngineMath.Equals0(v);
        }

        public bool IsColinear(Edge edge2)
        {
            Point vector = StartPoint.GetVectorTo(EndPoint);
            Point vector2 = edge2.StartPoint.GetVectorTo(edge2.EndPoint);

            Point startPointVector = StartPoint.GetVectorTo(edge2.StartPoint);

            return EngineMath.Equals0(vector.Cross(vector2)) && EngineMath.Equals0(vector.Cross(startPointVector));
        }

        public bool IsPointOnEdge(Point point)
        {
            if (EndPoint.Y == StartPoint.Y)
            {
                return point.Y == StartPoint.Y && Between(point.X, StartPoint.X, EndPoint.X);
            }
            else if (EndPoint.X == StartPoint.X)
            {
                return point.X == StartPoint.X && Between(point.Y, StartPoint.Y, EndPoint.Y);
            }


            double xCheck = (point.X - StartPoint.X) / (EndPoint.X - StartPoint.X);
            double yCheck = (point.Y - StartPoint.Y) / (EndPoint.Y - StartPoint.Y);

            return xCheck == yCheck && Between(point.X, StartPoint.X, EndPoint.X);
        }

        bool Between(double value, double limit1, double limit2)
        {
            if (limit1 < limit2)
            {
                return value >= limit1 - EngineMath.EPSILON && value <= limit2 + EngineMath.EPSILON;
            }
            return value <= limit1 + EngineMath.EPSILON && value >= limit2 - EngineMath.EPSILON;

        }

        public bool IsIntersecting(Edge edge)
        {
            // p
            Point startPoint1 = StartPoint;
            
            // q
            Point startPoint2 = edge.StartPoint;

            // r
            Point edgeVector1 = StartPoint.GetVectorTo(EndPoint);

            // s
            Point edgeVector2 = edge.StartPoint.GetVectorTo(edge.EndPoint);


            // r x s == 0, then parallel 
            if (edgeVector1.Cross(edgeVector2) == 0)
            {
                // (q − p) × r = 0, then the two lines are collinear
                if (startPoint1.GetVectorTo(startPoint2).Cross(edgeVector1) == 0)
                {
                    // (q − p) · r
                    double dot1 = startPoint1.GetVectorTo(startPoint2).Dot(edgeVector1);

                    // (p − q) · s
                    double dot2 = startPoint2.GetVectorTo(startPoint1).Dot(edgeVector2);

                    if (0 <= dot1 && dot1 <= edgeVector1.Dot(edgeVector1))
                    {
                        return true;
                    }

                    if (0 <= dot2 && dot2 <= edgeVector2.Dot(edgeVector2))
                    {
                        return true;
                    }

                    return false;
                }

                return false;
            }


            //          t = (q − p) × s / (r × s)
            double edgeVectorMultiplier1 = startPoint1.GetVectorTo(startPoint2).Cross(edgeVector2) / edgeVector1.Cross(edgeVector2);

            //          u = (q − p) × r / (r × s)
            double edgeVectorMultiplier2 = startPoint1.GetVectorTo(startPoint2).Cross(edgeVector1) / edgeVector1.Cross(edgeVector2);

            if (Between(edgeVectorMultiplier2, 0, 1) && Between(edgeVectorMultiplier1, 0, 1))
            {
                return true;                
            }

            return false;
        }

        public bool HasCommonEndpoints(Edge otherEdge)
        {
            return StartPoint.Equals(otherEdge.StartPoint)
                   || StartPoint.Equals(otherEdge.EndPoint)
                   || EndPoint.Equals(otherEdge.StartPoint)
                   || EndPoint.Equals(otherEdge.EndPoint);
        }

        public Edge Reverse()
        {
            return new Edge(EndPoint, StartPoint);
        }

        public Point GetSlicePoint(Edge otherEdge)
        {
            if (IsParallel(otherEdge))
            {
                return null;
            }

            Point startPoint1 = StartPoint;
            Point edgeVector1 = StartPoint.GetVectorTo(EndPoint);

            Point startPoint2 = otherEdge.StartPoint;
            Point edgeVector2 = otherEdge.StartPoint.GetVectorTo(otherEdge.EndPoint);

            //          t = (q − p) × s / (r × s)
            double edgeVectorMultiplier1 = startPoint1.GetVectorTo(startPoint2).Cross(edgeVector2) / edgeVector1.Cross(edgeVector2);

            //          u = (q − p) × r / (r × s)
            double edgeVectorMultiplier2 = startPoint1.GetVectorTo(startPoint2).Cross(edgeVector1) / edgeVector1.Cross(edgeVector2);

            if (Between(edgeVectorMultiplier2, 0, 1) )
            {
                return edgeVector1.Times(edgeVectorMultiplier1).Add(startPoint1);
            }

            return null;
        }


        public Edge GetLeftSlice(Edge edge)
        {
            return GetSlice(edge, IsLeft, IsRight);
        }

        public Edge GetRightSlice(Edge edge)
        {
            return GetSlice(edge, IsRight, IsLeft);
        }

        Edge GetSlice(Edge edge, Func<Point, bool> isOnSide, Func<Point, bool> isOnOtherSide)
        {

            if (isOnSide(edge.StartPoint) && isOnSide(edge.EndPoint)) return edge;
            if (isOnOtherSide(edge.StartPoint) && isOnOtherSide(edge.EndPoint)) return null;

            Point slicePoint = GetSlicePoint(edge);

            if (slicePoint != null)
            {
                if (!isOnOtherSide(edge.StartPoint) && slicePoint.GetDistanceSquaredTo(edge.StartPoint) > EngineMath.EPSILON)
                {
                    var newEdge = new Edge(edge.StartPoint, slicePoint);
                    return newEdge;
                }

                if (!isOnOtherSide(edge.EndPoint) && slicePoint.GetDistanceSquaredTo(edge.EndPoint) > EngineMath.EPSILON)
                {
                    var newEdge = new Edge(slicePoint, edge.EndPoint);
                        return newEdge;
                }
            }

            return null;
        }

        public ICollection<Edge> GetLeftSlices(ICollection<Edge> inputEdges)
        {
            return GetSlices(inputEdges, IsLeft, IsRight);
        }

        public ICollection<Edge> GetRightSlices(ICollection<Edge> inputEdges)
        {
            return GetSlices(inputEdges, IsRight, IsLeft);
        }

        public ICollection<Edge> GetSlices(ICollection<Edge> inputEdges, Func<Point, bool> isOnSide, Func<Point, bool> isOnOtherSide)
        {
            var edges = new List<Edge>();
            foreach (var edge in inputEdges)
            {
                var newEdge = GetSlice(edge, isOnSide, isOnOtherSide);
                if (newEdge != null) edges.Add(newEdge);
            }

            return edges;
        }

        public bool IsPolyOnLeft(Polygon polygon)
        {
            return polygon.Points.Any(IsLeft);
        }

        public bool IsPolyOnRight(Polygon polygon)
        {
            return polygon.Points.Any(IsRight);
        }


        public Intersection IsIntersecting(Point center, double radius)
        {
            double radiusSquared = radius * radius;

            double acx = center.X - StartPoint.X;
            double acy = center.Y - StartPoint.Y;

            double bcx = center.X - EndPoint.X;
            double bcy = center.Y - EndPoint.Y;

            if(acx * acx + acy * acy < radiusSquared || bcx * bcx + bcy * bcy < radiusSquared)
            {
                return Intersection.IntersectSegment;
            }

            double abx = EndPoint.X - StartPoint.X;
            double aby = EndPoint.Y - StartPoint.Y;

            // ac x ab
            double areaTimes2 = acx * aby - acy * abx;

            double areaTimes2Squared = areaTimes2 * areaTimes2;
            double expectedAreaTimes2Squared = radiusSquared * (abx * abx + aby * aby);

            // height of abc triangle at point c is greater than the radius.
            if(areaTimes2Squared >= expectedAreaTimes2Squared)
            {
                return Intersection.None;
            }

            // bap angle is greater than 90
            if(acx * abx + acy * aby  < 0)  // ac.Dot(ab) < 0) //  
            {
                return Intersection.IntersectLine;
            }

            double bax = StartPoint.X - EndPoint.X;
            double bay = StartPoint.Y - EndPoint.Y;

            if (bcx * bax + bcy * bay < 0)  // bc.Dot(ba) < 0)
            {
                return Intersection.IntersectLine;
            }

            return Intersection.IntersectSegment;
        }


        public Intersection IsIntersectingSlow(Point center, double radius)
        {

            double edgeVectorX = EndPoint.X - StartPoint.X; 
            double edgeVectorY = EndPoint.Y - StartPoint.Y;

            double startCenterVectorX = center.X - EndPoint.X;
            double startCenterVectorY = center.Y - EndPoint.Y;

            double a = edgeVectorX * edgeVectorX + edgeVectorY * edgeVectorY;

            double b = 2 * (edgeVectorX * startCenterVectorX + edgeVectorY * startCenterVectorY);

            double c = center.X * center.X + center.Y * center.Y;
            c += EndPoint.X * EndPoint.X + EndPoint.Y * EndPoint.Y;
            c -= 2 * (center.X * EndPoint.X + center.Y * EndPoint.Y);
            c -= radius * radius;

            double bb4ac = b * b - 4 * a * c;

            // System.Console.WriteLine("a:" + a + " b:" + b + " c: " + c + " bb4ac: " + bb4ac);

            // one result is just touching, no intersection
            if (bb4ac <= 0)
            {
                return Intersection.None;    // No collision
            }
            else
            {
                double sqrtbb4ac = Math.Sqrt(bb4ac);

                double t1 = (-b + sqrtbb4ac) / (2 * a);

                if (t1 < 1 && t1 > 0)
                {
                    return Intersection.IntersectSegment; //Collision with the segment
                }

                double t2 = (-b - sqrtbb4ac) / (2 * a);

                if (t2 < 1 && t2 > 0)
                {
                    return Intersection.IntersectSegment; //Collision with the segment
                }

                if( t2 <= 0 && t1 >= 1 || t1 <= 0 && t2 >= 1) // segment is inside
                {
                    return Intersection.IntersectSegment; //Collision with the segment
                }

                return Intersection.IntersectLine;      //Collision with the line
            }
        }


    }
}