﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine;
using TerekCraftEngine.events;
using TerekCraftEngine.actions;
using TerekCraftEngine.strategies;
using TerekCraftEngine.commands;


public class PocSkirmishScenarioWithObstacles : PocSkirmishScenario
{
	public PocSkirmishScenarioWithObstacles(int teamSize, System.Random random, string team1, string team2):base(teamSize, random, team1, team2)
	{

	}
	
	public override void Init (World world)
	{
		InitObstacles(world);

		base.Init (world);
	}

	void InitObstacles (World world)
	{
		var rockActorType = new ActorType("rock", 1000000, 15f);

		for(int i = 0; i < 10; i++){
			CreateActor (rockActorType, world, "", -50f, (float)random.NextDouble () * Mathf.PI);
		}
	}

}

public class PocSkirmishScenario : IScenario
{
    private readonly int teamSize;
    protected readonly System.Random random;
    private readonly string team1;
    private readonly string team2;

    private readonly ActorType actorWithMissileType;
    private readonly ActorType actorType;
    private readonly ActorType footSoldierType;

    int GenerationRate = 10;

    IStrategy strategy1;
    IStrategy strategy2;

    public PocSkirmishScenario(int teamSize, System.Random random, string team1, string team2)
    {
        strategy1 = new SimpleStrategyWithObstacleAvoidance(team1);
        strategy2 = new SimpleStrategyWithObstacleAvoidance(team2);

        this.teamSize = teamSize;
        this.team1 = team1;
        this.team2 = team2;
        this.random = random;

        actorType = new ActorType("tank", 15, 1.5f, actions: new BaseAction[] { new MoveForwardAction(1f), new TurnAction(0.5), new AimAction(1f), new ShootAimedAction(1, 30) { Cooldown = 10 } }, tags: new HashSet<string>() { "actor", "tank" } );

		var shootMissileAction = new ShootMissileAction(3f, 5f, 3, 20, "explode") {
            Cooldown = 30
        };

        actorWithMissileType = new ActorType("launcher", 5, 1.5f, actions: new BaseAction[]{  new MoveForwardAction(0.3f), new TurnAction(0.5), shootMissileAction } , tags: new HashSet<string>() { "actor", "tank" } );

		footSoldierType = new ActorType("soldier", 5, 0.5f, actions: new BaseAction[]{  new MoveAction(1f), new ShootAction(1, 10.0) { Cooldown = 2 } }, tags: new HashSet<string>() { "actor" } );
	}
	
	public virtual void Init(World world)
	{
		initTeam (world, team1, -100, 0f);
		initTeam (world, team2, +20, Mathf.PI);
	}
	
	private void initTeam(World world, string team, float xOffset, float facing){
		for (int i = 0; i < teamSize / 2; i++)
		{
			CreateActor (footSoldierType, world, team, xOffset, facing);
		}
		for (int i = 0; i < teamSize / 4; i++)
		{
			CreateActor (actorType, world, team, xOffset, facing);
		}
		for (int i = 0; i < teamSize / 4; i++)
		{
			CreateActor (actorWithMissileType, world, team, xOffset, facing);
		}
	}
	
	protected void CreateActor (ActorType actorType, World world, string team, float xOffset, float facing)
	{
		Actor actor = new Actor (actorType, team);
		actor.Location = GetFreeLocation (world, xOffset);
		actor.Direction = facing;
		world.AddActor (actor);
	}
	
	private Point GetFreeLocation(World world, float xOffset){
		
		Point point = new Point(random.NextDouble() * 80 + xOffset, random.NextDouble() * 200 - 100);
		while(world.SpatialDatabase.RetreiveInRange(point, 12).Count() > 0){
			point = new Point(random.NextDouble() * 80 + xOffset, random.NextDouble() * 200 - 100);
		}
		
		return point;
	}
	
	public void Update(World world)
	{
		if (world.FrameNumber % GenerationRate == 0) {
			CreateActor (GetRandomActorType (), world, team1, -140, 0f);
			CreateActor (GetRandomActorType (), world, team2, +60, Mathf.PI);
		}
		
		world.AddCommand (strategy1.DoTick (world));
		world.AddCommand (strategy2.DoTick (world));
	}
	
	private ActorType GetRandomActorType(){
		int randomInt = random.Next (10);
		if (randomInt == 1) {
			return actorWithMissileType;
		} else if (randomInt == 3) {
			return actorType;
		} else {
			return footSoldierType;
		}
	}
}
