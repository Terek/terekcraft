﻿using UnityEngine;
using System.Collections;

public class DecalHider : MonoBehaviour {

	public int framesUntilTransparent = 200;
	
	float alphaDiff = 0;
	
	ExplosionMat explosion;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (this.framesUntilTransparent > 0) {
			framesUntilTransparent--;
			Color color = this.transform.GetComponent<Renderer>().material.color;

			this.transform.GetComponent<Renderer>().material.color = new Color(color.r, color.g, color.b, color.a - (1f / framesUntilTransparent) );
		} else {
			Destroy (this.gameObject);
		}		
	}

}
