using UnityEngine;
using System.Collections;
using TerekCraftEngine.client;
using TerekCraftEngine.events;
using TerekCraftEngine;

public class SoldierController : BaseController {
	
	Animation animator;

	Transform muzzleFlash;
	
	string animationId = "soldierFiring";

	public int muzzleFrameCount = 30;
	
	private int currentMuzzleFrames = 4;


	public override void  Start(){
		animator = GetComponent<Animation> ();

		animator ["soldierDieFront"].speed = 2f;

		muzzleFlash = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/" +
			"Bip01 Spine2/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm/Bip01 R Hand/gun/muzzleFlash");
		muzzleFlash.GetComponent<Renderer>().enabled = false;
	}


	public override void HandleEvent(AimEvent aimEvent, float lerp){

	}
		
	public override void HandleEvent(DamageEvent damageEvent){}
		
	public override bool Select(){
		return true;
	}
	
	public override bool Deselect(){
		return true;
	}

	public virtual void Update(){
		if (animationId != null) {
						animator.Play (animationId);
		}

		if (animationId == "soldierDieFront") {
            animationId = null;
		}

		if(currentMuzzleFrames < muzzleFrameCount){
			ShowMuzzleFlash (true);
			currentMuzzleFrames++;
		}
		else{
			ShowMuzzleFlash (false);
		}

		this.transform.Find("armorBody").GetComponent<Renderer>().material.color = Tint;
	}


	public override void HandleEvent (DespawnEvent despawnEvent)
	{
        animationId = "soldierDieFront";
		Destroy (this.gameObject, 1f);
	}


	public override void HandleEvent(ShootEvent damageEvent){
        animationId = "soldierFiring";
		this.transform.forward = Vector3.Normalize (GetPositionFromActor(damageEvent.ToLocation) - GetPositionFromActor(damageEvent.FromLocation));
		currentMuzzleFrames = 0;
	}

	
	private void ShowMuzzleFlash(bool enabled){
		if (muzzleFlash != null) {
			muzzleFlash.transform.LookAt(Camera.main.transform.position);
			muzzleFlash.GetComponent<Renderer>().enabled = enabled;
		}	
	}

	protected override void Move(Vector3 oldPosition, Vector3 newPosition, float lerp){
		var newPosLerp = Vector3.Lerp(oldPosition, newPosition, lerp);
		this.transform.position = newPosLerp;
        animationId = "soldierRun";
		this.transform.forward = Vector3.Normalize (newPosition - oldPosition);

	}
	


}
