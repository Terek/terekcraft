﻿using UnityEngine;
using System.Collections;

public class PyroExplosionDriver : MonoBehaviour {


	public int framesUntilFullSize = 20;
	public int framesUntilTransparent = 20;

	float scaleDiff ;
	float alphaDiff ;

	ExplosionMat explosion;

	// Use this for initialization
	void Start () {
		scaleDiff = this.transform.localScale.x / framesUntilFullSize;
		this.transform.localScale = new Vector3(1, 1, 1);

		explosion = this.GetComponent(typeof(ExplosionMat)) as ExplosionMat; 

		alphaDiff = explosion._alpha / framesUntilTransparent;

	}
	
	// Update is called once per frame
	void Update () {
		if (framesUntilFullSize > 0) {
			framesUntilFullSize--;
			float scale = this.transform.localScale.x;
			this.transform.localScale = new Vector3 (scale + scaleDiff, scale + scaleDiff, scale + scaleDiff);
		} else if (this.framesUntilTransparent > 0) {
			framesUntilTransparent--;
			this.explosion._alpha -= alphaDiff;
		} else {
			Destroy (this.gameObject);
		}

	}
}
