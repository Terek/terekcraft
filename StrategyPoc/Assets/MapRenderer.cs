﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using TerekCraftEngine;
using TerekCraftEngine.terrain;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[AddComponentMenu("Mesh/Dynamic Mesh Generator")]
public class MapRenderer : MonoBehaviour {

    private Vector3[] vertices;


    private void OnDrawGizmos()
    {
        if (vertices == null)
        {
            return;
        }

        Gizmos.color = Color.black;
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], 0.1f);
        }
    }


    // Use this for initialization
    void Start ()
    {

    }

    public void Load(List<Polygon> polys)
    {
        List<Vector3> verticesList = new List<Vector3>();
        List<int> trianglesList = new List<int>();

        foreach (Polygon poly in polys)
        {
            int vertexIndex = verticesList.Count;

            foreach(Point point in poly.Points)
            {
                verticesList.Add(GetPositionFromActor(point));
            }

            for(int i = 1; i < poly.Points.Count - 1; i++)
            {
                trianglesList.Add(vertexIndex);
                trianglesList.Add(vertexIndex + i);
                trianglesList.Add(vertexIndex + i + 1);
            }

        }

        float minX = verticesList.Min(item => item.x);
        float maxX = verticesList.Max(item => item.x);
        float minZ = verticesList.Min(item => item.z);
        float maxZ = verticesList.Max(item => item.z);

        List<Vector2> uvList = new List<Vector2>();

        foreach(Vector3 location in verticesList)
        {
            float u = (location.x - minX) / (maxX - minX);
            float v = (location.z - minZ) / (maxZ - minZ);
            uvList.Add(new Vector2(u, v) );
        }

        trianglesList.Reverse();

        vertices = verticesList.ToArray();

        Mesh mesh;

        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Procedural Grid";

        mesh.vertices = vertices;
        mesh.uv = uvList.ToArray();
        mesh.triangles = trianglesList.ToArray();

        mesh.RecalculateNormals();

    }

    Vector3 GetPositionFromActor(Point location)
    {
        return new Vector3((float)location.X, 0f, (float)location.Y);
    }

}
