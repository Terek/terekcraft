using UnityEngine;
using System.Collections;
using TerekCraftEngine.client;
using TerekCraftEngine.events;
using TerekCraftEngine;

public class UnitController : BaseController {

	Transform muzzleFlash;
	Transform selection;
	Transform turret;
	
	public int muzzleFrameCount = 300;
	
	private int currentMuzzleFrames = 0;

    public override void Start(){

		turret = this.transform.Find("Turret");
		muzzleFlash = turret.transform.Find("muzzleflash");
		selection = this.transform.Find("selection");

		this.transform.Find("Main_Body").GetComponent<Renderer>().material.color = Tint;

		selection.GetComponent<Renderer>().enabled = false;

		this.Turn (-90);
	}


	public override void HandleEvent(AimEvent aimEvent, float lerp){
		float oldDirection = ConvertToRelativeUnityDegree (aimEvent.OldDirection);
		float newDirection = ConvertToRelativeUnityDegree (aimEvent.NewDirection);

		float diff = (newDirection - oldDirection);

		if (diff > 180) {
				diff = 360 - diff;	
		} else if (diff < -180) {
				diff = 360 + diff;	
		}

		float lerpDirection = diff * lerp + oldDirection;

		this.Aim (lerpDirection);
	}
		
	public override void HandleEvent(DamageEvent damageEvent){}
		
	public override bool Select(){
		if(selection != null){
			selection.GetComponent<Renderer>().enabled = true;
		}

		return true;
	}
	
	public override bool Deselect(){
		if(selection != null){
			selection.GetComponent<Renderer>().enabled = false;
		}

		return true;
	}
		
	public virtual void Update(){
		if(currentMuzzleFrames < muzzleFrameCount){
			ShowMuzzleFlash (true);
			currentMuzzleFrames++;
		}
		else{
			ShowMuzzleFlash (false);
		}
	}

	private void ShowMuzzleFlash(bool show){
		if (muzzleFlash != null) {
			muzzleFlash.transform.LookAt(Camera.main.transform.position);
			muzzleFlash.GetComponent<Renderer>().enabled = show;
		}
	}
	
	public override void HandleEvent(ShootEvent damageEvent){
		currentMuzzleFrames = 0;
	}

	private void Aim(float direction){
		if (turret != null) {
			turret.transform.localRotation = Quaternion.Euler (0, 0, direction);
		}
	}

	public virtual void Move(Vector3 oldPosition, Vector3 newPosition, float lerp){		
		var newPosLerp = Vector3.Lerp(oldPosition, newPosition, lerp);
		this.transform.position = newPosLerp;		
	}
	
	private const int PixelSize = 512;
	private const int HealthBarHeight = 4;


}
