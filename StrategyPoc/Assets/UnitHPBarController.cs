﻿using UnityEngine;
using System.Collections;
using TerekCraftEngine.client;
using TerekCraftEngine.events;
using TerekCraftEngine;

public class UnitHPBarController : MonoBehaviour {
	
	public GameObject hpBarPrefab;
	private GameObject hpBarObject;
	private	GUITexture hpBar;
	
	private BaseController unitController;
	
	public int PixelSize = 512;
	public int PixelHeight = 512;
	public int HealthBarHeight = 4;
	
	public void Start(){
		hpBarObject = Instantiate(hpBarPrefab) as GameObject;
		hpBarObject.transform.parent = this.transform;
		hpBar = hpBarObject.transform.GetComponent(typeof(GUITexture)) as GUITexture;
		
		unitController = GetComponent<BaseController> ();
		
	}
	
	
	void OnDestroy() {
		Destroy (hpBarObject);
	}
	
	public void Update(){
		UpdateStatusBarPosition();		
	}
	
	private void UpdateStatusBarPosition(){
		float distance = Vector3.Distance(Camera.main.transform.position, this.transform.position) / 5f;	
		
		var pixelInset = this.hpBar.pixelInset;
		
		var hitPoints = Mathf.Max (unitController.CurrentHitPoints, 0f);
		
		float hpRatio  = (float)hitPoints / (float)unitController.StartHitPoints;
		
		var color = this.hpBar.color;
		color.r = 1-hpRatio;
		color.g = hpRatio;
		
		this.hpBar.color = color;
		
		pixelInset.width = PixelSize * hpRatio / distance;
		pixelInset.height = Mathf.Max (HealthBarHeight / distance, HealthBarHeight);
		pixelInset.y = PixelHeight / 2f / distance ;
		pixelInset.x =  - PixelSize / 2f / distance ;
		
		this.hpBar.pixelInset = pixelInset;
		
		var screenPosition = Camera.main.WorldToViewportPoint(this.transform.position);
		this.hpBar.transform.position = screenPosition;
	}
	
}
