﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TerekCraftEngine;
using TerekCraftEngine.actions;
using TerekCraftEngine.spatial;
using TerekCraftEngine.terrain;
using UnityEngine;

namespace POC
{
    public class PocPathFinderDemoScenario : IScenario
    {
        private World world;

        ActorType actorType = new ActorType("tank", 15, 1.5f, actions: new BaseAction[] { new MoveForwardAction(1f), new TurnAction(0.5), new AimAction(1f), new ShootAimedAction(1, 30) { Cooldown = 10 } }, tags: new HashSet<string>() { "actor", "tank" });

        Actor actor;

        NavigationMesh<Polygon> navMesh;
        BSPTree<Polygon> bspTree;

        Point targetLocation;

        public Point TargetLocation
        {
            get { return targetLocation; }
            set { this.targetLocation = value; }
        }

        public void Init(World world)
        {
            this.world = world;

            actor = new Actor(actorType);
            actor.Location = GetRandomLocation();
            world.AddActor(actor);

            navMesh = new NavigationMesh<Polygon>(world.Map);

            targetLocation = GetRandomLocation();
            bspTree = new BSPTree<Polygon>(new List<Polygon>(world.Map));
        }

        private Point GetRandomLocation()
        {
            Polygon polygon = GetRandomPolygon();

            return polygon.GetCentroid();

        }

        private Polygon GetRandomPolygon()
        {
            return world.Map[world.Random.Next(world.Map.Count)];
        }

        public void Update(World world)
        {
            if (targetLocation != null)
            {
                if (world.SpatialDatabase.RetreiveAt(targetLocation).Contains(actor))
                {
                    targetLocation = null;
                    return;
                }


                Polygon from = bspTree.GetPolygonForPoint(actor.Location);
                Polygon to = bspTree.GetPolygonForPoint(targetLocation);

                List<Edge> path =  navMesh.GetPath(from, to);


                Point firstTarget;

                if (path.Any())
                {
                    Edge firstPathElement = path[0];

                    firstTarget = firstPathElement.StartPoint.Add(firstPathElement.EndPoint).Times(0.5);
                }
                else
                {
                    firstTarget = targetLocation;
                }

                double direction = actor.Location.GetDirectionTo(firstTarget);

                double dirOffsetBase = 0.1d;
                double dirOffset = dirOffsetBase;

                int counter = 0;

                while (bspTree.HasCollision(actor.Location.Move(direction, 1f), actor.Size) && counter < 20){
                    dirOffset += dirOffsetBase * Math.Sign(dirOffset);
                    dirOffset *= -1.0;

                    direction += dirOffset;

                    counter++;
                }

                world.AddCommand(new TurnCommand(actor.Id, direction));
                world.AddCommand(new MoveForwardCommand(actor.Id, 1f));
            }

        }
    }
}
