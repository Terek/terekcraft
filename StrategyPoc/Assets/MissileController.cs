using UnityEngine;
using System.Collections;
using TerekCraftEngine;
using TerekCraftEngine.events;


public class MissileController : BaseController {


	public override bool Select(){ return false; }

	public override bool Deselect() {
				return true;
		}

	public override void HandleEvent(AimEvent aimEvent, float lerp){
		}

	public override void HandleEvent(DamageEvent damageEvent){}
	public override void HandleEvent(ShootEvent damageEvent){}

	protected override Vector3 GetPositionFromActor (Point location)
	{
		return new Vector3( (float)location.X , 0.5f, (float)location.Y );
	}


}
