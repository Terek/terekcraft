using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TerekCraftEngine;

public class CameraControls : MonoBehaviour {

	float speed = 25f;
	
	List<BaseController> previouslySelected = new List<BaseController>();
	
	Vector3 mouseDownLocation ;
	float mouseDownStartTime = 0f;
	
	private const float SimpleClickTime = 0.2f;
	
	bool dragStarted = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
       if (Input.GetKey (KeyCode.UpArrow)) transform.position += (new Vector3(0,0,1) * Time.deltaTime*speed);
       if (Input.GetKey (KeyCode.DownArrow)) transform.position += (new Vector3(0,0,-1) * Time.deltaTime*speed);
       if (Input.GetKey (KeyCode.LeftArrow)) transform.Translate (new Vector3(-1,0,0) * Time.deltaTime*speed);
       if (Input.GetKey (KeyCode.RightArrow)) transform.Translate (new Vector3(1,0,0) * Time.deltaTime*speed);
			
	    if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
        {
            transform.Translate (new Vector3(0,0,-5) * Time.deltaTime*speed*3);
 
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            transform.Translate (new Vector3(0,0,5) * Time.deltaTime*speed*3);
        }
	
	}
		
	
	private Rect calculateScreenRectangle(){
		var currentMouseLocation = Input.mousePosition;
		int top = (int)Mathf.Min( Screen.height -(int)currentMouseLocation.y, Screen.height -(int)mouseDownLocation.y );
		int left = (int)Mathf.Min( currentMouseLocation.x, mouseDownLocation.x );
		int width = (int)Mathf.Abs( currentMouseLocation.x -  mouseDownLocation.x );
		int height = (int)Mathf.Abs( currentMouseLocation.y -  mouseDownLocation.y );

		Rect screenRectangle = new Rect(left, top, width, height);	
		
		return screenRectangle;
	}

    private Rect calculateSelectionRectangle(Point start, Point end)
    {
        float top = Mathf.Min((float)start.Y, (float)end.Y);
        float left = Mathf.Min((float)start.X, (float)end.X);
        float width = Mathf.Abs((float)start.X - (float)end.X);
        float height = Mathf.Abs((float)start.Y - (float)end.Y);

        return new Rect(left, top, width, height);
    }


    public void OnGUI(){
		if( Input.GetMouseButtonDown(0)){
			mouseDownLocation = Input.mousePosition;
			mouseDownStartTime = Time.time;
		}
		
		dragStarted = Time.time - mouseDownStartTime > SimpleClickTime;

		var screenRectangle = calculateScreenRectangle();
		
		if( Input.GetMouseButton(0) && dragStarted){
			GUI.Box( screenRectangle, "" );	
		}
		
		
		if (Input.GetMouseButtonUp (0)  ){ // if left button pressed...
			
			foreach(var unit in previouslySelected){
				unit.Deselect();	
			}
			previouslySelected.Clear();

            PocScript main = this.transform.GetComponent(typeof(PocScript)) as PocScript;


            if (!dragStarted)
            {

                RaycastHit hit = RayCast(Input.mousePosition);

                foreach (Actor actor in main.World.SpatialDatabase.RetreiveInRange(main.GetPoint(hit.point), 10f))
                {
                    main.unitControllers[actor.Id].Select();
                    previouslySelected.Add(main.unitControllers[actor.Id]);
                }

            }
            else
            {
                RaycastHit start = RayCast(mouseDownLocation);
                RaycastHit end = RayCast(Input.mousePosition);

                var selectionRectangle = calculateSelectionRectangle(main.GetPoint(start.point), main.GetPoint(end.point));

                foreach (var actor in main.actors.Values){

                    if (main.unitControllers.ContainsKey(actor.Id))
                    {
                        var unit = main.unitControllers[actor.Id];

                        if (selectionRectangle.Contains(new Vector2((float)actor.Location.X, (float)actor.Location.Y)))
                        {
                            unit.Select();
                            previouslySelected.Add(unit);
                        }
                    }
				}
				
				
			}
			
			dragStarted = false;
    	}
	}

    private static RaycastHit RayCast(Vector2 pos)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(pos.x, pos.y, 0.0f));
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        return hit;
    }

}
