﻿using UnityEngine;
using System.Collections;

public class SoldierAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Animation animator = GetComponent<Animation> ();

		animator.Play ("soldierFiring");
		animator ["soldierRun"].speed = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
		Animation animator = GetComponent<Animation> ();
		animator.Play ("soldierRun");
	}
}
