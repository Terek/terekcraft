﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine;
using TerekCraftEngine.events;
using TerekCraftEngine.actions;
using TerekCraftEngine.strategies;
using TerekCraftEngine.commands;

public class SimpleSkirmishStrategy : IStrategy
{
	System.Random random = new System.Random();
	
	string team;
	
	public SimpleSkirmishStrategy(string team) {
		this.team = team;
	}
	
	public string Name{  get{ return "Skynet"; }}
	
	
	public IEnumerable<ICommand> DoTick(World world)
	{
		foreach (var actor in world.Actors.Where(item => item.Team == team))
		{
			var victim = world.Actors.Where(item => item.Team != team && item.Type.HasTag("actor") )
				.OrderBy(item => actor.Location.GetDistanceSquaredTo(item.Location))
					.FirstOrDefault();
			
			if(victim == null) {
				yield return new ShootMissileCommand(actor.Id, 0);
				continue;
			};
			
			double direction = actor.Location.GetDirectionTo(victim.Location);
			double distance = System.Math.Sqrt(actor.Location.GetDistanceSquaredTo(victim.Location));
			
			var shootAimedAction = (ShootAimedAction)(actor.Type.GetActionByType<ShootAimedAction>());
			var shootMissile = (ShootMissileAction)(actor.Type.GetActionByType<ShootMissileAction>());
			
			if(shootAimedAction != null)
			{
				if (distance < shootAimedAction.Range)
				{
					yield return new AimCommand(actor.Id, direction - actor.Direction);
					yield return new ShootAimedCommand(actor.Id);
				}
				if (distance > shootAimedAction.Range / 2f)
				{
					yield return new TurnCommand(actor.Id, direction);
					yield return new MoveForwardCommand(actor.Id, ((MoveForwardAction)(actor.Type.GetActionByType<MoveForwardAction>())).MaxSpeed);
				}
			}
			if(shootMissile != null){
				if (distance < shootMissile.Speed * shootMissile.TimeToLive)
				{
					yield return new AimCommand(actor.Id, direction - actor.Direction);
					yield return new ShootMissileCommand(actor.Id, direction);
				}
				if (distance > shootMissile.Speed * shootMissile.TimeToLive)
				{
					yield return new TurnCommand(actor.Id, direction);
					yield return new MoveForwardCommand(actor.Id, ((MoveForwardAction)(actor.Type.GetActionByType<MoveForwardAction>())).MaxSpeed);
				}
			}
		}
	}
}

public class SimpleStrategyWithObstacleAvoidance : IStrategy
{
	System.Random random = new System.Random();
	
	string team;
	
	public SimpleStrategyWithObstacleAvoidance(string team) {
		this.team = team;
	}
	
	public string Name{ get{ return "SHODAN"; } }
	
	
	public IEnumerable<ICommand> DoTick(World world)
	{
		foreach (var actor in world.Actors.Where(item => item.Team == team))
		{
			var victim = world.Actors.Where(item => item.Team != team && item.Type.HasTag("actor") )
				.OrderBy(item => actor.Location.GetDistanceSquaredTo(item.Location))
					.FirstOrDefault();
			
			if(victim == null) {
				yield return new ShootMissileCommand(actor.Id, 0);
				continue;
			};
			
			double direction = actor.Location.GetDirectionTo(victim.Location);
            double distance = System.Math.Sqrt(actor.Location.GetDistanceSquaredTo(victim.Location));

            switch (actor.Type.Name){
			case "tank": {
				var shootAimedAction = (ShootAimedAction)(actor.Type.GetActionByType<ShootAimedAction>());
				if(shootAimedAction != null)
				{
					if (distance < shootAimedAction.Range)
					{
						yield return new AimCommand(actor.Id, direction - actor.Direction);
						yield return new ShootAimedCommand(actor.Id);
					}
					if (distance > shootAimedAction.Range / 2f)
					{
						double maxSpeed = ((MoveForwardAction)(actor.Type.GetActionByType<MoveForwardAction>())).MaxSpeed;
						
						double newMoveDirection = world.AiTools.GetNewDirection(actor, actor.Location.Move(direction, maxSpeed), victim.Location );
						
						yield return new TurnCommand(actor.Id, newMoveDirection);
						yield return new MoveForwardCommand(actor.Id, maxSpeed);
					}
				}

				break;
			}
			case "soldier": {
				var shootAction = (ShootAction)(actor.Type.GetActionByType<ShootAction>());
				if(shootAction != null)
				{
					if (distance < shootAction.Range)
					{
						if(random.Next(2) == 0){
							yield return new ShootCommand(actor.Id, direction);
						}
					}
					else{
						double maxSpeed = ((MoveAction)(actor.Type.GetActionByType<MoveAction>())).MaxSpeed;
						
						double newMoveDirection = world.AiTools.GetNewDirection(actor, actor.Location.Move(direction, maxSpeed), victim.Location );
						
						yield return new MoveCommand(actor.Id, newMoveDirection, maxSpeed);
					}
				}
				break;
			}
			case "launcher": {
				var shootMissile = (ShootMissileAction)(actor.Type.GetActionByType<ShootMissileAction>());
				if(shootMissile != null){
					if (distance < shootMissile.Speed * shootMissile.TimeToLive)
					{
						yield return new AimCommand(actor.Id, direction - actor.Direction);
						yield return new ShootMissileCommand(actor.Id, direction);
					}
					if (distance > shootMissile.Speed * shootMissile.TimeToLive)
					{
						double maxSpeed = ((MoveForwardAction)(actor.Type.GetActionByType<MoveForwardAction>())).MaxSpeed;
						double newMoveDirection = world.AiTools.GetNewDirection(actor, actor.Location.Move(direction, maxSpeed), victim.Location );
						
						yield return new TurnCommand(actor.Id, newMoveDirection);
						yield return new MoveForwardCommand(actor.Id, maxSpeed);
					}
				}

				break;
			}


			}

		}
	}
}

