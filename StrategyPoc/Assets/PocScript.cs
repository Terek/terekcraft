﻿using UnityEngine;
using System.Collections;
using TerekCraftEngine;
using System.Collections.Generic;
using System.Linq;
using TerekCraftEngine.events;
using TerekCraftEngine.actions;
using TerekCraftEngine.strategies;
using TerekCraftEngine.commands;
using System.Threading;
using TerekCraftEngine.terrain;
using System.IO;
using System.Text;
using POC;

public class PocScript : MonoBehaviour {

    static System.Random random = new System.Random();

    public TextAsset textFile;

    IScenario scenario; 

	int framePerTick = 5;
	int tickPerFrame = 1;

	private const float planeYLocation = 0.1f;
	
    World _world = new World();
	
	private int frameSinceTick = 0;

	GameObject _missile = null;

	GameObject _tank1 = null;
	GameObject _soldier1 = null;
	GameObject _launcher1 = null;
	GameObject _explosion = null;
	// GameObject _blastMark = null;
	GameObject _targetRectangle = null;

	GameObject _rock = null;

	GameObject _blastMarkParticlePrefab = null;
	ParticleSystem _blastMarkParticleSystem = null;
	
	GameObject _projectileTracerPrefab = null;
	ParticleSystem _projectileTracerParticleSystem = null;

    MapRenderer mapRenderer;
	


	public Dictionary<string, BaseController> unitControllers = new Dictionary<string, BaseController>();
	public Dictionary<string, Actor> actors = new Dictionary<string, Actor>();

	public List<GameObject> otherObjects = new List<GameObject>();

    public IScenario Scenario { get { return scenario; } }
    public World World { get { return _world; } }

    // Use this for initialization
    void Start () {
		_missile = Resources.Load("missile2", typeof(GameObject)) as GameObject;

		_tank1 = Resources.Load("tank01", typeof(GameObject)) as GameObject;
		_soldier1 = Resources.Load("soldier01", typeof(GameObject)) as GameObject;
		_launcher1 = Resources.Load("launcher01", typeof(GameObject)) as GameObject;
		_explosion = Resources.Load("TankExplosion2", typeof(GameObject)) as GameObject;
		// _blastMark = Resources.Load("Crater", typeof(GameObject)) as GameObject;
		_targetRectangle = Resources.Load("healthBar", typeof(GameObject)) as GameObject;

		_rock = Resources.Load("rock_01", typeof(GameObject)) as GameObject;

		_blastMarkParticlePrefab = Resources.Load("BlastMarkParticles", typeof(GameObject)) as GameObject;
		GameObject particleInstance = Instantiate (_blastMarkParticlePrefab, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		_blastMarkParticleSystem = particleInstance.GetComponent<ParticleSystem>( );

		_projectileTracerPrefab = Resources.Load("ProjectileTracer", typeof(GameObject)) as GameObject;
		GameObject projectileTracerInstance = Instantiate (_projectileTracerPrefab, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		_projectileTracerParticleSystem = projectileTracerInstance.GetComponent<ParticleSystem>( );


        GameObject mapPrefab = Resources.Load("MapObject", typeof(GameObject)) as GameObject;
        GameObject mapObject = Instantiate(mapPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;

        mapRenderer = mapObject.GetComponent(typeof(MapRenderer)) as MapRenderer;

    }

    void OnGUI(){

		float GameSpeed = 30f * tickPerFrame / framePerTick;
		        
		GUI.Box (new Rect (10,10,200,90), "Game speed:" + GameSpeed + "x" );
		
		if (GUI.Button (new Rect (20,40,180,20), "Faster")) {
			IncreaseGameSpeed();
		}		
		if (GUI.Button (new Rect (20,70,180,20), "Slower")) {
			DecreaseGameSpeed();
		}

		GUI.Box (new Rect (10,110,200,160), "Scenarios" );

		if (GUI.Button (new Rect (20,140,180,20), "Simple skirmish 10")) {
			StartScenario(new PocSkirmishScenario (10, random, "team1", "team2"));
		}

		if (GUI.Button (new Rect (20,170,180,20), "Simple skirmish 100")) {
			StartScenario(new PocSkirmishScenario (100, random, "team1", "team2"));
		}

		if (GUI.Button (new Rect (20,200,180,20), "Simple skirmish with Rocks")) {
			StartScenario(new PocSkirmishScenarioWithObstacles (100, random, "team1", "team2"));
		}

        if (GUI.Button(new Rect(20, 230, 180, 20), "Pathfinding demo"))
        {
            StartScenarioWithMap(new PocPathFinderDemoScenario());
        }


        if (Input.GetMouseButtonUp(0))
        { // if left button pressed...

            Vector2 pos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(pos.x, pos.y, 0.0f));
            RaycastHit hit;
            Physics.Raycast(ray, out hit);

            // main.DoExplosion(hit.point);

            if (Scenario is PocPathFinderDemoScenario)
            {
                (Scenario as PocPathFinderDemoScenario).TargetLocation = GetPoint(hit.point);

            }


        }


        /*
		GUI.Box (new Rect (10,250,200,130), "Misc" );

		if (GUI.Button (new Rect (20,280,180,20), "Shoot")) {
			GameObject.Find("soldierLOD3").GetComponent<Animation>().Play("soldierFiring");
		}

		if (GUI.Button (new Rect (20,310,180,20), "Run")) {
			GameObject.Find("soldierLOD3").GetComponent<Animation>().Play("soldierRun");
		}
*/


    }

    void DestroyCurrentWorld ()
	{
		foreach(var controller in unitControllers.Values){
			controller.Despawn();
		}
		unitControllers = new Dictionary<string, BaseController>();

		foreach (var gameObject in otherObjects) 
		{
			Destroy(gameObject);
		}

		otherObjects = new List<GameObject> ();
		actors = new Dictionary<string, Actor> ();
	}

	void StartScenario (IScenario newScenario)
	{
		this.scenario = newScenario;

		DestroyCurrentWorld ();
		_world = new World ();
		scenario.Init(_world);

        Polygon polygon = new Polygon(
            new Point(-100, -100),
            new Point(100, -100),
            new Point(100, 100),
            new Point(-100, 100)
        );

        mapRenderer.Load(new List<Polygon> { polygon });

    }


    void StartScenarioWithMap(IScenario newScenario)
    {
        PolyImporter importer = new PolyImporter(0, 2);
        List<Polygon> polys = importer.ConvertInput(textFile.text);

        this.scenario = newScenario;

        DestroyCurrentWorld();
        _world = new World();
        _world.SetMap(polys);
        scenario.Init(_world);


        mapRenderer.Load(polys);

    }


    private void IncreaseGameSpeed(){
		if (framePerTick > 1) {
			framePerTick--;
		} else {
			tickPerFrame++;
		}
	}

	private void DecreaseGameSpeed(){
		if (tickPerFrame > 1) {
			tickPerFrame--;
		} else {
			framePerTick++;
		}
	}

	
	// Update is called once per frame
	void Update () {
		frameSinceTick++;

		List<TerekCraftEngine.events.Event> events = new List<TerekCraftEngine.events.Event> ();

		if (frameSinceTick >= framePerTick) {
			frameSinceTick = 0;
			// DoTick();
			for(int i = 0; i < tickPerFrame; i++){ 

				if(scenario != null){
					scenario.Update(_world);
				}

				_world.Tick ();
				events.AddRange (_world.GetTickEvents ());
			}
		} else {
			events.AddRange (_world.GetTickEvents ());
		}
			
		float lerp = (float)frameSinceTick / (float)framePerTick;

		foreach(var tickEvent in events){

		// 	Debug.Log("frame:" + _world.FrameNumber + " frameSinceTick:" + frameSinceTick + "   " + tickEvent.ToString());

			if(tickEvent is DamageEvent){
				HandleDamageEvent (tickEvent as DamageEvent, lerp);
			}
			
			if(tickEvent is DespawnEvent){
				HandleDespawnEvent (tickEvent as DespawnEvent);
			}

			if( tickEvent is MoveEvent){
				HandleMoveEvent (tickEvent as MoveEvent, lerp);
			}
			
			if( tickEvent is  TurnEvent){
				HandleTurnEvent (tickEvent as TurnEvent, lerp);
			}
			
			if( tickEvent is  AimEvent){
				HandleAimEvent (tickEvent as AimEvent, lerp);
			}

			if(tickEvent is ShootEvent){
				HandleShootEvent (tickEvent as ShootEvent);
			}			

			if(tickEvent is SpawnEvent){
				HandleSpawnEvent (tickEvent as SpawnEvent);

			}			

		}		
	}

	void HandleSpawnEvent (SpawnEvent spawnEvent)
	{
		if (frameSinceTick == 0) {
			Actor actor = spawnEvent.Actor;

			this.actors.Add(actor.Id, actor);

			GameObject actorObject = null;
			Vector3 location = GetPosition(actor.Location);
			float direction = ConvertToRelativeUnityDegree (actor.Direction);

			if(actor.Type.Name == "tank"){
				actorObject = Instantiate(_tank1, location, Quaternion.Euler (0, 0, 0)) as GameObject;
			}
			else if(actor.Type.Name == "soldier"){
				actorObject = Instantiate(_soldier1, location, Quaternion.Euler (0, 0, 0)) as GameObject;
			}
			else if(actor.Type.Name == "launcher"){
				actorObject = Instantiate(_launcher1, location, Quaternion.Euler (0, 0, 0)) as GameObject;
			}
			else if (spawnEvent.Actor.Type.Name == "missile") {
				actorObject = Instantiate(_missile, location, Quaternion.Euler (0, 0, direction)) as GameObject;
			}
			else if(spawnEvent.Actor.Type.Name == "rock"){
				GameObject rock = (GameObject)Instantiate(_rock, location, Quaternion.Euler (0, (float)random.NextDouble() * 360, 0));
				this.otherObjects.Add(rock);
			}

			if(actorObject != null){
				BaseController unitController = actorObject.GetComponent (typeof(BaseController)) as BaseController;
				unitController.StartHitPoints = actor.HitPoints;
				unitController.CurrentHitPoints = actor.HitPoints;
				
				if(actor.Team == "team1"){
					unitController.Tint = new Color(1f, 0.3f, 0.3f);
				}
				else{
					unitController.Tint = new Color(0.3f, 0.3f, 1f);
				}
				

				unitControllers.Add (spawnEvent.Actor.Id, unitController);
			}
		}
	}

	
	void HandleDamageEvent (DamageEvent damageEvent, float lerp)
	{
		if (frameSinceTick == 0) {
						if (this.unitControllers.ContainsKey (damageEvent.Target)) {
								var unitController = this.unitControllers [damageEvent.Target];
								// Debug.Log ("Target: " + damageEvent.Target + " Current HP:" + unitController.CurrentHitPoints + " Damage: " + damageEvent.Damage + " in Turn:" + _world.FrameNumber);
								unitController.CurrentHitPoints = unitController.CurrentHitPoints - damageEvent.Damage;
						}
				}
	}	
	
	void HandleShootEvent (ShootEvent shootEvent)
	{
		if(this.unitControllers.ContainsKey(shootEvent.ActorId)){

			if(frameSinceTick >= framePerTick -5){
				var from = new Vector3 ((float)shootEvent.FromLocation.X, 1f, (float)shootEvent.FromLocation.Y);
				var to = new Vector3 ((float)shootEvent.ToLocation.X, 1f, (float)shootEvent.ToLocation.Y);
				Debug.DrawLine (
					from, 
					to, 
					Color.red, 
					1, 
					false);

				
				_projectileTracerParticleSystem.Emit (
					from,
					(to - from) * 10,
					0.5f, 
					0.1f,
					new Color32 (255, 255, 255,255));
				



				this.unitControllers[shootEvent.ActorId].HandleEvent(shootEvent);	
			}
		}
	}	

	void HandleTurnEvent (TurnEvent turnEvent, float lerp)
	{
		if(!unitControllers.ContainsKey(turnEvent.ActorId)){
			return;
		}
		BaseController controller = unitControllers[turnEvent.ActorId];			
		controller.HandleEvent(turnEvent, lerp);
	}

	void HandleAimEvent (AimEvent aimEvent, float lerp)
	{
		if(!unitControllers.ContainsKey(aimEvent.ActorId)){
			return;
		}
		BaseController controller = unitControllers[aimEvent.ActorId];			
		controller.HandleEvent(aimEvent, lerp);
	}
	
	void HandleMoveEvent (MoveEvent moveEvent, float lerp)
	{
		if(!unitControllers.ContainsKey(moveEvent.ActorId)){
			return;
		}
		BaseController controller = unitControllers[moveEvent.ActorId];			
		controller.HandleEvent(moveEvent, lerp);
	}

	void HandleDespawnEvent (DespawnEvent despawnEvent)
	{
		if(!unitControllers.ContainsKey(despawnEvent.ActorId)){
			return;
		}
		BaseController controller = unitControllers[despawnEvent.ActorId];			
		Actor actor = actors[despawnEvent.ActorId];


		if (frameSinceTick >= framePerTick - 1 || tickPerFrame > 1) {
			var position = GetPosition(despawnEvent.Location);
			if( tickPerFrame <= 1){
				GameObject explosionType = null;

				if(despawnEvent.HasTag("explode")){
					explosionType = _explosion;
				}
				else if(actor.Type.Tags.Contains("tank")){
					explosionType = _explosion;
				}

				if(explosionType != null)
                {
                    DoExplosion(position, explosionType);

                }
            }

			controller.HandleEvent(despawnEvent);
			unitControllers.Remove (despawnEvent.ActorId);
		}
	}

    private void DoExplosion(Vector3 position, GameObject explosionType)
    {
        GameObject explosion = Instantiate(explosionType) as GameObject;
        explosion.transform.position = new Vector3(position.x, 0f, position.z);
        _blastMarkParticleSystem.Emit(
            new Vector3(position.x, 0.1f, position.z),
            new Vector3(0, 0, 0),
            (float)random.NextDouble() * 5f + 5f,
            10f,
            new Color32(255, 255, 255, 255));
    }


    public void DoExplosion(Vector3 position)
    {
        DoExplosion(position, _explosion);
    }


    public Vector3 GetPosition (Point location)
	{
		return new Vector3( (float)location.X, 0.5f, (float)location.Y );
	}


    public Point GetPoint(Vector3 location)
    {
        return new Point(location.x, location.z);
    }

    protected float ConvertToUnityDegree (double direction)
	{
		float degree = (float)-direction * 180f / Mathf.PI;				
		degree -= 90;
		
		while( degree > 360 ){
			degree -= 360;	
		}
		while(degree <= -360){
			degree += 360;	
		}
		
		return degree;
	}	

	
	protected float ConvertToRelativeUnityDegree (double direction)
	{
		float degree = (float)-direction * 180f / Mathf.PI;				
		
		while( degree > 360 ){
			degree -= 360;	
		}
		while(degree <= -360){
			degree += 360;	
		}
		
		return degree;
	}

}
