using UnityEngine;
using System.Collections;
using TerekCraftEngine.client;
using TerekCraftEngine.events;
using TerekCraftEngine;

public abstract class BaseController : MonoBehaviour, IUnitHandler {
	
	public Color Tint = Color.red;
		
	public abstract bool Select();
	public abstract bool Deselect();
	public abstract void HandleEvent(AimEvent aimEvent, float lerp);
	public abstract void HandleEvent(DamageEvent damageEvent);
	public abstract void HandleEvent(ShootEvent damageEvent);
	
	public virtual void Start(){
		
	}

	public void HandleEvent(MoveEvent moveEvent, float lerp){
		var newPosition = GetPositionFromActor (moveEvent.NewLocation);
		var oldPosition = GetPositionFromActor (moveEvent.OldLocation);
		this.Move (oldPosition, newPosition, lerp);
	}
	
	public void HandleEvent(TurnEvent turnEvent, float lerp){		
		float oldDirection = ConvertToUnityDegree(turnEvent.OldDirection);
		float newDirection = ConvertToUnityDegree(turnEvent.NewDirection);
		
		float diff = (newDirection - oldDirection);
		
		if( diff > 180){
			diff = 360 - diff;	
		}
		else if (diff < -180){
			diff = 360 + diff;	
		}
		
		float lerpDirection = diff * lerp + oldDirection;
		
		this.Turn(lerpDirection);
	}
	

	public virtual void HandleEvent(DespawnEvent despawnEvent){
		Despawn ();
	}

	public void Despawn ()
	{
		Destroy (this.gameObject);
	}
	
	public int StartHitPoints{ get; set; }
	public int CurrentHitPoints{ get; set; }
	
	protected virtual void Turn(float direction){
		this.transform.rotation = Quaternion.Euler(-90, direction + 180, 0);	
	}
	
	protected virtual void Move(Vector3 oldPosition, Vector3 newPosition, float lerp){		
		var newPosLerp = Vector3.Lerp(oldPosition, newPosition, lerp);
		this.transform.position = newPosLerp;		
	}

	protected float ConvertToRelativeUnityDegree (double direction)
	{
		float degree = (float)-direction * 180f / Mathf.PI;				
		
		while( degree > 360 ){
			degree -= 360;	
		}
		while(degree <= -360){
			degree += 360;	
		}
		
		return degree;
	}
	
	
	protected float ConvertToUnityDegree (double direction)
	{
		float degree = (float)-direction * 180f / Mathf.PI;				
		degree -= 90;
		
		while( degree > 360 ){
			degree -= 360;	
		}
		while(degree <= -360){
			degree += 360;	
		}
		
		return degree;
	}	
	
	protected virtual Vector3 GetPositionFromActor (Point location)
	{
		return new Vector3( (float)location.X , 0f, (float)location.Y );
	}
	
}
