﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace POC
{
    public class PocPathFinderDemoScenario : IScenario
    {
        private World world;

        ActorType actorType = new ActorType("tank", 15, 1.5f, actions: new BaseAction[] { new MoveForwardAction(1f), new TurnAction(0.5), new AimAction(1f), new ShootAimedAction(1, 30) { Cooldown = 10 } }, tags: new HashSet<string>() { "actor", "tank" } );


        public void Init(World world)
        {
            this.world = world;

            Actor actor = new Actor(actorType);
            actor.Location = GetRandomLocation();
            world.AddActor(actor);


        }

        private Point GetRandomLocation()
        {
            Polygon polygon = GetRandomPolygon();

        }

        private Polygon GetRandomPolygon()
        {

        }

        public void Update(World world)
        {
        }
    }
}
